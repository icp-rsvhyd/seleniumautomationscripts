package com.framework.driver;

import static com.framework.driver.CommonMethod.takesScreenshot;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/*import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;*/
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;


/**
 * Initialization Class
 * @author Radinat Sage Tech India Services Pvt Ltd
 *
 */


public class TestBase 
{
	
	public static  WebDriver driver = null;
	public static String browserName;
	public Properties config =null;
	public static Xls_Reader datatable=null;
	public static Xls_Reader webdatatable=null;
	public  Logger log = Logger.getLogger(getClass());
	
	//public String version=null;
	/**
	 *   Initialize is a BeforeClass Method. Browser and portal are the TestNG parameter.  
	 *   @param Browser To launch the required browser.
	 *   @param Portal To launch the require Portal
	 *   @exception IOException 
	   
	 */
	//Launching browser
	//@Parameters({"browser","portal","Environment"})
	
	
	@Parameters({"browser","Environment"})
	@BeforeClass
	public void initialize(String Browser,String Environment) throws IOException
	{
		System.out.println("initializing is starting..........");
		
		//loading all the configurations from a property file
		System.out.println("Loading config files");
		config = new Properties();
		
		String pathLocation=Xml_Reader.getPathFromXml("ProjectConfigProperties");
		System.out.println(pathLocation);
		FileInputStream fp = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\resources\\"+pathLocation);
		
		config.load(fp);
		
		//Loading Data from Excel file
		// Loading Test Data
		
		String excelpathLocation=Xml_Reader.getPathFromXml("ProjectExcelProperties");

		datatable=new Xls_Reader(System.getProperty("user.dir")+"\\src\\main\\resources\\"+excelpathLocation);
		
		/*String webServiceexcelpathLocation=Xml_Reader.getPathFromXml("ProjectWebServiceExcelProperties");

		webdatatable=new Xls_Reader(System.getProperty("user.dir")+"\\src\\main\\resources\\"+webServiceexcelpathLocation);
		System.out.println(System.getProperty("user.dir")+"\\src\\main\\resources\\"+webServiceexcelpathLocation);*/
	
		//Creating Logs for Application		
		Properties log4jProperties;
		log4jProperties =new Properties();
		String logpathLocation=Xml_Reader.getPathFromXml("ProjectLogProperties");

			try
				{
				log4jProperties.load(new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\resources\\"+logpathLocation));
				}
			catch (IOException e) 
				{
				System.err.println("log4j Properties File\"log4j.properties\" not found in current directory..Exiting..");
				System.exit(-1);
				}

		PropertyConfigurator.configure(log4jProperties);
		log.info(" Log Genarating is started.....");
		
		browserName=Browser;
		//version=versionNum;
		// Launches Browser
		/*DesiredCapabilities caps=null;
		
		if(Browser.equalsIgnoreCase("firefox"))
			{
			caps=DesiredCapabilities.firefox();
			caps.setPlatform(org.openqa.selenium.Platform.ANY);
			caps.setBrowserName("firefox");
			browserName=caps.getBrowserName();
			//caps.setVersion("34.0");
			System.out.println("browserName"+browserName);
			
			}
		else if(Browser.equalsIgnoreCase("chrome"))
			{
			caps=DesiredCapabilities.chrome();
			caps.setPlatform(org.openqa.selenium.Platform.ANY);
			caps.setBrowserName("chrome");
			//caps.setVersion("39.0.2171.7");
			browserName=caps.getBrowserName();
			}
		else if(Browser.equalsIgnoreCase("ie"))
			{
			caps=DesiredCapabilities.internetExplorer();
			caps.setBrowserName("iexplorer");
			caps.setPlatform(org.openqa.selenium.Platform.ANY);
			browserName=caps.getBrowserName();
			}
		else if(Browser.equalsIgnoreCase("safari"))
			{
			caps=DesiredCapabilities.safari();
			caps.setBrowserName("safari");
			caps.setPlatform(org.openqa.selenium.Platform.ANY);
			browserName=caps.getBrowserName();
			}
			
	
		 driver=new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),caps);
		//driver = new RemoteWebDriver(new URL("http://sudharsan4:Ex4bnaxpECHpY9fuBJUb@hub.browserstack.com/wd/hub"),caps);
			  */
		
		 
		  
		 
		  //Browser initialization
		  if(Browser.equalsIgnoreCase("firefox"))
			{
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\GridConfiguration\\geckodriver.exe");
			driver =new FirefoxDriver();
			}
		  else if(Browser.equalsIgnoreCase("chrome"))
			{
			  DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\GridConfiguration\\chromedriver.exe");
			  ChromeOptions options = new ChromeOptions();
			  options.addArguments("--disable-notifications");
			  options.addArguments("incognito");
			  options.addArguments("--disable-extensions");
			   driver = new ChromeDriver(options);
			   
			   capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			}
		  else if(Browser.equalsIgnoreCase("ie"))
			{
			  System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\GridConfiguration\\IEDriverServer.exe");
			  driver =new InternetExplorerDriver();
			  
			}
		  else if(Browser.equalsIgnoreCase("safari"))
			{
			  driver = new SafariDriver();
			  
			}
		  else if(Browser.equalsIgnoreCase("opera"))
			{
			  driver = new OperaDriver();
			 
			}
		  
		
		  
		  //implicit wait 
		  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		  driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		  //Deleting cookies
		  driver.manage().deleteAllCookies();
		  //Maximize windows
		  driver.manage().window().maximize();
		  
		  //start /d "C:\Users\sudharsana\Sudharsan\Official\Workspace\GoRummy\GridConfiguration" java -Dwebdriver.ie.driver="C:\Users\sudharsana\Sudharsan\Official\Workspace\GoRummy\GridConfiguration\IEDriverServer.exe" -jar selenium-server-standalone-2.45.0.jar -role webdriver -hub  http://localhost:4444/grid/register -port 5560 -browser browserName=iexplore version=11 platform=WINDOWS 

		   // String TaskIt = config.getProperty("TaskIt");

		String QA=config.getProperty("QA");
		String Staging=config.getProperty("Staging");
		String Production=config.getProperty("Production");
		String TestRoles=config.getProperty("132Roles");

		//Selecting Environment
		if(Environment.equalsIgnoreCase("QA"))
			{
				driver.get(QA);
				log.info("Browser launched with "+QA+" URL successfully");
				Reporter.log("Browser launched with "+QA+" URL successfully");
			}
		else if(Environment.equalsIgnoreCase("Staging"))
			{
				driver.get(Staging);
				log.info("Browser launched with "+Staging+" URL successfully");
				Reporter.log("Browser launched with "+Staging+" URL successfully");
			}
		else if(Environment.equalsIgnoreCase("Production")) 
			{
				driver.get(Production);
				log.info("Browser launched with "+Production+" URL successfully");
				Reporter.log("Browser launched with "+Production+" URL successfully");
			}
		else if(Environment.equalsIgnoreCase("TestRoles"))
			{
				driver.get(TestRoles);
				log.info("Browser launched with "+TestRoles+" URL successfully");
				Reporter.log("Browser launched with "+TestRoles+" URL successfully");
			}
	}

	@AfterMethod
	public void screenShotOnFailedTestCase(ITestResult result) throws IOException {
		if (ITestResult.FAILURE == result.getStatus()) {
			takesScreenshot(driver, "Testcase Failure", result.getName());
			log.info("Screenshot taken on Testcase failure");
		}
	}
	
	/**
	 * After class Method
	 */
	
	@AfterClass
	public void quit()
	{
		try
		{
			Thread.sleep(2000);
			if (driver != null) {
				driver.manage().deleteAllCookies();
				driver.quit();
				driver.close();
			}
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");

		}
		catch (Exception anException)
		{
			anException.printStackTrace();
		}
	}

}




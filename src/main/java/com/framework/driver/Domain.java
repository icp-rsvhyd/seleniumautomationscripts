package com.framework.driver;


import java.util.ArrayList;

import java.util.Random;
/**
 * 
 * @author SecureT Concepts Pvt Ltd
 *
 */
public class Domain{



	public ArrayList<String> defaultResponses;
	public Random randomGenerator;

	/**
	 * This constructor will be used to initialize  random email domain for registration purpose.
	 */
    public Domain()
    {
   
        defaultResponses = new ArrayList<String>();
        fillDefaultResponses();
        randomGenerator = new Random();
    }

    /**
     * This method will be used to create a random email domains (.com, .net, .org etc) for registration purpose.
     */
    public void fillDefaultResponses()
    {
        defaultResponses.add("com");
        defaultResponses.add("net");
        defaultResponses.add("org");
        defaultResponses.add("in");
        defaultResponses.add("co");
        defaultResponses.add("edu");
        
    }
    /**
     * It will pickup the generated Domain names randomly.
     * @return defaultResponses - returns the next integer on index of defaultResponses 
     */

    public String pickDomain()
    {

        int index = randomGenerator.nextInt(defaultResponses.size());
        return defaultResponses.get(index);
    }
	
 
  
 }
	


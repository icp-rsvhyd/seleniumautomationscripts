package com.framework.driver;

import static com.framework.driver.CommonMethod.sleep;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import mx4j.log.Log;

/**
 * CommonMethod class
 * 
 * @author SecureT Concepts Pvt Ltd
 */
public class CommonMethod {

	public WebDriver driver;

	public static WebElement findElement(final WebDriver driver, String objectLocater) throws IOException {
		Properties OR = new Properties();
		String pathLocation = Xml_Reader.getPathFromXml("ProjectLocatorProperties");
		FileInputStream fp = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\main\\resources\\" + pathLocation);
		OR.load(fp);
		String objecttypeandvalues = OR.getProperty(objectLocater);
		System.out.println(objecttypeandvalues);
		String[] splits = objecttypeandvalues.split("~");
		String objecttype = splits[0];
		System.out.println("obj type: " + objecttype);
		String objectvalue = splits[1];
		System.out.println("obj val: " + objectvalue);

		switch (objecttype) {

		case "id":

			return driver.findElement(By.id(objectvalue));

		case "xpath":

			return driver.findElement(By.xpath(objectvalue));

		case "name":

			return driver.findElement(By.name(objectvalue));

		case "class":

			return driver.findElement(By.className(objectvalue));

		case "tagname":

			return driver.findElement(By.tagName(objectvalue));

		case "css":

			return driver.findElement(By.cssSelector(objectvalue));

		default:

			return null;

		}

		/*
		 * if (objecttype.equalsIgnoreCase("id")) { return
		 * driver.findElement(By.id(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("xpath")) { return
		 * driver.findElement(By.xpath(objectvalue));
		 * 
		 * } else if (objecttype.equalsIgnoreCase("name")) { return
		 * driver.findElement(By.name(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("class")) { return
		 * driver.findElement(By.className(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("tagname")) { return
		 * driver.findElement(By.tagName(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("css")) { return
		 * driver.findElement(By.cssSelector(objectvalue)); }
		 * 
		 * return null;
		 */

	}

	/**
	 * Click Operation
	 * 
	 * @param driver
	 * @param objectLocater
	 * @throws IOException
	 */
	public static void click(WebDriver driver, String objectLocater) throws IOException {
		findElement(driver, objectLocater).click();
	}

	/**
	 * checkbox Operation
	 * 
	 * @param driver
	 * @param objectLocater
	 * @throws IOException
	 */
	public static void selectCheckBox(WebDriver driver, String objectLocater) throws Exception {
		findElement(driver, objectLocater).click();
	}

	/**
	 * Sendkeys Operation
	 * 
	 * @param driver
	 * @param objectLocater
	 * @param value
	 * @throws IOException
	 */
	public static void sendKeys(WebDriver driver, String objectLocater, String value) throws IOException {
		findElement(driver, objectLocater).sendKeys(value);
	}

	/**
	 * Gettext Method
	 * 
	 * @param driver
	 * @param objectLocater
	 * @return
	 * @throws IOException
	 */
	public static String getText(WebDriver driver, String objectLocater) throws IOException {

		return findElement(driver, objectLocater).getText();

	}
	
	public static String getAttribute(WebDriver driver, String objectLocater) throws IOException {

		return findElement(driver, objectLocater).getAttribute("value");

	}

	/**
	 * Clear Operation
	 * 
	 * @param driver
	 * @param objectLocater
	 * @throws IOException
	 */
	public static void clear(WebDriver driver, String objectLocater) throws IOException {
		findElement(driver, objectLocater).clear();
	}

	/**
	 * Select drop down
	 * 
	 * @param driver
	 * @param by
	 * @param value
	 * @throws IOException
	 * 
	 */
	public static void selectDropdown(WebDriver driver, String objectLocater, String value) throws IOException {
		new Select(findElement(driver, objectLocater)).selectByVisibleText(value);
	}

	/**
	 * Method for Taking screenshot Screenshot
	 * 
	 * @param driver
	 * @param ScreenshotName
	 * @throws IOException
	 */
	public static void takesScreenshot(WebDriver driver, String ScreenshotName, String message) throws IOException {
		String str = TestBase.browserName;
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');

		// driver = new Augmenter().augment(driver);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "\\Screenshots\\" + str + "_"
				+ ScreenshotName + "_" + message + "_" + timeStamp + ".jpg"));

		String screen_shot_path = System.getProperty("user.dir") + "\\Screenshots\\" + str + "_" + ScreenshotName + "_"
				+ message + "_" + timeStamp;
		Reporter.log("<td><a href='" + screen_shot_path + ".jpg'><img src='" + screen_shot_path
				+ ".jpg' height='100' width='100' /></a></td>");
	}

	/**
	 * Handling New Tab from Parent
	 * 
	 * @param driver
	 */
	public static void handleNewTab(WebDriver driver) {
		Set<String> allWindowHandles = driver.getWindowHandles();
		Iterator<String> iter = allWindowHandles.iterator();
		int size = allWindowHandles.size();
		String window0 = null;
		for (int i = 0; i < size; i++) {
			window0 = iter.next();
		}

		driver.switchTo().window(window0);
	}

	/**
	 * Handling Parent tab
	 * 
	 * @param driver
	 */
	public static void handleParentTab(WebDriver driver) {

		Set<String> allWindowHandles = driver.getWindowHandles();
		String window0 = (String) allWindowHandles.toArray()[0];
		driver.switchTo().window(window0);

	}

	/**
	 * Handling childParent tab
	 * 
	 * @param driver
	 */
	public static void handleChildParentTab(WebDriver driver) {

		Set<String> allWindowHandles = driver.getWindowHandles();
		String window1 = (String) allWindowHandles.toArray()[1];
		driver.switchTo().window(window1);

	}

	/**
	 * Method to verify if an element is on the page
	 * 
	 * @param driver
	 * @param objectLocater
	 * @return
	 * @throws IOException
	 */

	public static boolean isElementPresent(final WebDriver driver, List<WebElement> objectLocater) throws IOException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (objectLocater.size() != 0) {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return true;

		} else {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return false;
		}
	}

	/**
	 * Compact way to verify if an element is on the page
	 * 
	 * @param driver
	 * @throws IOException
	 */
	public static boolean isElementPresent(final WebDriver driver, String objectLocater) throws IOException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (findElements(driver, objectLocater).size() != 0) {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return true;

		} else {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return false;
		}
	}

	/**
	 * For getting get attribut value from field
	 * 
	 * @param driver
	 * @param objectLocater
	 * @return
	 * @throws IOException
	 */
	public static String getValue(WebDriver driver, String objectLocater) throws IOException {
		return findElement(driver, objectLocater).getAttribute("value");

	}

	// Explicit wait for perticular object locator
	/**
	 * Explicit wait for perticular object locator
	 * 
	 * @param driver
	 * @param timeOutInSeconds
	 * @param objectLocater
	 * @throws IOException
	 */
	public static void explicitWait(WebDriver driver, int timeOutInSeconds, String objectLocater) throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		Properties OR = new Properties();
		String pathLocation = Xml_Reader.getPathFromXml("ProjectLocatorProperties");
		FileInputStream fp = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\main\\resources\\" + pathLocation);
		OR.load(fp);
		String objecttypeandvalues = OR.getProperty(objectLocater);

		String[] splits = objecttypeandvalues.split("~");
		String objecttype = splits[0];
		System.out.println("obj type: " + objecttype);
		String objectvalue = splits[1];
		System.out.println("obj val: " + objectvalue);

		if (objecttype.equalsIgnoreCase("id")) {

			wait.until(ExpectedConditions.elementToBeClickable(By.id(objectvalue)));

		} else if (objecttype.equalsIgnoreCase("xpath")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(objectvalue)));

		} else if (objecttype.equalsIgnoreCase("name")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.name(objectvalue)));
		} else if (objecttype.equalsIgnoreCase("class")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.className(objectvalue)));
		} else if (objecttype.equalsIgnoreCase("tagname")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.tagName(objectvalue)));
		} else if (objecttype.equalsIgnoreCase("css")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(objectvalue)));
		}

	}

	/**
	 * Handling alerts and popups
	 * 
	 * @param WebDriver
	 * @return
	 */
	public static void handleAlert(final WebDriver driver) {
		String message = null;
		try {
			Alert alert = driver.switchTo().alert();
			message = alert.getText();
			System.out.println("Alert Message is "+message);
			alert.accept();

		} catch (Exception e) {
			System.out.println("Alert error Messagae is"+e.getMessage());

		}

	}
	
	/**
	 * Handling alerts messages and popups
	 * 
	 * @param WebDriver
	 * @return 
	 * @return
	 */
	public static String handleAlertMessage(final WebDriver driver) {
		String message = null;
		try {
			Alert alert = driver.switchTo().alert();
			message = alert.getText();
			System.out.println("Alert Message is "+message);
			alert.accept();
			//return message;

		} catch (Exception e) {
			System.out.println("Alert error Messagae is "+e.getMessage());

		}
		return message;

	}
	
	public static boolean isAlertPresent(final WebDriver driver) 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }  
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}   // isAlertPresent()

	public static void switchToAlert(final WebDriver driver) {
		String message = null;
		try {
			Alert alert = driver.switchTo().alert();
			// message = alert.getText();
			// alert.accept();
		} catch (Exception e) {
			System.out.println("Driver unable to click Swith to Popup Alert");
		}

	}

	public static String cancelPopupMessageBox(final WebDriver driver) {
		String message = null;
		try {
			Alert alert = driver.switchTo().alert();

			message = alert.getText();
			alert.dismiss();
		} catch (Exception e) {
			message = null;
		}

		return message;
	}

	/**
	 * Compact way to verify if an element is on the page
	 * 
	 * @param driver
	 * @throws IOException
	 */

	/**
	 * List Web elements
	 * 
	 * @param driver
	 * @param object
	 * @return
	 * @throws IOException
	 */
	public static List<WebElement> findElements(final WebDriver driver, String objectLocater) throws IOException {
		Properties OR = new Properties();
		String pathLocation = Xml_Reader.getPathFromXml("ProjectLocatorProperties");
		FileInputStream fp = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\main\\resources\\" + pathLocation);

		OR.load(fp);

		String objecttypeandvalues = OR.getProperty(objectLocater);

		String[] splits = objecttypeandvalues.split("~");
		String objecttype = splits[0];
		System.out.println("obj type: " + objecttype);
		String objectvalue = splits[1];
		System.out.println("obj val: " + objectvalue);

		switch (objecttype) {

		case "id":
			return driver.findElements(By.id(objectvalue));

		case "xpath":

			return driver.findElements(By.xpath(objectvalue));

		case "name":

			return driver.findElements(By.name(objectvalue));

		case "class":

			return driver.findElements(By.className(objectvalue));

		case "tagname":

			return driver.findElements(By.tagName(objectvalue));

		case "css":

			return driver.findElements(By.cssSelector(objectvalue));
		default:

			return null;

		}

		/*
		 * if (objecttype.equalsIgnoreCase("id")) {
		 * 
		 * return driver.findElements(By.id(objectvalue));
		 * 
		 * } else if (objecttype.equalsIgnoreCase("xpath")) { return
		 * driver.findElements(By.xpath(objectvalue));
		 * 
		 * } else if (objecttype.equalsIgnoreCase("name")) { return
		 * driver.findElements(By.name(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("class")) { return
		 * driver.findElements(By.className(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("tagname")) { return
		 * driver.findElements(By.tagName(objectvalue)); } else if
		 * (objecttype.equalsIgnoreCase("css")) { return
		 * driver.findElements(By.cssSelector(objectvalue)); } return null;
		 */

	}

	/**
	 * Check hover message text(Tool Tip)
	 * 
	 * @param driver
	 * @return string
	 * @throws IOException
	 */
	public static String checkHoverMessage(WebDriver driver, String objectLocater) throws IOException {
		String tooltip = findElement(driver, objectLocater).getAttribute("title");
		System.out.println(tooltip);
		return tooltip;
	}

	/**
	 * Upload file with AutoIT
	 * 
	 * @param driver
	 * @param value
	 * @throws IOException
	 * 
	 */
	public static void uploadFileWithAutoIT(WebDriver driver, String fileName) throws IOException {
		//findElement(driver, objectLocater).sendKeys(value);
		//String fileName=datatable.getCellData("OpenClinica", "FileName", 2).trim(); //  passed as a command line parameter to AutoIT executable below
		String autoITExecutable=System.getProperty("user.dir")+"\\src\\main\\resources\\AutoITFileUploadWithParam.exe "+fileName;
		System.out.println(autoITExecutable+" AutoIT executable file ");
		try {
		    Runtime.getRuntime().exec(autoITExecutable);
		    sleep(2, TimeUnit.SECONDS);
		    System.out.println("Ran AutoIT script to upload : " + fileName);
		} catch (Exception e) {
			System.out.println("Failed to run AutoIT script : " + e.getMessage());
		}
	}
	
	public static void setDownloadPathWithAutoIT(WebDriver driver, String fileName) throws IOException {
		//findElement(driver, objectLocater).sendKeys(value);
		//String fileName=datatable.getCellData("OpenClinica", "FileName", 2).trim(); //  passed as a command line parameter to AutoIT executable below
		String autoITExecutable=System.getProperty("user.dir")+"\\src\\main\\resources\\DownloadPath.exe "+fileName;
		System.out.println(autoITExecutable+" AutoIT executable file ");
		try {
		    Runtime.getRuntime().exec(autoITExecutable);
		    sleep(2, TimeUnit.SECONDS);
		    System.out.println("Ran AutoIT script to upload : " + fileName);
		} catch (Exception e) {
			System.out.println("Failed to run AutoIT script : " + e.getMessage());
		}
	}

	/**
	 * Downloads a file from the defined url, and saves it into the
	 * OutputDatafolder, using the filename
	 * 
	 * @param href
	 * @param fileName
	 */
	public static void downloadFile(String href, String fileName) throws Exception {

		URL url = null;
		URLConnection con = null;
		int i;

		url = new URL(href);

		con = url.openConnection();
		File file = new File(".//OutputData//" + fileName);
		BufferedInputStream bis = new BufferedInputStream(con.getInputStream());

		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
		while ((i = bis.read()) != -1) {
			bos.write(i);
		}
		bos.flush();
		bis.close();

	}

	/**
	 * Remove space between String
	 * 
	 * @param str
	 * @return
	 */
	public static String removeBetweenSpaceFromString(String str) {

		String st = str.replaceAll("\\s+", "");
		return st;
	}

	/**
	 * Method for Taking screenshot Screenshot
	 * 
	 * @param driver
	 * @param ScreenshotName
	 * @throws IOException
	 */
	public static void takesScreenshotOnFailure(WebDriver driver, String ScreenshotName, String message)
			throws IOException {
		String str = TestBase.browserName;
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');
		// driver = new Augmenter().augment(driver);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "\\Screenshots\\FailureScreenshots\\"
				+ str + "_" + ScreenshotName + "_" + message + "_" + timeStamp + ".jpg"));
		String screen_shot_path = System.getProperty("user.dir") + "\\Screenshots\\FailureScreenshots\\" + str + "_"
				+ ScreenshotName + "_" + message + "_" + timeStamp;
		Reporter.log("<td><a href='" + screen_shot_path + ".jpg'><img src='" + screen_shot_path
				+ ".jpg' height='100' width='100' /></a></td>");
	}


	public void handleSliders(WebDriver driver, String objectLocater) throws Exception {

		// Locate slider pointer.
		WebElement dragElementFrom = CommonMethod.findElement(driver, objectLocater);
		new Actions(driver).dragAndDropBy(dragElementFrom, 70, 0).build().perform();
		Thread.sleep(5000);
		new Actions(driver).clickAndHold(dragElementFrom).moveByOffset(70, 0).release().perform();

	}

	/**
	 * Method for Handling mouse Movements
	 * 
	 * @param driver
	 * @throws IOException
	 */
	public static void mouseMoveToElement(WebDriver driver, String objectLocater) throws IOException {

		WebElement element = CommonMethod.findElement(driver, objectLocater);
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();

	}

	public static void keyPress(WebDriver driver, Keys keyName) {

		Actions action = new Actions(driver);
		action.keyDown(keyName).build().perform();
	}

	public static void keyRelease(WebDriver driver, Keys keyName) {
		Actions action = new Actions(driver);
		action.keyUp(keyName).build().perform();
	}
	
	/**
     * Method for Handling Keyevents for KeyPress
     *  @param keyName to press (e.g. <code>KeyEvent.VK_A</code>)
     */
	public static void robotKeyPress(WebDriver driver, int keyName) throws AWTException {

		Robot robot = new Robot();
		robot.keyPress(keyName);
		/*robot.keyPress(KeyEvent.VK_MINUS);
		robot.keyDown(keyName).build().perform();*/
	}

	public static void robotKeyRelease(WebDriver driver, int keyName) throws AWTException {
		
		Robot robot = new Robot();
		robot.keyRelease(keyName);
		/*Actions action = new Actions(driver);
		action.keyUp(keyName).build().perform();*/
	}
	

	public static void sendKeys(WebDriver driver, String objectLocater, Keys keyName) throws IOException {
		findElement(driver, objectLocater).sendKeys(keyName);
	}
	
	

	public static void allOptionsSize(WebDriver driver, String objectLocater) {

	}

	public static void sleep(long timeout, TimeUnit timeUnit) {
		try {
			timeUnit.sleep(timeout);
		} catch (InterruptedException ie) {
			System.err.println(ie.getMessage());
		}
	}

}

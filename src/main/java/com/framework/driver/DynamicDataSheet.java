package com.framework.driver;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author AllSmart TechnoSolutions Pvt Ltd
 *
 */

public class DynamicDataSheet {

	public static final String ALPHA_CAPS  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String ALPHA   = "abcdefghijklmnopqrstuvwxyz";
    public static final String NUM     = "0123456789";
    public static final String SPL_CHARS   = "@$";
 
    public static ArrayList<String> defaultResponses;
	public static Random randomGenerator;

	public static Domain d=new Domain();
	
    
   /**
    * Generation of randomvalue based on provided length of string including no of Capital letters and no of digits and no of special characters.
    * 
    * @param minLen
    * @param maxLen
    * @param noOfCAPSAlpha
    * @param noOfDigits
    * @param noOfSplChars
    * @return pswd Password
    */

    public static char[] generateRandomValue(int minLen, int maxLen, int noOfCAPSAlpha, int noOfDigits,int noOfSplChars)
    {
    	
        if(minLen > maxLen)
            throw new IllegalArgumentException("Min. Length > Max. Length!");
        if( (noOfCAPSAlpha + noOfDigits +noOfSplChars) > minLen )
            throw new IllegalArgumentException
            ("Min. Length should be atleast sum of (CAPS, DIGITS, SPL CHARS) Length!");
        Random rnd = new Random();
        int len = rnd.nextInt(maxLen - minLen + 1) + minLen;
        char[] pswd = new char[len];
        int index = 0;
        for (int i = 0; i < noOfCAPSAlpha; i++)
        {
            index = getNextIndex(rnd, len, pswd);
            pswd[index] = ALPHA_CAPS.charAt(rnd.nextInt(ALPHA_CAPS.length()));
        }
        for (int i = 0; i < noOfDigits; i++) 
        {
            index = getNextIndex(rnd, len, pswd);
            pswd[index] = NUM.charAt(rnd.nextInt(NUM.length()));
        }
        for (int i = 0; i < noOfSplChars; i++) 
        {
            index = getNextIndex(rnd, len, pswd);
            pswd[index] = SPL_CHARS.charAt(rnd.nextInt(SPL_CHARS.length()));
        }
        for(int i = 0; i < len; i++)
        {
            if(pswd[i] == 0) 
            {
                pswd[i] = ALPHA.charAt(rnd.nextInt(ALPHA.length()));
            }
        }
        return pswd;
    }
 
    /**
     * To get next index value while creating the dynamic testdata.
     * 
     * @param rnd
     * @param len
     * @param pswd
     * @return index
     */
    public static int getNextIndex(Random rnd, int len, char[] pswd) {
        int index = rnd.nextInt(len);
        while(pswd[index = rnd.nextInt(len)] != 0);
        return index;
    }

    public static String generateEmail()
    {
   	 String email=null;
   	 char[] name = generateRandomValue(3, 20, 1, 1, 0);
   	 char[] domain = generateRandomValue(3, 20, 1, 1, 0);
   	
   	 email=new String(name)+"@"+new String(domain)+"."+d.pickDomain();
   	 
   	return email;
    }
    
 
    /**
     * Generate random MobileNumber
     * @return
     */
      public static String generateMobileNumber()
      {
     	 String mobileNumber=null;
     	 char[] number = generateRandomValue(9, 9, 0, 9, 0);
     	
     	mobileNumber= "9"+new String(number);
     	 
     	return mobileNumber;
      }
    
    
    
 }
   
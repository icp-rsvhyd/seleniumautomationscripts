package com.framework.driver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import bsh.classpath.BshClassPath.GeneratedClassSource;

public class MainConfig {

	static Properties properties = null;

	private static void init() throws IOException {
		InputStream is = MainConfig.class.getResourceAsStream("/mainconfig.properties");
		properties = new Properties();
		properties.load(is);
	}

	public static String getProperty(String key) {
		if (properties == null) {
			try {
				init();
			} catch (Exception e) {
				properties = null;
				e.printStackTrace();
				return null;
			}
		}
		return properties.getProperty(key);
	}

	public static ChromeDriver getChromeDriver(ChromeOptions options) {
		String chromedriverPath = System.getProperty("webdriver.chrome.driver");
		if (chromedriverPath == null) {
			String os = System.getProperty("os.name").toLowerCase();
			String filename = "chromedriver.exe";
			if (os.contains("win")) {
				filename = "chromedriver.exe";
			} else if (os.contains("osx")) {
				// Operating system is Apple OSX based
			} else if (os.contains("nix") || os.contains("aix") || os.contains("nux")) {
				filename = "chromedriver";
			}
			URL url = MainConfig.class.getResource("/GridConfiguration/" + filename);
			chromedriverPath = url.getFile();
		}
		File file = new File(chromedriverPath); // Strangely, URL.getFile does not return a File
		ChromeDriverService.Builder bldr = (new ChromeDriverService.Builder()).usingDriverExecutable(file)
				.usingAnyFreePort();
		ChromeDriver driver = new ChromeDriver(bldr.build(),options);
		return driver;
	}

	public static void main(String[] args) {
		System.out.println(getProperty("ProjectName"));
		// System.setProperty("webdriver.chrome.driver",
		// "/usr/lib/chromium-browser/chromedriver");
		  ChromeOptions options = new ChromeOptions();
		  options.addArguments("--disable-notifications");
		  options.addArguments("incognito");
		  options.addArguments("--disable-extensions");
		WebDriver driver = getChromeDriver(options);

		driver.get("https://mvnrepository.com");

		driver.close();

		/*
		 * System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
		 * ChromeDriverService service = new ChromeDriverService.Builder()
		 * .usingDriverExecutable(new File("/usr/local/bin/chromedriver"))
		 * .usingAnyFreePort() .build(); try { service.start(); } catch (IOException e)
		 * { e.printStackTrace(); } return new RemoteWebDriver(service.getUrl(),
		 * DesiredCapabilities.chrome());
		 */
	}
}

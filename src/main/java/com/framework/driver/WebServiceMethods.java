/*package com.framework.driver;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
//import org.json.*;
import org.testng.annotations.BeforeClass;


	public class WebServiceMethods {
		
	 	public static XlsReader webdatatable=null;
		int row;
		int rowcount;
		public String URL=null;
		public URL url;
		public HttpURLConnection conn ;
		public static int ResponseCode;
		public  String Title=null;
		public  String EmployeeId=null;
		public  String CreatedBy=null;
		public  String CreatedEmpId=null;
		public  String CreatedOn=null;
		public  String StartDate=null;
		public  String EndDate=null;
		public  String ModifiedOn=null;
		public  String ModifiedBy=null;
		
		@BeforeClass
		public void initialize() throws Exception{
			
			String webServiceexcelpathLocation=Xml_Reader.getPathFromXml("ProjectWebServiceExcelProperties");
			webdatatable=new XlsReader(System.getProperty("user.dir")+"\\src\\main\\resources\\"+webServiceexcelpathLocation);
			 
		}
		public void request(String Sheetname,String Methodname) throws Exception{
			
		try{
			
			row=webdatatable.rowCount(Methodname);
			rowcount=row+1;	
			URL=webdatatable.getCellData(Sheetname, "WebServiceURL", 2)+webdatatable.getCellData(Sheetname, "MethodURL", rowcount);	
			System.out.println(URL);
			url = new URL(URL);
			conn = (HttpURLConnection) url.openConnection();
			String methodType=webdatatable.getCellData(Sheetname, "WebServiceType", rowcount);
			conn.setRequestMethod(methodType);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
		}
			
		catch(Exception e){
				System.out.println(e.getLocalizedMessage());
		}
			
			
			
		}
		
		
		public String  getResponseInfo(String Sheetname) throws IOException{
			 ResponseCode=conn.getResponseCode();
			  if (ResponseCode!=200) {
				throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode());
			  }
			
			Scanner scan = new Scanner(conn.getInputStream());
			String str = new String();
			try{
			while (scan.hasNext())
			str += scan.nextLine();
			scan.close();
			System.out.println("str : " + str);
			conn.disconnect();
			
			webdatatable.setCellData(Sheetname, "ResponseValue", rowcount, str);
			}
			catch(Exception e){
				System.out.println(e.getLocalizedMessage());
			}
			return str;
			
		}
		
	
		//public void passingparameters() throws JSONException, IOException{
			
			 //Object[] assignedPersons =new Object[] {"7f9da777-d3de-400b-8c11-57af2a6277c4","8bb13b48-b1ba-4180-bb4f-a1274485c51f"};
		     //Object[] tagnames =new Object[] {"Apple","Creating-TestTask"};
			// JSONObject jsonParam = new JSONObject();
		     jsonParam.put("Title", Title);
		     jsonParam.put("EmployeeId", EmployeeId);
		     jsonParam.put("CreatedBy", CreatedBy );
		     jsonParam.put("CreatedEmpId", CreatedEmpId );
		     jsonParam.put("CreatedOn", CreatedOn);
		     jsonParam.put("StartDate",StartDate);
		     jsonParam.put("EndDate", EndDate);
		     //jsonParam.put("AssignedToEmpIds",assigned("CreateTask", "AssignedToEmpIds"));
		     //jsonParam.putOpt("TagNames", assigned("CreateTask", "Tags"));
		     conn.setDoOutput(true);
		     OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
		     out.write(jsonParam.toString());
		     System.out.println(jsonParam.toString());
		     out.flush ();
		     out.close (); 
		     ResponseCode=conn.getResponseCode();
		     System.out.println(ResponseCode);
		}
		
		public void genaratingDynamicParametersToService(String sheetName){
			
			Title=webdatatable.getCellData(sheetName, "Title",2);
			System.out.println(Title);
			EmployeeId=webdatatable.getCellData(sheetName, "EmployeeId", 2);
			System.out.println(EmployeeId);
			CreatedBy=webdatatable.getCellData(sheetName, "CreatedBy", 2);
			System.out.println(CreatedBy);
			CreatedEmpId=webdatatable.getCellData(sheetName, "EmployeeId", 2);
			System.out.println(CreatedEmpId);
			CreatedOn=webdatatable.getCellData(sheetName, "CreatedOn", 2);
			System.out.println(CreatedOn);
			StartDate=webdatatable.getCellData(sheetName, "StartDate", 2);
			System.out.println(StartDate);
			EndDate=webdatatable.getCellData(sheetName, "EndDate", 2);
			System.out.println(EndDate);
			ModifiedOn=webdatatable.getCellData(sheetName, "ModifiedOn", 2);
			System.out.println(ModifiedOn);
			ModifiedBy=webdatatable.getCellData(sheetName, "ModifiedBy", 2);
			System.out.println(ModifiedBy);
			
			}
		
		public ArrayList<String> assigned(String Sheetname,String Colname){
			int count=webdatatable.getRowCount(Sheetname);
			
			ArrayList<String> al= new ArrayList<String>();
			for(int i=2;i<=count;i++)
			{
		
			String actual =webdatatable.getCellData(Sheetname,Colname, i);
			al.add(actual);
			//System.out.println(al.size());
			
			 }
			return al;
			
		}
		
		
	
		
		
		
		
		
	
	
	
	
}
*/

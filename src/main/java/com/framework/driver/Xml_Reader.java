package com.framework.driver;


import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Xml_Reader {

	static NodeList fstNm;
 
public static String  getPathFromXml(String configPath){
	
	 
	try {
	  
		  	File file = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\XMLConfig.xml");
//		File file = new File("src/main/resources/XMLConfig.xml");
		  	//System.out.println("Files exists? " + file.exists());
		  	DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		  	DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		  	Document document = documentBuilder.parse(file);
		  	document.getDocumentElement().normalize();
		  	NodeList nodeList = document.getElementsByTagName("Configurations");
		  	for (int i = 0; i < nodeList.getLength(); i++) {
		  		Node node = nodeList.item(i);
		  		if (node.getNodeType() == Node.ELEMENT_NODE) {
		  			Element element = (Element) node;
		  			//System.out.println("Element: " + element);
		  			NodeList fstNmElmntLst = element.getElementsByTagName(configPath);
		  			Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
		  			//System.out.println("fstElement: " + fstNmElmnt);
		  			fstNm = fstNmElmnt.getChildNodes();
	      //	System.out.println(configPath +" : "  + ((Node) fstNm.item(0)).getNodeValue());
		  			}
		  		}
		  	} catch (Exception e) {
		  		e.printStackTrace();
		  		}
	
	DocumentBuilderFactory documentBuilderFactory = null;
    DocumentBuilder documentBuilder = null;
    Document document = null;
    documentBuilderFactory = DocumentBuilderFactory.newInstance();
    try {
         documentBuilder = documentBuilderFactory.newDocumentBuilder();
         } catch (ParserConfigurationException ex) {
        	 System.err.println("Failed to configure parser " + ex.getLocalizedMessage());
        	 }
    try {
    	 File file = new File("src/main/resources/XMLConfig.xml");
    	 //System.out.println("File is found? " + file.exists());
         document = documentBuilder.parse(file);
         document.getDocumentElement().normalize();
         NodeList nodeList = document.getElementsByTagName("Configurations");
        // System.out.println("Nodes available? " + nodeList.getLength());
         //System.out.println("NodeList? " + nodeList);
         for(int i=0; i<nodeList.getLength(); i++) {
        	 Node configurationsNode = nodeList.item(i); 
        	 if(configurationsNode.getNodeType() == Node.ELEMENT_NODE) {
        		// System.out.println("Looping");
        		 Element configurationsElement = (Element) configurationsNode;
        		 NodeList configurationsChildNodes = configurationsElement.getChildNodes();
        		// System.out.println("--->"+configurationsChildNodes.getLength());
        		 for (int j = 0; j < configurationsChildNodes.getLength(); j++) {
                     Node childNode = configurationsChildNodes.item(j);
                     if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                         Element childElement = (Element)childNode;
//                         System.out.println("Element Name: " + childElement.getNodeName()+" Text Content: " + childElement.getTextContent());
                     }
                 }
        	 }
         }
//         Element element = document.getDocumentElement();
//         System.out.println("root element? " + element.getNodeName());
         Node node = nodeList.item(0);
        // System.out.println("Node==? " + node);
     } catch (IOException | SAXException ex) {
         System.err.println(ex.getMessage());
     }
    finally {
         documentBuilderFactory = null;
         documentBuilder = null;
         document = null;
    }
	return ((Node) fstNm.item(0)).getNodeValue();
		 	  
 }


/*public static void main(String argv[]) {
	Xml_Reader Xml_Reader = new Xml_Reader();
	Xml_Reader.getPathFromXml("ProjectLocatorProperties");
	
}
*/

 
 
 
 
 
}
package com.framework.utilities;

import org.testng.IExecutionListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListners implements ITestListener {

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println(result.getName()+" Test Case execution started");
	}


	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println(result.getName()+" TestCase successfully executed");
	}


	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println(result.getName()+" TestCase execution failed");
	}


	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println(result.getName()+" TestCase Skipped");
	}


	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println(result.getName()+" nothing");
	}


	@Override
	public void onStart(ITestContext result) {
		System.out.println(result.getName()+" TestCases execution Started");
	}


	@Override
	public void onFinish(ITestContext result) {
		System.out.println(result.getName()+" TestCases execution Completed");
	}
	
	
	
	SendMailReports mail = new SendMailReports();

	/*@Override
	public void onExecutionStart() {

		System.out.println("Starting Execution of Testcases");

	}

	@Override
	public void onExecutionFinish() {
		try {

			// mail.zipAutomationReports();
			// mail.sendmail();
			System.out.println("Execution has been Completed");
		}

		catch (Exception e) {
			e.printStackTrace();

		}

	}*/

}

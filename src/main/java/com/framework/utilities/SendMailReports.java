package com.framework.utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.framework.driver.MainConfig;

public class SendMailReports

{

	static List<String> filesListInDir = new ArrayList<String>();

	File filename = null;
	ZIPFile Zip = new ZIPFile();
	// ZIPFile Zip1 = new ZIPFile();

	public void zipAutomationReports()
			throws InterruptedException, ParserConfigurationException, IOException, SAXException {
		filename = new File(System.getProperty("user.dir") + "\\AutomationReports");
		Zip.zipDirectory(filename, "AutomationReports.zip");
		System.out.println("Zipping of AutomationReports successfully");
		Thread.sleep(3000);

	}

	// @Test(priority=4)
	public void zipfailureScreenshots() throws InterruptedException {
		filename = new File(System.getProperty("user.dir") + "\\screenshots\\FailureScreenshots");
		// Zip1.zipDirectory(filename, "FailureScreenshots.zip");
		System.out.println("Zipping of Failure Screenshots successfully");
	}

	public void sendmail()

	{
		String username = MainConfig.getProperty("MailUsername");
		String password = MainConfig.getProperty("MailPassword");
		String toadd = MainConfig.getProperty("MailTo");
		String ccadd = MainConfig.getProperty("MailCc");
		String bccadd = MainConfig.getProperty("MailBcc");
		String subject = MainConfig.getProperty("MailSubject");

		String[] to = { toadd };
		String[] cc = { ccadd };
		String[] bcc = { bccadd };
		MailUtility.sendMail(username, password, "smtp.gmail.com", "465", "true", "true", true,
				"javax.net.ssl.SSLSocketFactory", "false", to, cc, bcc, subject, "PFA",
				System.getProperty("user.dir") + "\\AutomationReports.zip", "AutomationReports.zip");

		System.out.println("Report has been sent through mail Successfully");

	}

}

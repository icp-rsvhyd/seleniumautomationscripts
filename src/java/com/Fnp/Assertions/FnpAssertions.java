package com.Fnp.Assertions;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class FnpAssertions  extends TestBase{
	
	
      public static void verifyFnpHomePage() throws IOException
      {
		
		CommonMethod.explicitWait(driver, 10, "Fnp_Home_Title");
		String actual = CommonMethod.getText(driver, "Fnp_Home_Title");
		
		Assert.assertEquals(actual, "Registered User");
		CommonMethod.takesScreenshot(driver, "FNPHOMETITLE", "Sucessfull");
		
		
	}
	
    public static void verifyLogin() throws IOException{
    	
    	driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    	String str = CommonMethod.getText(driver, "Fnp_AfterLogin");
    	Assert.assertEquals(str, "Catalog Administration Main Page");
    	CommonMethod.takesScreenshot(driver, "verifyLogin", "successfull");
    }
      
      
      
      
      
      
    public static void searchFlight(WebDriver driver) throws IOException
    {
        Assert.assertEquals(CommonMethod.getText(driver, "Search_Flights"), "Search flights");
    	CommonMethod.takesScreenshot(driver, "verifySearchFlight", "Success");
    		
    }
    
    
     public static void verifyLogin(WebDriver driver) throws IOException
    {
		if(CommonMethod.getText(driver, "Login_Homepage").contains("Login"))
		{
			System.out.println("login link display sucessfull");
			CommonMethod.takesScreenshot(driver, "loginlink", "Success");
		}
		else
		{
			System.out.println("login link Failed");
			CommonMethod.takesScreenshotOnFailure(driver, "loginlink", "Failed");
			throw new RuntimeException("login Failed");

        }
    }

    
    public static void verifyCentraLoginpage(WebDriver driver) throws IOException, InterruptedException
    {
    	
 		if(CommonMethod.isElementPresent(driver, "SignUpButton_Login"))
 		{
 			System.out.println("Login page successfully displayed");
 			CommonMethod.takesScreenshot(driver, "verifyCentraLoginpage", "Success");
 		}
 		else
 		{
 			System.out.println("verifyCentraLoginpage Failed");
 			CommonMethod.takesScreenshotOnFailure(driver, "verifyCentraLoginpage", "Failed");
 			throw new RuntimeException("Centra Login page not displayed");

 		}
 	}
    
    public static void verifyChangeButton(WebDriver driver) throws Exception
    {
 		if(CommonMethod.isElementPresent(driver, "Change_Button"))
 		{
 			
 			System.out.println("flights available for your search.");
 			CommonMethod.takesScreenshot(driver, "verifyChangeButton", "Success");
 			Thread.sleep(6000);
 		}
 		else
 		{
 			System.out.println("No flight search avialable");
 		
 			CommonMethod.takesScreenshotOnFailure(driver, "verifyChangeButton", "Failed");
 			
 			
 			//throw new RuntimeException("Sorry, there aren't any flights available for your search.");

 		}
 	}


     
    
    public static void verifyNonstop(WebDriver driver) throws IOException
     {
    	Assert.assertEquals(CommonMethod.getText(driver, "Verify_NonStop"), "non-stop");
    	CommonMethod.takesScreenshot(driver, "Verify_NonStop", "Success");
     }



     public static void verifyContinueBooking(WebDriver driver) throws IOException
     {
		if(CommonMethod.isElementPresent(driver, "Continue_Booking"))
		{
		    //System.out.println("flights available for your search.");
			CommonMethod.takesScreenshot(driver, "verifyContinueBooking", "Success");
		}
		else
		{
			System.out.println("Not opened continuebooking");
		    CommonMethod.takesScreenshotOnFailure(driver, "verifyContinueBooking", "Failed");
			//throw new RuntimeException("Sorry, there aren't any flights available for your search.");

		}
	 }


    public static void verifyEmailPage(WebDriver driver) throws IOException
  {
	   if(CommonMethod.isElementPresent(driver, "Continue_Button"))
		{
			
			//System.out.println("flights available for your search.");
			CommonMethod.takesScreenshot(driver, "Continue_Button", "Success");
		}
		else
		{
			System.out.println("Not displayed email page");
		
			CommonMethod.takesScreenshotOnFailure(driver, "verifyEmailPage", "Failed");
			
			
			//throw new RuntimeException("Sorry, there aren't any flights available for your search.");

		}
    }
		 
      
        public static void verifyTravels(WebDriver driver) throws IOException
	 {
				if(CommonMethod.getText(driver, "Verify_BaggaegInformation").contains("Baggage Information"))
				{
					System.out.println("Travels form display sucessfull");
					CommonMethod.takesScreenshot(driver, "verifyTravels", "Success");
				}
				else
				{
					System.out.println("Travelsl page not displayed");
					CommonMethod.takesScreenshotOnFailure(driver, "verifyTravels", "Failed");
					throw new RuntimeException("Verify_BaggaegInformation Failed");

				}
		
     }
		  
		 
      public static void verifyBookingReview(WebDriver driver) throws IOException
	 {
				if(CommonMethod.getText(driver, "Block_BookingReview").contains("Block"))
				{
					System.out.println("BookingBlock displayed sucess");
					CommonMethod.takesScreenshot(driver, "verifyBookingReview", "Success");
				}
				else
				{
					System.out.println("verifyBookingReview failed");
					CommonMethod.takesScreenshotOnFailure(driver, "verifyBookingReview", "Failed");
					throw new RuntimeException("BookingReview Failed");

				}
	}  
		  
		 
      
      public static void verifyPnrSucess(WebDriver driver) throws IOException
	 {
				if(CommonMethod.getText(driver, "Pnr_Sucess").contains("PNR successfully generated."))
				{
					System.out.println("BookingBlock displayed sucess");
					CommonMethod.takesScreenshot(driver, "verifyPnrSucess", "Success");
				}
				else
				{
					System.out.println("verifyPnrSucess failed");
					CommonMethod.takesScreenshotOnFailure(driver, "verifyPnrSucess", "Failed");
					throw new RuntimeException("verifyPnrSucess Failed");

				}
		    }  
		  
      public static void verifyFlightFilter(WebDriver driver) throws IOException
 	 {
 				if(CommonMethod.getText(driver, "Errormsg_FlightsFilter").contains("We couldn't find flights to match your filters"))
 				{
 					System.out.println("We couldn't find flights to match your filters");
 				CommonMethod.takesScreenshot(driver, "verifyTravels", "Success");	
 					
 				}
 				else
 				{
 					System.out.println("fights list available");
 					
 					CommonMethod.takesScreenshot(driver, "verifyFlightFilter", "Success");
 					//throw new RuntimeException("Verify_BaggaegInformation Failed");

 				}
 		
      } 
     

}


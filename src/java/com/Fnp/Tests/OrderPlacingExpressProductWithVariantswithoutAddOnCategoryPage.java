package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class OrderPlacingExpressProductWithVariantswithoutAddOnCategoryPage extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
	@Test
			public void OrderPlacing_ExpressProduct_With_Variants_without_AddOn_CategoryPage(){
				
				log.info("*************** OrderPlacing_ExpressProduct_With_Variants_without_AddOn_CategoryPage ******************* ");
		    	 
		    	 try {
			
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 methods.clickingFlowers_Category();
		    		 methods.filtersFlowersProduct();
		    		 methods.checkingFlower_VariantProduct();
		    		 methods.filters_SelectingCity();
		    		 methods.filters_SelectDate_Timeslot();
		    		 methods.selectingTimeSlots();
		    		 methods.clickAddToCart();
		 			 methods.withoutAddons();
		 			 methods.afterAddingProductclickCartIcon();
		    		 methods.clickProceedToPay();
		    		 methods.fnpLoginPage();
		    		 methods.addNewAddressisPresentorNot_Check();
		    		 methods.addNewAddress();
		    		 methods.clickSaveandContinue();
		    		 methods.iHereByCheckBox();
		    		 methods.clickProceedToPay();
		    		 methods.FNPLogoClick();
		    		 methods.logoutFnp();
		    		 methods.navigatetoHomePage();
		    		 
		    		 
			
			     }catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "OrderPlacing_ExpressProduct_With_Variants_without_AddOn_CategoryPage", "failed");
			    	 try {
				    		methods.navigatetoHomePage();
							methods.logoutFnp();
						} catch (Exception e) {
							e.printStackTrace();
						}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
			
		}
	
	
}

package com.Fnp.Tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class FnpFb_Google_Login extends TestBase {

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
	
	
		
	/**
	 * Verify the "FNP" application
	 * @throws IOException;
	 * @throws InterruptedException 
	 * 
	 */
	
	  @Test(priority=1)
		public void gmailLogin() throws InterruptedException, IOException {
			
	    	 log.info("*************** fnpLogin ******************* ");
	    	 
	    	 try {
	    		 
	    		 
	    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	    		//methods.clickHambergerMenu();
	 			methods.clickSubCategory();
	 			methods.scrollingToBottomofAPage();
	 			methods.selectFirstFlowerItem();
	 			methods.enterCityName();
	 			methods.selectingDate();
	 			/*methods.selectingTimeSlots();
	 			methods.selectingTimeSlots_ExpressDeliveryFreeTime();
	 			methods.clickAddToCart();*/
	 			methods.selectAddons();
	 			methods.clickContinueBtn();
	 			methods.googleLogin();
	 			methods.FNPLogoClick();
	    		methods.logoutFnp();
	    		methods.FNPLogoClick();
	    		 
				log.info("****  fnpHomePage executed successfully   ****");
				
			} catch (IOException t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
	    	    
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}

	 @Test(priority=2)
		public void facebookLogin() throws InterruptedException, IOException {
			
	    	 log.info("*************** fnpLogin ******************* ");
	    	 
	    	 try {
	    		 
	    		 
	    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	    		//methods.clickHambergerMenu();
	 			methods.clickSubCategory();
	 			methods.scrollingToBottomofAPage();
	 			methods.selectFirstFlowerItem();
	 			methods.enterCityName();
	 			methods.selectingDate();
	 			/*methods.selectingTimeSlots();
	 			methods.selectingTimeSlots_ExpressDeliveryFreeTime();
	 			methods.clickAddToCart();*/
	 			methods.selectAddons();
	 			methods.clickContinueBtn();
	 			methods.fbLogin();
	 			methods.FNPLogoClick();
	    		methods.logoutFnp();
				
	    		 
				log.info("****  fnpHomePage executed successfully   ****");
				
			} catch (IOException t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
	    	    
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
	
	 
	 
	
}

package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class PersonalizedProductwithVariantswithoutAddOnSearch extends TestBase{

	
	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
			@Test
			public void PersonalizedProductwith_Variants_withoutAddOn_Search() {
				
		    	 log.info("*************** PersonalizedProductwith_Variants_withoutAddOn_Search ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 //CommonMethod.click(driver, "click_LoginIcon");
		    		 methods.SearchMenuCLick();
		    		 methods.Personalized_Search();
		    		 methods.clickSearchButton();
		    		 methods.Personalized_Search_Product();
		    		 methods.personalizedPincode();
		 			 methods.DateTentativeSelect();
		 			 methods.personalizedDate();
		 			 methods.clickAddToCart();
		 			 methods.withoutAddons();
		 			 methods.afterAddingProductclickCartIcon();
		 			 methods.clickProceedToPay();
		 			 methods.fnpLoginPage();
		 			 methods.addNewAddressisPresentorNot_Check();
		 			 methods.addNewAddress();
		 			 methods.clickSaveandContinue();
		    		 methods.iHereByCheckBox();
		    		 methods.clickProceedToPay();
		 			 methods.FNPLogoClick();
		    		 methods.logoutFnp();
		    		 methods.FNPLogoClick();
		    		 methods.navigatetoHomePage();
		    		 
					log.info("****  PersonalizedProductwith_Variants_withoutAddOn_Search executed successfully   ****");
					
				} catch (Exception t) {
					
					//CommonMethod.takesScreenshotOnFailure(driver, "PersonalizedProductwith_Variants_withoutAddOn_Search", "failed");
					try {
			    		methods.navigatetoHomePage();
						methods.logoutFnp();
					} catch (Exception e) {
						e.printStackTrace();
					}
		  		    Log.info(t.getLocalizedMessage());
		  		    Error e1 = new Error(t.getMessage()); 
		  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		  		    e1.setStackTrace(t.getStackTrace()); 
		  		    throw e1;
		  		    
				} 
			}
}

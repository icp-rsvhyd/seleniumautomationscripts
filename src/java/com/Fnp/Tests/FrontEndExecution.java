package com.Fnp.Tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class FrontEndExecution extends TestBase {
	
	FnpMethods methods = new FnpMethods();
	{
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}			
		
	/**
	 * Verify the "FNP" application
	 * @throws IOException;
	 * @throws InterruptedException 
	 * 
	 */
	
	
	 @Test(priority=1)
		public void fnpLogin() throws InterruptedException, IOException {
			
	    	 log.info("*************** fnpLogin ******************* ");
	    	 
	    	 try {
	    		 
	    		 driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	    		 CommonMethod.click(driver, "click_LoginIcon");
	    		 Thread.sleep(6000);
	    		 methods.fnpLoginPage();
	    		 methods.FNPLogoClick();
	    		 methods.SearchMenuCLick();
	    		 methods.Searchcakess();
	    		 methods.clickSearchButton();
	    		 methods.selectingoneProduct();
	    		 methods.enterCityName();
	 			 methods.selectingDate();
	    		 methods.selectingTimeSlotsEarliestDelivery();
	    		 methods.selectEggRadioBtn();
	    		// methods.clickAddToCart();
	    		 methods.selectAddons();
	    		 methods.clickContinueBtn();
	    		 methods.clickSaveandContinue();
	    		 methods.FNPLogoClick();

				log.info("****  fnpHomePage executed successfully   ****");
				
			} catch (IOException t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
	    	    
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
	
	 // @Test(priority=2)
		public void fnpHomePage() throws IOException, InterruptedException{
			
			log.info("*************** FnpHomePage ******************* ");
			
			try {
				
				//methods.clickHandSymbol();
				//methods.clickHambergerMenu();
				methods.clickSubCategory();
				methods.scrollingToBottomofAPage();
				methods.selectFirstFlowerItem();
				methods.enterCityName();
				methods.selectingDate();
				/*methods.selectingTimeSlots();
				methods.clickAddToCart();*/
				methods.selectAddons();
				methods.clickContinueBtn();
				methods.clickSaveandContinue();
	    		methods.FNPLogoClick();
				
				log.info("****  fnpHomePage executed successfully   ****");
			} catch (IOException t) {
				
				
	    	   // CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
	    	    
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
			}
		
		}
	  
	  
	 // @Test(priority=3)
		public void Courierproduct() throws InterruptedException, IOException {
			
	    	 log.info("*************** Courierproduct ******************* ");
	    	 
	    	 try {
	    		 
	    		 /*methods.selectfirstCourierProduct();*/
	    		 methods.clickCourierProduct();
	    		 methods.Select_Date_Tentative();
	    		 //methods.selctingCourierProduct();
	    		/* methods.clickAddToCart();*/
				 methods.selectAddons();
				 methods.clickContinueBtn();
				 methods.clickSaveandContinue();
		    	 methods.FNPLogoClick();
	    		 
				log.info("****  Courierproduct executed successfully   ****");
				
			} catch (IOException t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
	    	    
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
	  
	  
	 // @Test(priority=4)
		public void personalizedProduct() throws InterruptedException, IOException {
			
	    	 log.info("*************** personalizedProduct ******************* ");
	    	 
	    	 try {
	    		 methods.PersonalizedCoordinates();
	    		 methods.clicking_PersonalizedProduct();
	    		 methods.Uploading_PersonalizedProduct();
	    		 methods.clickCourierProduct();
	    		 methods.Select_Date_Tentative();
	    		 /*methods.clickAddToCart();*/
				 methods.selectAddons();
				 methods.clickContinueBtn();
				 methods.clickSaveandContinue();
		    	 methods.FNPLogoClick();
	    		
	    		 
				log.info("****  personalizedProduct executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
	    	    
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
	  
	  
}

package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class verifying404Error extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
	/**
	 * Succes
	 */
	@Test
	public void verifying404_Error() {
		 
		 log.info("*************** verifying404_Error ******************* ");
   	 
   	 try {
   		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    		methods.checkingPopupisDispalying();
    		
    		driver.navigate().to("http://www.fnp.com/gifts/.com.in");
    		String url = driver.getCurrentUrl();
    		
    		methods.navigatetoHomePage();
			log.info("****  verifying404_Error executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
			methods.navigatetoHomePage();
 		    Log.info(t.getLocalizedMessage());
 		    Error e1 = new Error(t.getMessage()); 
 		    Log.error("TestCase failed" +t.getLocalizedMessage() );
 		    e1.setStackTrace(t.getStackTrace()); 
 		    throw e1;
 		    
		} 
		 
	 }
}

package com.Fnp.Tests;
import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;





public class Test {

	
		static WebDriver oBrowser;
		
		public static void setUp(String browserType, String url){
			if (browserType.equalsIgnoreCase("firefox")){
				oBrowser = new FirefoxDriver();
			}else if (browserType.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "D:\\DecBatch\\Downloads\\chromedriver.exe");
				oBrowser = new ChromeDriver();
			}else if (browserType.equalsIgnoreCase("ie")){
				System.setProperty("webdriver.ie.driver", "D:\\DecBatch\\Downloads\\IEDriverServer.exe");
				oBrowser = new InternetExplorerDriver();
			}else{
				System.out.println("Unsupported browser type (give firefox/chrome/ie)");
				System.exit(-1);
			}
			
			oBrowser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			oBrowser.manage().window().maximize();
			oBrowser.get(url);
		}
		
		public static void cleanUp(){
			oBrowser.quit();
		}
		
		public static void scroll_In_eBay(String searchString, String catToSelect) throws Exception{
			oBrowser.findElement(By.xpath("//input[@id='gh-ac']")).clear();
			oBrowser.findElement(By.xpath("//input[@id='gh-ac']")).sendKeys(searchString);
			Select catSelect = new Select(oBrowser.findElement(By.cssSelector("#gh-cat")));
			catSelect.selectByVisibleText(catToSelect);
			oBrowser.findElement(By.xpath("//input[@id='gh-btn']")).click();
			
			WebDriverWait driverWait = new WebDriverWait(oBrowser, 60);
			driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='rtm_html_270']")));
			
			File screenImg = ((TakesScreenshot)oBrowser).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenImg, new File("E:\\Screenshot.jpg"));
			
			List<WebElement> searchItems = oBrowser.findElements(By.xpath("//div[@id='ResultSetItems']/ul/li"));
			WebElement searchItem;
			int xPos, yPos;
			
			Actions mouseMove = new Actions(oBrowser);
			
			for(int i=0;i<searchItems.size();i++){
				searchItem = searchItems.get(i);
				xPos = searchItem.getLocation().x;
				yPos = searchItem.getLocation().y;
				mouseMove.moveToElement(searchItem, xPos, yPos).perform();
				Thread.sleep(3000);
			}
		}
		
		public static void main(String args[]) throws Exception{
			setUp("firefox", "http://ebay.in");
			scroll_In_eBay("dell laptop", "Laptops & Computer Peripherals");
			cleanUp();
		}
	
	
}

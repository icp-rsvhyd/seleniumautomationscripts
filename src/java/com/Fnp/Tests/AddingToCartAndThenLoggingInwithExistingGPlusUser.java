package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class AddingToCartAndThenLoggingInwithExistingGPlusUser extends TestBase {

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
	@Test
		 public void Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser() {
			 
			 log.info("*************** Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser ******************* ");
	 	 
	 	 try {
	 		 	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	  			methods.checkingPopupisDispalying();
	  			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  			//methods.googleLogin();
	  			methods.AnniversryMenu();
				methods.AnniversarySub_Menu();
				methods.PersonalizedProduct();
				methods.personalizedPincode();
				methods.Select_Date_Tentative();
				methods.sendingImgToPersonalizedProd();
				methods.clickAddToCart();
				methods.clickContinueBtn();
				methods.afterAddingProductclickCartIcon();
				methods.clickProceedToPay();
				methods.googleLogin();
				methods.clickSaveandContinue();
				methods.clickProceedToPay();
				methods.iHereByCheckBox();
				methods.clickProceedToPay();
				methods.FNPLogoClick();
				methods.logoutFnp();
				methods.GmailLogout_WhenGmailLoggedIn();
				methods.FNPLogoClick();
				methods.navigatetoHomePage();
	 		
				log.info("****  Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser", "failed");
				
			    Log.info(t.getLocalizedMessage());
			    Error e1 = new Error(t.getMessage()); 
			    Log.error("TestCase failed" +t.getLocalizedMessage() );
			    e1.setStackTrace(t.getStackTrace()); 
			    throw e1;
			    
			} 
			 
		 }
}

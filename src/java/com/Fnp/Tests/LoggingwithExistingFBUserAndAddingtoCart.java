package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class LoggingwithExistingFBUserAndAddingtoCart extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
	
			@Test
			public void Loggingwith_Existing_FBUser_And_AddingtoCart(){
				
		    	 log.info("*************** Loggingwith_Existing_FBUser_And_AddingtoCart ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 CommonMethod.click(driver, "click_LoginIcon");
		    		 methods.fbLogin();
		    		 methods.FNPLogoClick();
		    		 methods.clickingFlowers_Category();
		    		 methods.filtersFlowersProduct();
		    		 methods.clickAddToCart();
		    		 methods.filters_SelectingCity();
		    		 methods.filters_SelectDate_Timeslot();
		    		 methods.selectingTimeSlots();
		    		 methods.clickAddToCart();
		 			 methods.selectAddons();
		 			 methods.clickContinueBtn();
		 			 methods.afterAddingProductclickCartIcon();
		    		 methods.clickProceedToPay();
		    		 methods.iHereByCheckBox();
		    		 methods.clickSaveandContinue();
		    		 methods.FNPLogoClick();
		    		 methods.logoutFnp();
		    		 methods.FacebookLogout_WhenFBLoggedIn();
		    		 methods.FNPLogoClick();
		    		 methods.navigatetoHomePage();
					log.info("****  Loggingwith_Existing_FBUser_And_AddingtoCart executed successfully   ****");
					
				} catch (Exception t) {
					
					//CommonMethod.takesScreenshotOnFailure(driver, "Loggingwith_Existing_FBUser_And_AddingtoCart", "failed");
					try {
						methods.navigatetoHomePage();
						methods.FacebookLogout_WhenFBLoggedIn();
					} catch (Exception e) {
						e.printStackTrace();
					}
		  		    Log.info(t.getLocalizedMessage());
		  		    Error e1 = new Error(t.getMessage()); 
		  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		  		    e1.setStackTrace(t.getStackTrace()); 
		  		    throw e1;
		  		    
				} 
			}
}

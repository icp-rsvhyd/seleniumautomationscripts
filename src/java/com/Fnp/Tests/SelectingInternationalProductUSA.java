package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;
import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class SelectingInternationalProductUSA extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
	
	@Test
		 public void selecting_InternationalProductUSA() {
			 
			 log.info("*************** selecting_InternationalProductUSA ******************* ");
	    	 
	    	 try {
	    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	     		methods.checkingPopupisDispalying();
	     		methods.Internation_Menu();
	    		methods.International_Submenu();
	    		methods.International_Product();
	    		//methods.clickAddToCart();
	    		methods.International_DateShippingMethodPlaceholder();
	    		methods.international_DateCalendar();
	    		methods.international_DeliveryType();
	    		methods.clickAddToCart();
				methods.afterAddingProductclickCartIcon();
				methods.clickProceedToPay();
				methods.fnpLoginPage();
				methods.addNewAddressisPresentorNot_Check();
				methods.International_Adding_NewAddress();
				methods.clickSaveandContinue();
				methods.clickProceedToPay();
				methods.iHereByCheckBox();
				methods.clickProceedToPay();
				methods.FNPLogoClick();
				methods.logoutFnp();
				methods.navigatetoHomePage();
	    		
				log.info("****  selecting_InternationalProductUSA executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
				
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
			 
		 }
}

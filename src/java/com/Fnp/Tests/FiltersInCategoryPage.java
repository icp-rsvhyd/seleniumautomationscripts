package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class FiltersInCategoryPage extends TestBase{

	
	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
			@Test
			public void Filters_In_CategoryPage(){
				
		    	 log.info("*************** Filters_In_CategoryPage ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 methods.clickingFlowers_Category();
		    		 methods.deliveryCity_Selecting();
		    		 methods.deliveryCity_SearchCityName();
		    		 methods.filters_PriceMenu();
		    		 methods.filters_DeliveryDate();
		    		 methods.filters_CalendarDateSelecting();
		    		 methods.navigatetoHomePage();
		    		 //methods.filtersFlowersProduct();
					log.info("****  Filters_In_CategoryPage executed successfully   ****");
					
				} catch (Exception t) {
					
					//CommonMethod.takesScreenshotOnFailure(driver, "LoadMoreButton_Clicking_CategoryPage", "failed");
					methods.navigatetoHomePage();
		  		    Log.info(t.getLocalizedMessage());
		  		    Error e1 = new Error(t.getMessage()); 
		  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		  		    e1.setStackTrace(t.getStackTrace()); 
		  		    throw e1;
		  		    
				} 
			}
}

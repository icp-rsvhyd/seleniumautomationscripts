package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class LoadMoreButtonClickingCategoryPage extends TestBase{

	
	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	@Test
			public void LoadMoreButton_Clicking_CategoryPage(){
				
		    	 log.info("*************** LoadMoreButton_Clicking_CategoryPage ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.navigate().to("http://www.fnp.com/flowers/birthday");
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 //methods.clickingFlowers_Category();
		    		 methods.clicking_LoadMoreBtn();
		    		 methods.FNPLogoClick();
		    		 methods.navigatetoHomePage();
					log.info("****  LoadMoreButton_Clicking_CategoryPage executed successfully   ****");
					
				} catch (Exception t) {
					
					//CommonMethod.takesScreenshotOnFailure(driver, "LoadMoreButton_Clicking_CategoryPage", "failed");
					methods.navigatetoHomePage();
		  		    Log.info(t.getLocalizedMessage());
		  		    Error e1 = new Error(t.getMessage()); 
		  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		  		    e1.setStackTrace(t.getStackTrace()); 
		  		    throw e1;
		  		    
				} 
			}
	
}

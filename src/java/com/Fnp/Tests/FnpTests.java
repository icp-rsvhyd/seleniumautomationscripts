package com.Fnp.Tests;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;


public class FnpTests extends TestBase {

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
	
	/**
	 * Verify the "FNP" application
	 * @throws IOException;
	 * @throws InterruptedException 
	 * 
	 */
	
	
	/*
	 * 
	 * Success
	 */
	
	//@Test(priority=1)
	public void PersonalizedProduct(){
		
		log.info("*************** PersonalizedProduct ******************* ");
		
		try {
			
			methods.checkingPopupisDispalying();   		
   		 	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			methods.AnniversryMenu();
			methods.AnniversarySub_Menu();
			methods.PersonalizedProduct();
			methods.personalizedPincode();
			methods.Select_Date_Tentative();
			methods.sendingImgToPersonalizedProd();
			methods.clickAddToCart();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.fnpLoginPage();
			methods.addNewAddressisPresentorNot_Check();
			methods.addNewAddress();
			methods.clickSaveandContinue();
			methods.clickProceedToPay();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.FNPLogoClick();
			methods.navigatetoHomePage();
			log.info("****  PersonalizedProduct executed successfully   ****");
		} catch (Exception t) {
			
			
    	   // CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
		}
	
	}
	/*
	 * Success
	 */
	
     //@Test(priority=2)
     public void OrderPlacing_CakeProductwith_Egg_Eggless_Search_Menu(){
    	 
    	 log.info("*************** OrderPlacing_CakeProductwith_Egg_Eggless_Search_Menu ******************* ");
    	 
    	 try {
    		 
    		 methods.checkingPopupisDispalying();
    		 driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    		 CommonMethod.click(driver, "click_LoginIcon");
    		 methods.fnpLoginPage();
    		 methods.FNPLogoClick();
    		 methods.SearchMenuCLick();
    		 methods.Searchcakess();
    		 methods.clickSearchButton();
    		 methods.egglessProduct_Select(); 
    		 methods.Eggless_ANd_WithEgg_Products_Checking();
    		 methods.enterPincode();
 			 methods.selectingDate();
 			 methods.nextDat_Delivery_Dates();
 			 methods.nextDat_Delivery_DateswithTime();
 			 methods.clickAddToCart();
 			 methods.selectEggAddons();
 			 methods.clickContinueBtn();
 			 methods.afterAddingProductclickCartIcon();
			 methods.clickProceedToPay();
			 methods.addNewAddressisPresentorNot_Check();
 			 methods.addNewAddress();
 			 methods.clickSaveandContinue();
			 methods.iHereByCheckBox();
			 methods.clickProceedToPay();
			 methods.FNPLogoClick();
			 methods.logoutFnp();
			 methods.FNPLogoClick();
			 methods.navigatetoHomePage();
			 
			 
			 
			 log.info("*************** Executed Successfully OrderPlacing_CakeProductwith_Egg_Eggless_Search_Menu ******************* ");
    		 
			
		} catch (Exception t) {
			
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace());
  		    throw e1;
		
		} 
    	 
     }
     
     /*
      * Success
      */
     
    // @Test(priority=3)
	 public void Logging_with_ExistingUser_And_AddingToCart() {
		 
		 log.info("*************** Logging_with_ExistingUser_And_AddingToCart ******************* ");
    	 
    	 try {
    		
    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    		methods.checkingPopupisDispalying();
    		CommonMethod.click(driver, "click_LoginIcon");
   		 	methods.fnpLoginPage();
   		 	methods.FNPLogoClick();
    		methods.SearchMenuCLick();
    		methods.quickSearch_SelectOccasion();
    		methods.quickSearch_SelectCity();
    		methods.quick_SelectDate();
    		methods.quick_NextMonthDate();
    		methods.quick_select_Date();
    		methods.click_ShopNowBtn();
    		methods.QuickShop_selectFirstFlowerItem();
    		methods.enterPincode();
    		methods.selectingQuickShow_SelectDate_Timeslots();
    		methods.quick_select_Calendar_Date();
    		methods.quick_SelectingExpressDelivery();
    		methods.quick_Select_TimeSlot_Time();
    		methods.clickAddToCart();
    		methods.clickContinueBtn();
    		methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.addNewAddressisPresentorNot_Check();
			methods.addNewAddress();
			methods.clickSaveandContinue();
    		methods.clickProceedToPay();
    		methods.FNPLogoClick();
			methods.logoutFnp();
			methods.FNPLogoClick();
			methods.navigatetoHomePage();
    		log.info("****  Logging_with_ExistingUser_And_AddingToCart executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Logging_with_ExistingUser_And_AddingToCart", "failed");
			
    	    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		 
	 }
     
	 /*
	  * Success
	  */
	 //@Test(priority=4)
	 public void selecting_InternationalProductUSA() {
		 
		 log.info("*************** selecting_InternationalProductUSA ******************* ");
    	 
    	 try {
    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
     		methods.checkingPopupisDispalying();
     		methods.Internation_Menu();
    		methods.International_Submenu();
    		methods.International_Product();
    		//methods.clickAddToCart();
    		methods.International_DateShippingMethodPlaceholder();
    		methods.international_DateCalendar();
    		methods.international_DeliveryType();
    		methods.clickAddToCart();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.fnpLoginPage();
			methods.addNewAddressisPresentorNot_Check();
			methods.International_Adding_NewAddress();
			methods.clickSaveandContinue();
			methods.clickProceedToPay();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.navigatetoHomePage();
    		
			log.info("****  selecting_InternationalProductUSA executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		 
	 }
	 
	 /*
	  * Success
	  */
	// @Test(priority=5)
	 public void deliveryDetailsPage_AddNewAddressValidating_WithFBLogin() {
		 
		 log.info("*************** deliveryDetailsPage_AddNewAddressValidating_WithFBLogin ******************* ");
    	 
    	 try {
    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
     		methods.checkingPopupisDispalying();
     		CommonMethod.click(driver, "click_LoginIcon");
     		methods.fbLogin();
     		methods.FNPLogoClick();
     		methods.SearchMenuCLick();
     		methods.Searchcakess();
     		methods.clickSearchButton();
     		methods.egglessProduct_Select(); 
     		methods.Eggless_ANd_WithEgg_Products_Checking();
     		methods.enterCityName();
			methods.selectingDate();
			methods.nextDat_Delivery_Dates();
			methods.nextDat_Delivery_DateswithTime();
			methods.clickAddToCart();
			methods.selectEggAddons();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.addNewAddressisPresentorNot_Check();
			methods.addNewAddress();
			methods.clickSaveandContinue();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			//methods.savedCreditCardDetails();
			methods.FNPLogoClick();
			methods.logoutFnp();
    		methods.FacebookLogout_WhenFBLoggedIn();
    		methods.navigatetoHomePage();
			log.info("****  deliveryDetailsPage_AddNewAddressValidating_WithFBLogin executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "deliveryDetailsPage_AddNewAddressValidating_WithFBLogin", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		 
	 } 
     
     /*
      * Failed
      */
	 //@Test(priority=6)
	 public void Adding_To_Cart_And_thenLogging_Inwith_NewUser() {
		 
		 log.info("*************** Adding_To_Cart_And_thenLogging_Inwith_NewUser ******************* ");
    	 
    	 try {
    		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
     		methods.checkingPopupisDispalying();
     		CommonMethod.click(driver, "click_LoginIcon");
     		methods.fnpNewLogin();
     		methods.FNPLogoClick();
     		methods.SearchMenuCLick();
     		methods.Searchcakess();
     		methods.clickSearchButton();
     		methods.egglessProduct_Select(); 
     		methods.Eggless_ANd_WithEgg_Products_Checking();
     		methods.enterCityName();
			methods.selectingDate();
			methods.nextDat_Delivery_Dates();
			methods.nextDat_Delivery_DateswithTime();
			methods.clickAddToCart();
			methods.selectEggAddons();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.addNewAddressisPresentorNot_Check();
			methods.clickSaveandContinue();
			methods.addNewAddress();
			methods.clickSaveandContinue();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			//methods.savedCreditCardDetails();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.FacebookLogout_WhenFBLoggedIn();
			methods.navigatetoHomePage();
			log.info("****  Adding_To_Cart_And_thenLogging_Inwith_NewUser executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Adding_To_Cart_And_thenLogging_Inwith_NewUser", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		 
	 } 
	 /**
	  * Success
	  */
	//@Test(priority=7)
	 /*public void Verifying_International_Productsfor_UAE() {
		 
		 log.info("*************** Verifying_International_Productsfor_UK_UAE_And_Germany ******************* ");
    	 
    	 try {
    		
    		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
      		methods.checkingPopupisDispalying();
    		methods.Internation_Menu();
    		methods.UAE_ChocolatesMenu();
    		methods.International_Product();
    		methods.International_DateShippingMethodPlaceholder();
    		methods.international_DateCalendar();
    		methods.international_DeliveryType();
    		methods.clickAddToCart();
    		methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.fnpLoginPage();
			methods.addNewAddressisPresentorNot_Check();
    		methods.International_Adding_NewAddress();
			methods.clickSaveandContinue();
			methods.clickProceedToPay();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.navigatetoHomePage();
    		
			log.info("****  Verifying_International_Productsfor_UK_UAE_And_Germany executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Verifying_International_Productsfor_UK_UAE_And_Germany", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		 
	 }*/
	 
	 /**
	  * Success
	  */
	 
	//@Test(priority=8)
		 public void verifying404_Error() {
			 
			 log.info("*************** verifying404_Error ******************* ");
	    	 
	    	 try {
	    		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	     		methods.checkingPopupisDispalying();
	     		
	     		driver.navigate().to("http://www.fnp.com/gifts/.com.in");
	     		String url = driver.getCurrentUrl();
	     		
	     		methods.navigatetoHomePage();
				log.info("****  verifying404_Error executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
				methods.navigatetoHomePage();
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
			 
		 }
	 
		 /*
		  *Failed 
		  */
	//@Test(priority=9)
	 public void AddingToCart_Andthen_Loggingin_with_NewUser() {
		 
		 log.info("*************** AddingToCart_Andthen_Loggingin_with_NewUser ******************* ");
   	 
		try {
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    		methods.checkingPopupisDispalying();
    		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			methods.AnniversryMenu();
			methods.AnniversarySub_Menu();
			methods.PersonalizedProduct();
			methods.personalizedPincode();
			methods.Select_Date_Tentative();
			methods.sendingImgToPersonalizedProd();
			methods.clickAddToCart();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
    		methods.fnpNewLogin();
    		methods.addNewAddress();
			methods.clickSaveandContinue();
			methods.clickProceedToPay();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.FNPLogoClick();
			methods.navigatetoHomePage(); 		
   		
			log.info("****  AddingToCart_Andthen_Loggingin_with_NewUser executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "AddingToCart_Andthen_Loggingin_with_NewUser", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
 		    Log.info(t.getLocalizedMessage());
 		    Error e1 = new Error(t.getMessage()); 
 		    Log.error("TestCase failed" +t.getLocalizedMessage() );
 		    e1.setStackTrace(t.getStackTrace()); 
 		    throw e1;
 		    
		} 
		 
	 }
    
	/**
	 * Success
	 */
	//@Test(priority=10)
	 public void Loggingwith_NewUser_And_AddingtoCart() {
		 
		 log.info("*************** Loggingwith_NewUser_And_AddingtoCart ******************* ");
  	 
  	 try {
  		 	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
   			methods.checkingPopupisDispalying();
   			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
   			CommonMethod.click(driver, "click_LoginIcon");
   			methods.fnpNewLogin();
   			methods.FNPLogoClick();
			methods.AnniversryMenu();
			methods.AnniversarySub_Menu();
			methods.PersonalizedProduct();
			methods.personalizedPincode();
			methods.Select_Date_Tentative();
			methods.sendingImgToPersonalizedProd();
			methods.clickAddToCart();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.addNewAddress();
			methods.clickSaveandContinue();
			methods.clickProceedToPay();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.FNPLogoClick();
			methods.navigatetoHomePage();
  		
			log.info("****  Loggingwith_NewUser_And_AddingtoCart executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Loggingwith_NewUser_And_AddingtoCart", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
		    Log.info(t.getLocalizedMessage());
		    Error e1 = new Error(t.getMessage()); 
		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		    e1.setStackTrace(t.getStackTrace()); 
		    throw e1;
		    
		} 
		 
	 }
	
	 /**
	  * Sucess
	  */
	
	//@Test(priority=11)
	 public void Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser() {
		 
		 log.info("*************** Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser ******************* ");
 	 
 	 try {
 		 	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
  			methods.checkingPopupisDispalying();
  			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  			//methods.googleLogin();
  			methods.AnniversryMenu();
			methods.AnniversarySub_Menu();
			methods.PersonalizedProduct();
			methods.personalizedPincode();
			methods.Select_Date_Tentative();
			methods.sendingImgToPersonalizedProd();
			methods.clickAddToCart();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.googleLogin();
			methods.clickSaveandContinue();
			methods.clickProceedToPay();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			methods.FNPLogoClick();
			methods.logoutFnp();
			methods.GmailLogout_WhenGmailLoggedIn();
			methods.FNPLogoClick();
			methods.navigatetoHomePage();
 		
			log.info("****  Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Adding_To_Cart_AndThen_LoggingIn_with_Existing_GPlusUser", "failed");
			try {
				methods.navigatetoHomePage();
				methods.GmailLogout_WhenGmailLoggedIn();
			} catch (Exception e) {
				e.printStackTrace();
			}
		    Log.info(t.getLocalizedMessage());
		    Error e1 = new Error(t.getMessage()); 
		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		    e1.setStackTrace(t.getStackTrace()); 
		    throw e1;
		    
		} 
		 
	 }
	/*
	 * Success
	 */
	//@Test(priority=12)
	public void Logging_With_Existing_GPlusUser_And_Adding_To_Cart() {
		
    	 log.info("*************** Logging_With_Existing_GPlusUser_And_Adding_To_Cart ******************* ");
    	 
    	 try {
    		 
    		 methods.checkingPopupisDispalying();
    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    		 CommonMethod.click(driver, "click_LoginIcon");
    		 Thread.sleep(6000);
    		 methods.googleLogin();
    		 methods.FNPLogoClick();
    		 methods.clickingFlowers_Category();
    		 methods.filtersFlowersProduct();
    		 methods.clickAddToCart();
    		 methods.filters_SelectingCity();
    		 methods.filters_SelectDate_Timeslots();
    		 methods.selectingTimeSlots();
    		 methods.clickAddToCart();
 			 methods.selectAddons();
 			 methods.clickContinueBtn();
 			 methods.afterAddingProductclickCartIcon();
    		 methods.clickProceedToPay();
    		 methods.iHereByCheckBox();
    		 methods.clickSaveandContinue();
    		 methods.FNPLogoClick();
    		 methods.logoutFnp();
    		 methods.GmailLogout_WhenGmailLoggedIn();
    		 methods.FNPLogoClick();
    		 methods.navigatetoHomePage();
    		 
			log.info("****  Logging_With_Existing_GPlusUser_And_Adding_To_Cart executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Logging_With_Existing_GPlusUser_And_Adding_To_Cart", "failed");
			
			try {
				methods.navigatetoHomePage();
				methods.GmailLogout_WhenGmailLoggedIn();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
	}
	
	/*
	 * Success
	 */
	//@Test(priority=13)
	public void Logging_With_Existing_FBUser_And_Adding_To_Cart()  {
		
    	 log.info("*************** Logging_With_Existing_GPlusUser_And_Adding_To_Cart ******************* ");
    	 
    	 try {
    		 
    		 methods.checkingPopupisDispalying();
    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    		 CommonMethod.click(driver, "click_LoginIcon");
    		 methods.fbLogin();
    		 methods.FNPLogoClick();
    		 methods.clickingFlowers_Category();
    		 methods.filtersFlowersProduct();
    		 methods.clickAddToCart();
    		 methods.filters_SelectingCity();
    		 methods.filters_SelectDate_Timeslots();
    		 methods.selectingTimeSlots();
 			 methods.clickAddToCart();
 			 methods.selectAddons();
 			 methods.clickContinueBtn();
 			 methods.afterAddingProductclickCartIcon();
    		 methods.clickProceedToPay();
    		 methods.iHereByCheckBox();
    		 methods.clickSaveandContinue();
    		 methods.FNPLogoClick();
    		 methods.logoutFnp();
    		 methods.FacebookLogout_WhenFBLoggedIn();
    		 methods.FNPLogoClick();
    		 methods.navigatetoHomePage();
			log.info("****  Logging_With_Existing_GPlusUser_And_Adding_To_Cart executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Logging_With_Existing_GPlusUser_And_Adding_To_Cart", "failed");
			
			try {
				methods.navigatetoHomePage();
				methods.FacebookLogout_WhenFBLoggedIn();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
	}
	/*
	 * Success
	 */
	
		//@Test(priority=14)
		public void Loggingwith_Existing_FBUser_And_AddingtoCart(){
			
	    	 log.info("*************** Loggingwith_Existing_FBUser_And_AddingtoCart ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		/* CommonMethod.click(driver, "click_LoginIcon");
	    		 methods.fbLogin();*/
	    		 methods.FNPLogoClick();
	    		 methods.clickingFlowers_Category();
	    		 methods.filtersFlowersProduct();
	    		 methods.clickAddToCart();
	    		 methods.filters_SelectingCity();
	    		 methods.filters_SelectDate_Timeslots();
	    		 methods.selectingTimeSlots();
	    		 methods.clickAddToCart();
	 			 methods.selectAddons();
	 			 methods.clickContinueBtn();
	 			 methods.afterAddingProductclickCartIcon();
	    		 methods.clickProceedToPay();
	    		 methods.iHereByCheckBox();
	    		 methods.clickSaveandContinue();
	    		 methods.FNPLogoClick();
	    		 methods.logoutFnp();
	    		 methods.FacebookLogout_WhenFBLoggedIn();
	    		 methods.FNPLogoClick();
	    		 methods.navigatetoHomePage();
				log.info("****  Loggingwith_Existing_FBUser_And_AddingtoCart executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "Logging_With_Existing_GPlusUser_And_Adding_To_Cart", "failed");
				try {
					methods.navigatetoHomePage();
					methods.FacebookLogout_WhenFBLoggedIn();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}

		/*
		 * Success
		 */
		//@Test(priority=15)
		public void LoadMoreButton_Clicking_CategoryPage(){
			
	    	 log.info("*************** LoadMoreButton_Clicking_CategoryPage ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 methods.clickingFlowers_Category();
	    		 methods.clicking_LoadMoreBtn();
	    		 methods.FNPLogoClick();
	    		 methods.navigatetoHomePage();
				log.info("****  LoadMoreButton_Clicking_CategoryPage executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "LoadMoreButton_Clicking_CategoryPage", "failed");
				methods.navigatetoHomePage();
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
		/*
		 * Success
		 */
		//@Test(priority=16)
		public void Filters_In_CategoryPage(){
			
	    	 log.info("*************** Filters_In_CategoryPage ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 methods.clickingFlowers_Category();
	    		 methods.deliveryCity_Selecting();
	    		 methods.deliveryCity_SearchCityName();
	    		 methods.filters_PriceMenu();
	    		 methods.filters_DeliveryDate();
	    		 methods.filters_CalendarDateSelecting();
	    		 methods.navigatetoHomePage();
	    		 //methods.filtersFlowersProduct();
				log.info("****  Filters_In_CategoryPage executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "LoadMoreButton_Clicking_CategoryPage", "failed");
				methods.navigatetoHomePage();
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
		
		/*
		 * Success
		 */
		//@Test(priority=17)
		public void backNavigationIn_CheckOut_Pages(){
			
	    	 log.info("*************** backNavigationIn_CheckOut_Pages ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 methods.clickingFlowers_Category();
	    		 methods.filtersFlowersProduct();
	    		 methods.filters_SelectingCity();
	    		 methods.filters_SelectDate_Timeslots();
	    		 methods.selectingTimeSlots();
	    		 methods.clickAddToCart();
	 			 methods.selectAddons();
	 			 methods.clickContinueBtn();
	 			 methods.afterAddingProductclickCartIcon();
	    		 methods.clickProceedToPay();
	    		 methods.fnpLoginPage();
	    		 methods.addNewAddressisPresentorNot_Check();
	    		 methods.addNewAddress();
	    		 methods.clickSaveandContinue();
	    		 methods.iHereByCheckBox();
	    		 methods.clickProceedToPay();
	    		 methods.checkoutFlowBackNavigation();
	    		 methods.checkoutFlowBackNavigation();
	    		 methods.checkoutFlowBackNavigation();
	    		 methods.checkoutFlowBackNavigation();
	    		 methods.checkoutFlowBackNavigation();
	    		 methods.navigatetoHomePage();
	    		 methods.logoutFnp();
				log.info("****  backNavigationIn_CheckOut_Pages executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "backNavigationIn_CheckOut_Pages", "failed");
				try {
		    		methods.navigatetoHomePage();
					methods.logoutFnp();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
		
		/*
		 * Success
		 */
		//@Test(priority=18)
		public void Internation_Germany(){
		
			log.info("*************** Internation_Germany ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 methods.Internation_Menu();
	    		 methods.International_SubMenuGERMANY();
	    		 methods.SearchMenuCLick();
	    		 methods.germanyCakesSearch();
	    		 methods.germanyCakes_SearchBtn();
	    		 methods.germany_CakesProd();
	    		 methods.International_DateShippingMethodPlaceholder();
	    		 methods.international_UKDateCalendar();
	    		 methods.international_DeliveryType();
	    		 methods.clickAddToCart();
	    		 methods.afterAddingProductclickCartIcon();
	    		 methods.clickProceedToPay();
	    		 methods.fnpLoginPage();
	    		 methods.addNewAddressisPresentorNot_Check();
	    		 methods.International_Adding_NewAddress();
	    		 methods.clickSaveandContinue();
	    		 methods.iHereByCheckBox();
	    		 methods.clickProceedToPay();
	    		 methods.FNPLogoClick();
	    		 methods.logoutFnp();
	    		 methods.navigatetoHomePage();
	    		 
	    	 }catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Internation_Germany", "failed");
	    		 try {
	 	    		methods.navigatetoHomePage();
	 				methods.logoutFnp();
	 			} catch (Exception e) {
	 				e.printStackTrace();
	 			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		
	}
		
		/*
		 * Success
		 */
		//@Test(priority=19)
		public void OrderPlacing_ExpressProduct_With_Variants_without_AddOn_CategoryPage(){
			
			log.info("*************** OrderPlacing_ExpressProduct_With_Variants_without_AddOn_CategoryPage ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 methods.clickingFlowers_Category();
	    		 methods.filtersFlowersProduct();
	    		 methods.checkingFlower_VariantProduct();
	    		 methods.filters_SelectingCity();
	    		 methods.filters_SelectDate_Timeslots();
	    		 methods.selectingTimeSlots();
	    		 methods.clickAddToCart();
	 			 methods.withoutAddons();
	 			 methods.afterAddingProductclickCartIcon();
	    		 methods.clickProceedToPay();
	    		 methods.fnpLoginPage();
	    		 methods.addNewAddressisPresentorNot_Check();
	    		 methods.addNewAddress();
	    		 methods.clickSaveandContinue();
	    		 methods.iHereByCheckBox();
	    		 methods.clickProceedToPay();
	    		 methods.FNPLogoClick();
	    		 methods.logoutFnp();
	    		 methods.navigatetoHomePage();
	    		 
	    		 
		
		     }catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "OrderPlacing_ExpressProduct_With_Variants_without_AddOn_CategoryPage", "failed");
		    	 try {
			    		methods.navigatetoHomePage();
						methods.logoutFnp();
					} catch (Exception e) {
						e.printStackTrace();
					}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		
	}
		
	/*
	 * Success
	 */
		//@Test(priority=20)
		public void OrderPlacing_InternationalProduct_UK_Category_Page() {
		
			log.info("*************** OrderPlacing_InternationalProduct_UK_Category_Page ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 
	    		 methods.checkingPopupisDispalying();
	      		
	      		methods.Internation_Menu();
	      		methods.International_SubMenuUK();
	     		methods.International_Product();
	     		methods.International_DateShippingMethodPlaceholder();
	     		methods.international_UKDateCalendar();
	     		methods.international_UKDeliveryType();
	     		methods.clickAddToCart();
	     		methods.afterAddingProductclickCartIcon();
	 			methods.clickProceedToPay();
	 			methods.fnpLoginPage();
	     		methods.addNewAddressisPresentorNot_Check();
	     		methods.International_Adding_NewAddress();
	     		methods.clickSaveandContinue();
	     		methods.FNPLogoClick();
	     		methods.navigatetoHomePage();
	    			    		 
	    	 }catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "OrderPlacing_InternationalProduct_UK_Category_Page", "failed");
	    	
	    	try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
  		    Log.info(t.getLocalizedMessage());
  		    Error e1 = new Error(t.getMessage()); 
  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
  		    e1.setStackTrace(t.getStackTrace()); 
  		    throw e1;
  		    
		} 
		
	}
	
		/*
		 * Success
		 */
		//@Test(priority=21)
		public void PersonalizedProductwith_Variants_withoutAddOn_Search() {
			
	    	 log.info("*************** PersonalizedProductwith_Variants_withoutAddOn_Search ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 //CommonMethod.click(driver, "click_LoginIcon");
	    		 methods.SearchMenuCLick();
	    		 methods.Personalized_Search();
	    		 methods.clickSearchButton();
	    		 methods.Personalized_Search_Product();
	    		 methods.personalizedPincode();
	 			 methods.DateTentativeSelect();
	 			 methods.personalizedDate();
	 			 methods.clickAddToCart();
	 			 methods.withoutAddons();
	 			 methods.afterAddingProductclickCartIcon();
	 			 methods.clickProceedToPay();
	 			 methods.fnpLoginPage();
	 			 methods.addNewAddressisPresentorNot_Check();
	 			 methods.addNewAddress();
	 			 methods.clickSaveandContinue();
	    		 methods.iHereByCheckBox();
	    		 methods.clickProceedToPay();
	 			 methods.FNPLogoClick();
	    		 methods.logoutFnp();
	    		 methods.FNPLogoClick();
	    		 methods.navigatetoHomePage();
	    		 
				log.info("****  PersonalizedProductwith_Variants_withoutAddOn_Search executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "PersonalizedProductwith_Variants_withoutAddOn_Search", "failed");
				try {
		    		methods.navigatetoHomePage();
					methods.logoutFnp();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
	
}
			           	
			
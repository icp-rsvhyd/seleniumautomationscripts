package com.Fnp.Tests;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.server.handler.FindElement;

public class CommonUtils {
	
	public static WebDriver driver;
	
	public static void pause_Script(int seconds) {
		
		try {
			
			
			Thread.sleep(seconds*1000);
			
		} catch (InterruptedException e) {
			
			System.exit(-1);
		}
		
	}
	
	
	public static void sendKeys(String objectLocater,String value) throws IOException{
		
		driver.findElement(By.xpath(objectLocater)).sendKeys(value);
		
		//findElement(driver, objectLocater).sendKeys(value);
		
	}

}

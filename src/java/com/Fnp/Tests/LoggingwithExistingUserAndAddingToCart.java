package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;
import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class LoggingwithExistingUserAndAddingToCart extends TestBase{
	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
		 
		 
   	 @Test
   	public void Logging_with_ExistingUser_And_AddingToCart() {
   	 try {
   		log.info("*************** Logging_with_ExistingUser_And_AddingToCart ******************* ");
   		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
   		methods.checkingPopupisDispalying();
   		CommonMethod.click(driver, "click_LoginIcon");
  		methods.fnpLoginPage();
  		methods.FNPLogoClick();
   		methods.SearchMenuCLick();
   		methods.quickSearch_SelectOccasion();
   		methods.quickSearch_SelectCity();
   		methods.quick_SelectDate();
   		methods.quick_NextMonthDate();
   		methods.quick_select_Date();
   		methods.click_ShopNowBtn();
   		methods.QuickShop_selectFirstFlowerItem();
   		methods.clickDontKnow();
   		methods.selectingDate();
   		/*methods.selectingQuickShow_SelectDate_Timeslots();
   		methods.quick_select_Calendar_Date();*/
   		methods.quick_SelectingExpressDelivery();
   		methods.quick_Select_TimeSlot_Time();
   		methods.clickAddToCart();
   		methods.clickContinueBtn();
   		methods.afterAddingProductclickCartIcon();
		methods.clickProceedToPay();
		methods.addNewAddressisPresentorNot_Check();
		methods.addNewAddress();
		methods.clickSaveandContinue();
   		methods.clickProceedToPay();
   		methods.FNPLogoClick();
		methods.logoutFnp();
		methods.FNPLogoClick();
		methods.navigatetoHomePage();
   		log.info("****  Logging_with_ExistingUser_And_AddingToCart executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "Logging_with_ExistingUser_And_AddingToCart", "failed");
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
   	    Log.info(t.getLocalizedMessage());
 		    Error e1 = new Error(t.getMessage()); 
 		    Log.error("TestCase failed" +t.getLocalizedMessage() );
 		    e1.setStackTrace(t.getStackTrace()); 
 		    throw e1;
 		    
		} 
		 
   	 }	 	
	
}

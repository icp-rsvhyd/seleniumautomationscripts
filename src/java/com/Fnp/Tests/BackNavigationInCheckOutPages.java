package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class BackNavigationInCheckOutPages extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
			@Test
			public void backNavigationIn_CheckOut_Pages(){
				
		    	 log.info("*************** backNavigationIn_CheckOut_Pages ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 methods.clickingFlowers_Category();
		    		 methods.filtersFlowersProduct();
		    		 methods.filters_SelectingCity();
		    		 methods.filters_SelectDate_Timeslot();
		    		 methods.selectingTimeSlots();
		    		 methods.clickAddToCart();
		 			 methods.selectAddons();
		 			 methods.clickContinueBtn();
		 			 methods.afterAddingProductclickCartIcon();
		    		 methods.clickProceedToPay();
		    		 methods.fnpLoginPage();
		    		 methods.addNewAddressisPresentorNot_Check();
		    		 methods.addNewAddress();
		    		 methods.clickSaveandContinue();
		    		 methods.iHereByCheckBox();
		    		 methods.clickProceedToPay();
		    		 methods.checkoutFlowBackNavigation();
		    		 methods.checkoutFlowBackNavigation();
		    		 methods.checkoutFlowBackNavigation();
		    		 methods.checkoutFlowBackNavigation();
		    		 methods.checkoutFlowBackNavigation();
		    		 methods.navigatetoHomePage();
		    		 methods.logoutFnp();
					log.info("****  backNavigationIn_CheckOut_Pages executed successfully   ****");
					
				} catch (Exception t) {
					
					//CommonMethod.takesScreenshotOnFailure(driver, "backNavigationIn_CheckOut_Pages", "failed");
					try {
			    		methods.navigatetoHomePage();
						methods.logoutFnp();
					} catch (Exception e) {
						e.printStackTrace();
					}
		  		    Log.info(t.getLocalizedMessage());
		  		    Error e1 = new Error(t.getMessage()); 
		  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
		  		    e1.setStackTrace(t.getStackTrace()); 
		  		    throw e1;
		  		    
				} 
			}
	
}

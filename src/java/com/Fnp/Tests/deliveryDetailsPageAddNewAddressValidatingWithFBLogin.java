package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;
import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class deliveryDetailsPageAddNewAddressValidatingWithFBLogin extends TestBase {

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	

	@Test
	 public void deliveryDetailsPage_AddNewAddressValidating_WithFBLogin() {
		 
		 log.info("*************** deliveryDetailsPage_AddNewAddressValidating_WithFBLogin ******************* ");
   	 
   	 try {
   		    driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    		methods.checkingPopupisDispalying();
    		CommonMethod.click(driver, "click_LoginIcon");
    		methods.fbLogin();
    		methods.FNPLogoClick();
    		methods.SearchMenuCLick();
    		methods.Searchcakess();
    		methods.clickSearchButton();
    		methods.egglessProduct_Select(); 
    		methods.Eggless_ANd_WithEgg_Products_Checking();
    		//methods.enterPincode();
    		methods.clickDontKnow();
			methods.selectingDate();
			methods.nextDat_Delivery_Dates();
			methods.nextDat_Delivery_DateswithTime();
			methods.clickAddToCart();
			methods.selectEggAddons();
			methods.clickContinueBtn();
			methods.afterAddingProductclickCartIcon();
			methods.clickProceedToPay();
			methods.addNewAddressisPresentorNot_Check();
			methods.addNewAddress();
			methods.clickSaveandContinue();
			methods.iHereByCheckBox();
			methods.clickProceedToPay();
			//methods.savedCreditCardDetails();
			methods.FNPLogoClick();
			methods.logoutFnp();
   		    methods.FacebookLogout_WhenFBLoggedIn();
   		    methods.navigatetoHomePage();
			log.info("****  deliveryDetailsPage_AddNewAddressValidating_WithFBLogin executed successfully   ****");
			
		} catch (Exception t) {
			
			//CommonMethod.takesScreenshotOnFailure(driver, "deliveryDetailsPage_AddNewAddressValidating_WithFBLogin", "failed");
			
 		    Log.info(t.getLocalizedMessage());
 		    Error e1 = new Error(t.getMessage()); 
 		    Log.error("TestCase failed" +t.getLocalizedMessage() );
 		    e1.setStackTrace(t.getStackTrace()); 
 		    throw e1;
 		    
		} 
		 
	 } 
}

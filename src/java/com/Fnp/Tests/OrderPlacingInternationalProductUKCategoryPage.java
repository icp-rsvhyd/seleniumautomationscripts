package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class OrderPlacingInternationalProductUKCategoryPage extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
			@Test
			public void OrderPlacing_InternationalProduct_UK_Category_Page() {
			
				log.info("*************** OrderPlacing_InternationalProduct_UK_Category_Page ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 
		    		 methods.checkingPopupisDispalying();
		      		
		      		methods.Internation_Menu();
		      		methods.International_SubMenuUK();
		     		methods.International_Product();
		     		methods.International_DateShippingMethodPlaceholder();
		     		methods.international_UKDateCalendar();
		     		methods.international_UKDeliveryType();
		     		methods.clickAddToCart();
		     		methods.afterAddingProductclickCartIcon();
		 			methods.clickProceedToPay();
		 			methods.fnpLoginPage();
		     		methods.addNewAddressisPresentorNot_Check();
		     		methods.International_Adding_NewAddress();
		     		methods.clickSaveandContinue();
		     		methods.FNPLogoClick();
		     		methods.navigatetoHomePage();
		    			    		 
		    	 }catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "OrderPlacing_InternationalProduct_UK_Category_Page", "failed");
		    	
		    	try {
		    		methods.navigatetoHomePage();
					methods.logoutFnp();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
			
		}
}

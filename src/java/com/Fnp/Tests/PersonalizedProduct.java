package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class PersonalizedProduct extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
		@Test
		public void PersonalizedProduct(){
			
			log.info("*************** PersonalizedProduct ******************* ");
			
			try {
				
				methods.checkingPopupisDispalying();   		
	   		 	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				methods.AnniversryMenu();
				methods.AnniversarySub_Menu();
				methods.PersonalizedProduct();
				methods.personalizedPincode();
				methods.Select_Date_Tentative();
				methods.sendingImgToPersonalizedProd();
				methods.clickAddToCart();
				methods.clickContinueBtn();
				methods.afterAddingProductclickCartIcon();
				methods.clickProceedToPay();
				methods.fnpLoginPage();
				methods.addNewAddressisPresentorNot_Check();
				methods.addNewAddress();
				methods.clickSaveandContinue();
				methods.clickProceedToPay();
				methods.iHereByCheckBox();
				methods.clickProceedToPay();
				methods.FNPLogoClick();
				methods.logoutFnp();
				methods.FNPLogoClick();
				methods.navigatetoHomePage();
				log.info("****  PersonalizedProduct executed successfully   ****");
			} catch (Exception t) {
				
				
	    	   // CommonMethod.takesScreenshotOnFailure(driver, "FnpHomePage", "failed");
				try {
		    		methods.navigatetoHomePage();
					methods.logoutFnp();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
			}
		
		}
}

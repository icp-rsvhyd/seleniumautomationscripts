package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class LoggingWithExistingGPlusUserAndAddingToCart extends TestBase {

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
	
	@Test
		public void Logging_With_Existing_GPlusUser_And_Adding_To_Cart() {
			
	    	 log.info("*************** Logging_With_Existing_GPlusUser_And_Adding_To_Cart ******************* ");
	    	 
	    	 try {
	    		 
	    		 methods.checkingPopupisDispalying();
	    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    		 CommonMethod.click(driver, "click_LoginIcon");
	    		 Thread.sleep(6000);
	    		 methods.googleLogin();
	    		 methods.FNPLogoClick();
	    		 methods.clickingFlowers_Category();
	    		 methods.filtersFlowersProduct();
	    		 methods.clickDontKnow();
	    		 methods.filters_SelectDate_Timeslots();
	    		 methods.selectingTimeSlots();
	    		 methods.clickAddToCart();
	 			 methods.selectAddons();
	 			 methods.clickContinueBtn();
	 			 methods.afterAddingProductclickCartIcon();
	    		 methods.clickProceedToPay();
	    		 methods.iHereByCheckBox();
	    		 methods.clickSaveandContinue();
	    		 methods.FNPLogoClick();
	    		 methods.logoutFnp();
	    		 methods.GmailLogout_WhenGmailLoggedIn();
	    		 methods.FNPLogoClick();
	    		 methods.navigatetoHomePage();
	    		 
				log.info("****  Logging_With_Existing_GPlusUser_And_Adding_To_Cart executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "Logging_With_Existing_GPlusUser_And_Adding_To_Cart", "failed");
				
				try {
					methods.navigatetoHomePage();
					methods.GmailLogout_WhenGmailLoggedIn();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
		}
	
}

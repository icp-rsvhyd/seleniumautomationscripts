package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class InternationGermany extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}
	
		    @Test
			public void Internation_Germany(){
			
				log.info("*************** Internation_Germany ******************* ");
		    	 
		    	 try {
		    		 
		    		 methods.checkingPopupisDispalying();
		    		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    		 methods.Internation_Menu();
		    		 methods.International_SubMenuGERMANY();
		    		 methods.SearchMenuCLick();
		    		 methods.germanyCakesSearch();
		    		 methods.germanyCakes_SearchBtn();
		    		 methods.germany_CakesProd();
		    		 methods.International_DateShippingMethodPlaceholder();
		    		 methods.international_UKDateCalendar();
		    		 methods.international_DeliveryType();
		    		 methods.clickAddToCart();
		    		 methods.afterAddingProductclickCartIcon();
		    		 methods.clickProceedToPay();
		    		 methods.fnpLoginPage();
		    		 methods.addNewAddressisPresentorNot_Check();
		    		 methods.International_Adding_NewAddress();
		    		 methods.clickSaveandContinue();
		    		 methods.iHereByCheckBox();
		    		 methods.clickProceedToPay();
		    		 methods.FNPLogoClick();
		    		 methods.logoutFnp();
		    		 methods.navigatetoHomePage();
		    		 
		    	 }catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "Internation_Germany", "failed");
		    		 try {
		 	    		methods.navigatetoHomePage();
		 				methods.logoutFnp();
		 			} catch (Exception e) {
		 				e.printStackTrace();
		 			}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
			
		}
	
}

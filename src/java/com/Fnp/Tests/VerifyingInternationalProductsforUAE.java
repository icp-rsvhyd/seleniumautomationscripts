package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;

import org.jfree.util.Log;
import org.testng.annotations.Test;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.TestBase;

public class VerifyingInternationalProductsforUAE extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
	
	
	@Test
		 public void Verifying_International_Productsfor_UAE() {
			 
			 log.info("*************** VerifyingInternationalProductsforUAE ******************* ");
	    	 
	    	 try {
	    		
	    		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	      		methods.checkingPopupisDispalying();
	    		methods.Internation_Menu();
	    		methods.UAE_ChocolatesMenu();
	    		methods.International_Product();
	    		methods.International_DateShippingMethodPlaceholder();
	    		methods.international_DateCalendar();
	    		methods.international_DeliveryType();
	    		methods.clickAddToCart();
	    		methods.afterAddingProductclickCartIcon();
				methods.clickProceedToPay();
				methods.fnpLoginPage();
				methods.addNewAddressisPresentorNot_Check();
	    		methods.International_Adding_NewAddress();
				methods.clickSaveandContinue();
				methods.clickProceedToPay();
				methods.iHereByCheckBox();
				methods.clickProceedToPay();
				methods.FNPLogoClick();
				methods.logoutFnp();
				methods.navigatetoHomePage();
	    		
				log.info("****  VerifyingInternationalProductsforUAE executed successfully   ****");
				
			} catch (Exception t) {
				
				//CommonMethod.takesScreenshotOnFailure(driver, "VerifyingInternationalProductsforUAE", "failed");
				try {
		    		methods.navigatetoHomePage();
					methods.logoutFnp();
				} catch (Exception e) {
					e.printStackTrace();
				}
	  		    Log.info(t.getLocalizedMessage());
	  		    Error e1 = new Error(t.getMessage()); 
	  		    Log.error("TestCase failed" +t.getLocalizedMessage() );
	  		    e1.setStackTrace(t.getStackTrace()); 
	  		    throw e1;
	  		    
			} 
			 
		 }
}

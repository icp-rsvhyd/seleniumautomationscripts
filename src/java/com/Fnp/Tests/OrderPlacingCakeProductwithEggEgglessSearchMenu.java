package com.Fnp.Tests;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;
import org.jfree.util.Log;

import com.Fnp.ReUsableMethods.FnpMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class OrderPlacingCakeProductwithEggEgglessSearchMenu extends TestBase{

	FnpMethods methods = new FnpMethods();
	{
		
		System.setProperty("atu.reporter.config",System.getProperty("user.dir")+"//src//main//resources//atu.properties");

	}	
	
	@Test(priority=3)
    public void OrderPlacing_CakeProductwith_Egg_Eggless_Search_Menu(){
   	 
   	 log.info("*************** OrderPlacing_CakeProductwith_Egg_Eggless_Search_Menu ******************* ");
   	 
   	 try {
   		 methods.checkingPopupisDispalying();
   		 driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
   		 CommonMethod.click(driver, "click_LoginIcon");
   		 methods.fnpLoginPage();
   		 methods.FNPLogoClick();
   		 methods.SearchMenuCLick();
   		 methods.Searchcakess();
   		 methods.clickSearchButton();
   		 methods.egglessProduct_Select(); 
   		 methods.Eggless_ANd_WithEgg_Products_Checking();
   		 methods.enterPincode();
	     methods.clickingSelectDatePlaceholder();
		 methods.nextDat_Delivery_Dates();
		 methods.nextDat_Delivery_DateswithTime();
		 methods.clickAddToCart();
		 methods.selectEggAddons();
		 methods.clickContinueBtn();
		 methods.afterAddingProductclickCartIcon();
		 methods.clickProceedToPay();
		 methods.addNewAddressisPresentorNot_Check();
		 methods.addNewAddress();
		 methods.clickSaveandContinue();
		 methods.iHereByCheckBox();
		 methods.clickProceedToPay();
		 methods.FNPLogoClick();
		 methods.logoutFnp();
		 methods.FNPLogoClick();
		 methods.navigatetoHomePage();
			 
			 
			 
			 log.info("*************** Executed Successfully OrderPlacing_CakeProductwith_Egg_Eggless_Search_Menu ******************* ");
   		 
			
		} catch (Exception t) {
			
			try {
	    		methods.navigatetoHomePage();
				methods.logoutFnp();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.info(t.getLocalizedMessage());
 		    Error e1 = new Error(t.getMessage()); 
 		    Log.error("TestCase failed" +t.getLocalizedMessage() );
 		    e1.setStackTrace(t.getStackTrace());
 		    throw e1;
		
		} 
   	 
    }
}

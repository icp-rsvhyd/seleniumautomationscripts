package com.Fnp.ReUsableMethods;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import bsh.ParseException;

import com.Fnp.Assertions.FnpAssertions;
import com.framework.driver.CommonMethod;
import com.framework.driver.DynamicDataSheet;
import com.framework.driver.TestBase;

public class FnpMethods extends TestBase {

	public String date;
	public String Email=null;
	public String surname=null;
	public String firstname=null;
	public String freqeuentNumber=null;
	public String phonenumber=null;
	public String Ephonenumber=null;
	public String Ename=null;
	public int size;
	
	public String passportnumber=null;
	
	
	public void googleLogin() throws IOException, InterruptedException {
		
		CommonMethod.click(driver, "click_GooglePlus");
		/*String str = driver.getWindowHandle();
		driver.switchTo().window(str);*/
		String Uname = datatable.getCellData("GoogleLogin", "Uname", 2);
		CommonMethod.sendKeys(driver, "Email_Name", Uname);
		
		CommonMethod.click(driver, "NextBtn");
		Thread.sleep(8000);
		String Pswd = datatable.getCellData("GoogleLogin", "Pwd", 2);
		CommonMethod.sendKeys(driver, "Email_Pwd", Pswd);
		Thread.sleep(8000);
		CommonMethod.click(driver, "SignInBtn");
		/*JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,750)", "");*/
		
	}
	
	
	public void GmailLogout_WhenGmailLoggedIn() throws IOException, InterruptedException{
		
		driver.navigate().to("http://gmail.com");
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.explicitWait(driver, 60, "ClickLogoutICON");
		CommonMethod.click(driver, "ClickLogoutICON");
		Thread.sleep(8000);
		CommonMethod.explicitWait(driver, 60, "Gmail_Logout");
		CommonMethod.click(driver, "Gmail_Logout");
		Thread.sleep(9000);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.navigate().to("http://fnp.com");
		
	}
	
	public void FacebookLogout_WhenFBLoggedIn() throws IOException, InterruptedException {
		
		driver.navigate().to("http://facebook.com");
		//driver.findElement(By.xpath("//*[@id='facebook']/body/div[18]/div[1]")).click();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.explicitWait(driver, 60, "ClickFBLogoutICON");
		CommonMethod.click(driver, "ClickFBLogoutICON");
		Thread.sleep(6000);
		CommonMethod.explicitWait(driver, 60, "Facebook_Logout");
		CommonMethod.click(driver, "Facebook_Logout");
		//CommonMethod.click(driver, "Facebook_Logout");
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.navigate().to("http://fnp.com");
	}
	
	
	public void fbLogin() throws IOException, InterruptedException {
		
		Thread.sleep(4000);
		CommonMethod.click(driver, "click_Fb");
		Thread.sleep(6000);
		String Uname = datatable.getCellData("FbLogin", "Uname", 2);
		CommonMethod.sendKeys(driver, "Enter_Email", Uname);
		
		String Pswd = datatable.getCellData("FbLogin", "Pwd", 2);
		CommonMethod.sendKeys(driver, "Enter_Pwd", Pswd);
		Thread.sleep(8000);
		CommonMethod.click(driver, "Click_LoginBtn");
		Thread.sleep(8000);
		/*JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,750)", "");*/
		
	}

	public void fnpNewLogin() throws IOException, InterruptedException{
		
		String dynamic_email = DynamicDataSheet.generateEmail();
		//System.out.println("Dynamic Email"+dynamic_email);
		CommonMethod.sendKeys(driver, "Fnp_Login_Email", dynamic_email);
		CommonMethod.click(driver, "Fnp_LOgin_ContinueBtn");
		Thread.sleep(3000);
		String fnpNewUname=datatable.getCellData("FnpNewLogin", "Name", 2);
		CommonMethod.sendKeys(driver, "NewUser_Name", fnpNewUname);
		Thread.sleep(3000);
		String fnpNewMobileNo=datatable.getCellData("FnpNewLogin", "Mobile", 2);
		CommonMethod.sendKeys(driver, "NewUser_MobileNo", fnpNewMobileNo);
		Thread.sleep(4000);
		
		CommonMethod.click(driver, "Fnp_LOgin_ContinueBtn");
		Thread.sleep(6000);
		
	}

	public void checkingPopupisDispalying() throws IOException, InterruptedException{
		if(driver.findElement(By.xpath("//*[@id='newuserdialog']/span[2]/span[2]")).isDisplayed()){
			 Thread.sleep(6000);
			 CommonMethod.click(driver, "click_CloseBtn");
			 Thread.sleep(6000);
		 }
	}
	
	public void clickPopupcloseBtn() throws IOException, InterruptedException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		Thread.sleep(6000);
		CommonMethod.click(driver, "click_CloseBtn");
		Thread.sleep(6000);
	}
	
	public void AnniversaryMenu() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		CommonMethod.click(driver, "FNPAnniversaryMenu");
		
	}
	
	public void clickSubCategory() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		CommonMethod.click(driver, "BirthDay_CarnationsMenu");
	
	}
	
	public void selectFirstFlowerItem() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		CommonMethod.explicitWait(driver, 45, "FirstFlower_1stProduct");
		CommonMethod.click(driver, "FirstFlower_1stProduct");
		Thread.sleep(4000);
	}
	
public void QuickShop_selectFirstFlowerItem() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		//CommonMethod.explicitWait(driver, 45, "FirstFlower_1stProduct");
		CommonMethod.click(driver, "QuickShop_FirstFlower_1stProduct");
		Thread.sleep(4000);
	}
	
	public void select_Variant_FirstFlowerItem() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(8000);
		CommonMethod.click(driver, "Select_VariantProduct");
		
	}
	
	
	public void selecting_VariantProduct1() throws InterruptedException, IOException {
		Thread.sleep(8000);
		
		CommonMethod.click(driver, "select_Vivid2ndProduct");
		Thread.sleep(4000);
	}
	
	
	public void selectingQuickShow_SelectDate_Timeslots() throws IOException {
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Selecting_Date_TimeSlot");
	}
	
	
	public void enterCityName() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		String cityName = datatable.getCellData("CityDetails", "City Name", 2);
		/*CommonMethod.sendKeys(driver, "EnterCityName", cityName);
		Thread.sleep(5000);*/
		WebElement wele = driver.findElement(By.id("pincodelookup"));	
		wele.sendKeys(cityName);
		Thread.sleep(5000);
		wele.sendKeys(Keys.DOWN);
		Thread.sleep(5000);
		/*wele.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(8000);*/
		wele.sendKeys(Keys.ENTER);
	
	}
	
public void enterPincode() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		String cityName = datatable.getCellData("Pincode", "CityPincode", 2);
		/*CommonMethod.sendKeys(driver, "EnterCityName", cityName);
		Thread.sleep(5000);*/
		WebElement wele = driver.findElement(By.id("pincodelookup"));	
		wele.sendKeys(cityName);
		Thread.sleep(5000);
		wele.sendKeys(Keys.DOWN);
		Thread.sleep(5000);
		/*wele.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(8000);*/
		wele.sendKeys(Keys.ENTER);
	
	}
	
	public void selectingDate() throws InterruptedException, IOException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		/*Thread.sleep(7000);
		CommonMethod.click(driver, "DateFieldClick");*/
		Thread.sleep(7000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(6000);
		
		
	}
	
public void clickingSelectDatePlaceholder() throws InterruptedException, IOException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(7000);
		CommonMethod.click(driver, "DateFieldClick");
		Thread.sleep(7000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(6000);
		
		
	}
	
public void personalizedDate() throws InterruptedException, IOException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(7000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(6000);
		
		
	}
	
	public void nextDat_Delivery_Dates() throws IOException {
		CommonMethod.click(driver, "Next_Day_Delivery");
	}
	
	public void nextDat_Delivery_DateswithTime() throws IOException, InterruptedException{
		Thread.sleep(4000);
		CommonMethod.click(driver, "Next_Day_DeliveryTime");
		Thread.sleep(4000);
	}

	public void Select_Date_Tentative() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		CommonMethod.click(driver, "Select_Date_Tentative");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(6000);
		
		
	}
	
	public void sendingImgToPersonalizedProd() throws IOException, InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		/*CommonMethod.sendKeys(driver, "Uploading_Images", "user.dir"+"/Testdata/IMG_20161019_173752.jpg");
		CommonMethod.explicitWait(driver, 60, "Success_Msg");
		String errMessage = driver.findElement(By.xpath(".//*[@id='uploadMsg']")).getText();*/
		Thread.sleep(3000);
		CommonMethod.sendKeys(driver, "TextArea_Message", "Happy");
		
	}
	
	
	public void selectingTimeSlots() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Selecting_TimeSlot_Flowers");
		Thread.sleep(5000);
		CommonMethod.click(driver, "Selecting_TimeSlot_Flowers_StDelivery");
		
	}
	
	public void  quick_SelectingExpressDelivery() throws IOException, InterruptedException {
		Thread.sleep(6000);
		CommonMethod.click(driver, "Quick_Select_ExpressDelivery");
	}
	
	public void quick_Select_TimeSlot_Time() throws IOException, InterruptedException{
		Thread.sleep(6000);
		CommonMethod.click(driver, "Quick_Select_ExpressDelivery_SelectTime");
	}
	
	public void clickAddToCart() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(8000);
		CommonMethod.click(driver, "AddToCart_Button");
		Thread.sleep(8000);
		
		
	}
	
	public void selectAddons() throws IOException, InterruptedException {
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		
		String  handle= driver.getWindowHandle();//Return a string of alphanumeric window handle
		driver.switchTo().window(handle);
		Thread.sleep(5000);
		 	
		CommonMethod.click(driver, "SelectingAddons");
		Thread.sleep(4000);
		
	}
	
	
public void withoutAddons() throws IOException, InterruptedException {
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		
		String  handle= driver.getWindowHandle();//Return a string of alphanumeric window handle
		driver.switchTo().window(handle);
		Thread.sleep(5000);
		CommonMethod.click(driver, "Clicking_NoThanks");
		Thread.sleep(4000);
		
	}
	
	public void afterAddingProductclickCartIcon() throws IOException, InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(5000);
		CommonMethod.click(driver, "After_AddProduct_CArt");
		
		Thread.sleep(5000);
	}
	
	public void clickContinueBtn() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		Thread.sleep(3000);
		CommonMethod.click(driver, "Click_ContinueBtn");
		
	}
	
	public void international_DateCalendar() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(4000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(4000);
	}
	
	public void international_UKDateCalendar() throws InterruptedException, IOException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDateUK");
		Thread.sleep(6000);
	}
	
	public void international_DeliveryType() throws IOException, InterruptedException {
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		CommonMethod.click(driver, "InternatiponalDeliveryTimeslots");
		Thread.sleep(8000);
	}
	
	public void international_UKDeliveryType() throws IOException {
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		CommonMethod.click(driver, "UKInternatiponalDeliveryTimeslots");
		
	}
	
	public void fnpLoginPage() throws IOException, InterruptedException{
		
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
		Thread.sleep(8000);
		
		String email = datatable.getCellData("FNPLoginDetails", "Email", 2);
		CommonMethod.sendKeys(driver, "Fnp_Login_Email", email);
		
		CommonMethod.click(driver, "Fnp_LOgin_ContinueBtn");
				
		String password = datatable.getCellData("FNPLoginDetails", "Password", 2);
		CommonMethod.sendKeys(driver, "Fnp_Login_Password", password);
		
		CommonMethod.click(driver, "Fnp_LOgin_ContinueBtn");
		Thread.sleep(6000);
		
		
	}
	
	public void clickSaveandContinue() throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		browserScrolldown();
		CommonMethod.click(driver, "Click_Save_ContinueBtn");
		Thread.sleep(4000);
		
	}
	
	public void addNewAddressisPresentorNot_Check(){
		if(driver.findElement(By.xpath(".//div[@class='address-panel add-address-panel']")).isDisplayed()){
			driver.findElement(By.xpath(".//div[@class='address-panel add-address-panel']")).click();
		}
	}
	
	public void clickProceedtoCheckOut() throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		browserScrolldown();
		CommonMethod.click(driver, "CLick_ProceedTo_CheckOut");
		Thread.sleep(4000);
		
	}
	
	public void AnniversryMenu() throws IOException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "AnniversaryProductSelecting");
	}
	
	public void AnniversarySub_Menu() throws IOException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "AnniversaryProductSelectingSub_menu");
	}
	
	public void PersonalizedProduct() throws IOException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "PersonalizedProduct_MugPhoto");
	}
	
	public void personalizedPincode() throws IOException, InterruptedException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		/*String picode = datatable.getCellData("CityPinCode", "Pincode", 2);
		CommonMethod.sendKeys(driver, "EnteringCityPin_code", picode);*/
		WebElement wele = driver.findElement(By.id("pincodelookup"));
		Thread.sleep(5000);
		wele.sendKeys("40010");
		Thread.sleep(5000);
		wele.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(5000);
		wele.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
	}
	
	public void DateTentativeSelect() throws IOException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "EnteringCityPin_code");
		
	}
	
	
	public void addNewAddress() throws IOException, InterruptedException {
		Thread.sleep(5000);
		String recName = datatable.getCellData("AddNewAddress", "Rec Name", 2);
		CommonMethod.sendKeys(driver, "RecName", recName);
		Thread.sleep(3000);
		String recAddr = datatable.getCellData("AddNewAddress", "Rec Addr", 2);
		CommonMethod.sendKeys(driver, "RecAddress", recAddr);
		Thread.sleep(3000);
			
		String recNo = datatable.getCellData("AddNewAddress", "Recipients No", 2);
		CommonMethod.sendKeys(driver, "RecMobileNo", recNo);
		Thread.sleep(3000);
	}
	
	public void savedCreditCardDetails() {
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		if(driver.findElement(By.id("SAVED_CREDIT_CARD")).isDisplayed()){
			driver.findElement(By.xpath("(.//input[@placeholder='CVV'])[1]")).sendKeys("123");
			driver.findElement(By.xpath("(.//button[contains(text(),'PAY')])[1]")).click();
		}
	}
	
	public void clickDontKnow() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath(".//*[@id='nopin']")).click();
		String  handle= driver.getWindowHandle();//Return a string of alphanumeric window handle
		
	 	driver.switchTo().window(handle);
	 	Thread.sleep(8000);
	 	WebElement wele = driver.findElement(By.id("citylookup"));
	 	wele.sendKeys("Hyder");
	 	Thread.sleep(6000);
	 	wele.sendKeys(Keys.ARROW_DOWN);
	 	Thread.sleep(6000);
	 	wele.sendKeys(Keys.ENTER);
	 	Thread.sleep(6000);
	 	
	 	WebElement area = driver.findElement(By.id("arealookup"));
	 	area.sendKeys("Hydera");
	 	Thread.sleep(6000);
	 	area.sendKeys(Keys.ARROW_DOWN);
	 	Thread.sleep(6000);
	 	area.sendKeys(Keys.ENTER);
	 	Thread.sleep(6000);
	 	driver.findElement(By.xpath(".//*[contains(text(),'SELECT DELIVERY DATE')]")).click();
		Thread.sleep(6000);
	}
	
	
	
	public void getCreditCardDetails() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		
		String ccNumber = datatable.getCellData("CreditCardDetails", "CC Number", 2);
		String ccName = datatable.getCellData("CreditCardDetails", "Name", 2);
		String ccMonth = datatable.getCellData("CreditCardDetails", "Month", 2);
		String ccYear = datatable.getCellData("CreditCardDetails", "Year", 2);
		String ccCvv = datatable.getCellData("CreditCardDetails", "Year", 2);
		Thread.sleep(8000);
		
		CommonMethod.clear(driver, "CCNumber");
		CommonMethod.sendKeys(driver, "CCNumber", ccNumber);
		Thread.sleep(8000);
		CommonMethod.sendKeys(driver, "CCName", ccName);
		Thread.sleep(8000);
		CommonMethod.sendKeys(driver, "CCMonth", ccMonth);
		Thread.sleep(8000);
		CommonMethod.sendKeys(driver, "CCYear", ccYear);
		Thread.sleep(8000);
		CommonMethod.sendKeys(driver, "CCCVV", ccCvv);
		Thread.sleep(8000);
		CommonMethod.click(driver, "Click_Pay_Btn");
		Thread.sleep(8000);
		
		
		
	}
	
	public void clickProceedToPay() throws IOException, InterruptedException {
	
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,1250)", "");
		CommonMethod.click(driver, "Click_ProceedTo_Pay");
		Thread.sleep(5000);
		
	}
	
	public void iHereByCheckBox() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,2250)", "");
		CommonMethod.click(driver, "CheckIHereBy_CheckBox");
		
		
	}
	
	
	public void rec_Phone() throws InterruptedException, IOException {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		String  handle= driver.getWindowHandle();//Return a string of alphanumeric window handle
		
	 	driver.switchTo().window(handle);
	 	
		Thread.sleep(8000);
		
		String phone = datatable.getCellData("ReceipientDetails", "phone number", 2);
		CommonMethod.sendKeys(driver, "phone number", phone);
		Thread.sleep(8000);
		
		
	}
	
	public void receipentsDetails() throws InterruptedException, IOException {
		
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);

		String recName = datatable.getCellData("ReceipientDetails", "Recipients Name", 2);
		CommonMethod.sendKeys(driver, "Recp_Name", recName);
		Thread.sleep(6000);
		
		String recAddr = datatable.getCellData("ReceipientDetails", "Recipients Address", 2);
		CommonMethod.sendKeys(driver, "Recp_Addr", recAddr);
		Thread.sleep(6000);
		
		String recPhone = datatable.getCellData("ReceipientDetails", "phone number", 2);
		CommonMethod.sendKeys(driver, "Recp_Phone", recPhone);
		Thread.sleep(8000);
		
		CommonMethod.click(driver, "Click_Save_ContinueBtn");
		
		Thread.sleep(8000);
		
		
	}
	
	
	
	
	public void logoutFnp() throws IOException {
		
		
		
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			Thread.sleep(8000);
			CommonMethod.click(driver, "LogOut_Menu");
			Thread.sleep(8000);
			CommonMethod.click(driver, "Click_Logout");
			Thread.sleep(8000);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	
	/*public void cakeProduct() throws InterruptedException, IOException {
		
		log.info("*************** cakeProduct ******************* ");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		//clickHambergerMenu();
		
		log.info("*************** executed cakeProduct ******************* ");
		
	}*/
	
	
	public void browserScrolldown() throws InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,750)", "");
		
		
	}
	
	
	/*public void selectCakeMenu() throws InterruptedException, IOException {
	
		log.info("*************** selectCakeMenu ******************* ");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(8000);
		//clickHambergerMenu();
		Thread.sleep(8000);
		CommonMethod.click(driver, "select_CakeMenu");
		Thread.sleep(8000);
		browserScrolldown();
		CommonMethod.click(driver, "Selecting_FirstCakeItem");
		selectEggRadioBtn();		
		enterCityName();
		selectingDate();
		selectingTimeSlotsEarliestDelivery();
		//clickAddToCart();
		CommonMethod.click(driver, "Click_ContinueBtn");
		fnpLoginPage();
		clickContinueBtn();
		iHereByCheckBox();
		clickProceedToPay();
		getCreditCardDetails();
		
		log.info("*************** executed selectCakeMenu ******************* ");
		
	}
	*/
	
	
	public void quickSearch_SelectOccasion() throws IOException, InterruptedException {
		
		
		String selectOccasion =datatable.getCellData("QuickSearch", "Occasion", 2);
		
		WebElement occas = driver.findElement(By.xpath("(.//input[@placeholder='Select Occasion'])[4]"));
		
		Thread.sleep(5000);
		occas.sendKeys(selectOccasion);
		Thread.sleep(15000);
		occas.sendKeys(Keys.ARROW_UP);
		Thread.sleep(5000);
		occas.sendKeys(Keys.ENTER);
	}
	
	
	public void quick_SelectDate() throws IOException {
		
		CommonMethod.click(driver, "Quick_SelectDate");
		
	}
	
	public void quick_NextMonthDate() throws IOException, InterruptedException {
		Thread.sleep(4000);
		CommonMethod.click(driver, "Quick_ClickNextMonth");
		Thread.sleep(4000);
	}
	
	public void quick_select_Date() throws IOException, InterruptedException {
		Thread.sleep(4000);
		CommonMethod.click(driver, "Quick_ClickNextMonth_Date");
	}
	
public void quick_select_Calendar_Date() throws IOException, InterruptedException {
	Thread.sleep(4000);
		CommonMethod.click(driver, "Quick_Selecting_CalenderDate");
	}
	
	
	public void quick_selectDate1() throws IOException {
		
		CommonMethod.click(driver, "SelectDate");
	}
	
	public void quickSearch_SelectCity() throws IOException, InterruptedException {
		
		String selectCity =  datatable.getCellData("QuickSearch", "City", 2);
		
		
		WebElement occas = driver.findElement(By.xpath("(.//input[@placeholder='City'])[4]"));
		occas.sendKeys(selectCity);
		Thread.sleep(5000);
		occas.sendKeys(Keys.UP);
		Thread.sleep(5000);
		occas.sendKeys(Keys.ENTER);
		
	}
	
	
	public void click_ShopNowBtn() throws IOException {
		
		CommonMethod.click(driver, "Quick_ShopNow");
		
	}
	
	
		
	public void selectEggRadioBtn() throws InterruptedException, IOException {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);
		CommonMethod.click(driver, "Select_Egg_RadioBtn");
		
		
	}
	
	public void selectingTimeSlotsEarliestDelivery() throws InterruptedException, IOException {
	
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(4000);
		CommonMethod.click(driver, "selectingTimeSlotsEarliestDelivery");
		
		
	}
	
	public void FNPLogoClick() throws IOException, InterruptedException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(4000);
		CommonMethod.click(driver, "Click_FNPLOGO");
		Thread.sleep(4000);
	}
	
	
	public void SearchMenuCLick() throws InterruptedException, IOException {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(4000);
		CommonMethod.click(driver, "click_Search");
		
	}
	
	public void Searchcakess() throws IOException, InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		String recName = datatable.getCellData("SearchCakes", "Search Product", 2);
		CommonMethod.sendKeys(driver, "Fnp_search", recName);
		Thread.sleep(6000);
		
	}
	
	public void clickSearchButton() throws IOException {
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		CommonMethod.click(driver, "click_SearchBtn");
		
	}
	
	public void ProductType() throws IOException {
		CommonMethod.explicitWait(driver, 30, "Product_Type_Selecting");
		CommonMethod.click(driver, "Product_Type_Selecting");
		
	}
	
	
	public void ProductType_Selecting_EgglessProd() throws IOException {
		CommonMethod.explicitWait(driver, 30, "Product_Type_Selecting_EgglessProduct");
		CommonMethod.click(driver, "Product_Type_Selecting_EgglessProduct");
				
	}
	
	public void egglessProduct_Select() throws IOException {
		CommonMethod.explicitWait(driver, 30, "Eggless_First_Product");
		CommonMethod.click(driver, "Eggless_First_Product");
		
	}
	
	public void Eggless_ANd_WithEgg_Products_Checking() throws IOException {
		CommonMethod.explicitWait(driver, 30, "Checking_WithEggProductRadioBtn");
		CommonMethod.click(driver, "Checking_WithEggProductRadioBtn");
	}
	
	
	
	public void selectingoneProduct() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		CommonMethod.click(driver, "selectCakeProduct");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(6000);
		
	}
	
	
	public void selectRadioBn() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(6000);
		CommonMethod.click(driver, "selectingRadioBtn");
		
		
	}
	
		public void selectEggAddons() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
	 	String  handle= driver.getWindowHandle();//Return a string of alphanumeric window handle
		driver.switchTo().window(handle);
		Thread.sleep(5000);
		CommonMethod.click(driver, "SelectingEggAddons");
		Thread.sleep(5000);
		
		}
	
		public void scrollingToBottomofAPage() {
			/*((JavascriptExecutor) driver)
     	    .executeScript("window.scrollTo(0, document.body.scrollHeight)");*/
	
			/*Actions actions = new Actions(driver);
		    actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();*/
	
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,750)", "");
	
		}

	
	public void clickCourierProduct() throws IOException, InterruptedException {
		
		
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Thread.sleep(5000);
		//CommonMethod.sendKeys(driver, "EnterCityPincode", "503");
		WebElement wele = driver.findElement(By.id("localitylookup"));	
		wele.sendKeys("503");
		Thread.sleep(5000);
		wele.sendKeys(Keys.DOWN);
		Thread.sleep(5000);
		wele.sendKeys(Keys.ENTER);
		
	}
	
		
	public void selctingCourierProduct() throws InterruptedException, IOException {
		
			driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
			Thread.sleep(5000);
			CommonMethod.click(driver, "DateFieldClick");
			Thread.sleep(12000);
			CommonMethod.click(driver, "NextMonthDateArrow");
			Thread.sleep(6000);
			CommonMethod.click(driver, "NextMonthDate");
		
	}
	
	public void PersonalizedCoordinates() {
		
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(35,1820)", "");
		
		
	}
	
	public void clicking_PersonalizedProduct() throws IOException{
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		
		CommonMethod.click(driver, "click_Personalized");
	}
	
	public void Uploading_PersonalizedProduct() throws IOException, InterruptedException{
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.findElement(By.id("imageuploadbtn")).sendKeys("G:/sravan/Latest FrameWork/FNP/Tetsdata/Upload.jpg");
		Thread.sleep(5000);
	}
	
	public void Internation_Menu() throws IOException, InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "International_Menu");
		Thread.sleep(4000);
				
	}
	
	 public void International_Submenu() throws IOException {
		
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			CommonMethod.click(driver, "International_USA"); 
	 }
	
	public void International_Product() throws IOException, InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "UAE_FirstProd"); 
		Thread.sleep(4000);
	}
	
	
	public void International_SubMenuUK() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "International_UK_Flowers");	
		Thread.sleep(4000);
	}
	
	public void International_SubMenuUAE() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "International_UAE_Flowers");	
		Thread.sleep(4000);
	}
	
	public void International_UKFlowers_Product() throws IOException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "International_UK_Flowers");		
	}
	
	public void International_DateShippingMethodPlaceholder() throws IOException, InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(6000);
		CommonMethod.click(driver, "International_SelectTimeSlot"); 
	}
	
	public void International_Selecting_DateCalendar() throws IOException, InterruptedException {
		log.info("*************** International_Submenu() ******************* ");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(6000);
		CommonMethod.click(driver, "International_SelectTimeSlot"); 
	}
	
	
	public void International_SubMenuGERMANY() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "International_GERMANY_Flowers");	
		Thread.sleep(4000);
	}
	
	public void International_GermanyFlowers_Product() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(7000);
		CommonMethod.explicitWait(driver, 30, "International_GERMANY_FlowersProduct");
		CommonMethod.click(driver, "International_GERMANY_FlowersProduct");		
	}
	
	public void International_Adding_NewAddress() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Thread.sleep(9000);
		String reName=datatable.getCellData("UKAddress", "Receipient Name", 2);
		CommonMethod.sendKeys(driver, "RecName", reName);
		Thread.sleep(3000);
		String reAddress=datatable.getCellData("UKAddress", "Receipients Addresses", 2);
		CommonMethod.sendKeys(driver, "RecAddress", reAddress);
		Thread.sleep(3000);
		String reCity=datatable.getCellData("UKAddress", "City", 2);
		CommonMethod.sendKeys(driver, "RecCity", reCity);
		Thread.sleep(3000);
		String rePincode=datatable.getCellData("UKAddress", "Pincode", 2);
		CommonMethod.sendKeys(driver, "RecPincode", rePincode);
		Thread.sleep(3000);
		String reMobNo=datatable.getCellData("UKAddress", "Receipients Mobile number", 2);
		CommonMethod.sendKeys(driver, "RecMobileNo", reMobNo);
		Thread.sleep(3000);	
	}
	
	
	public void clickingFlowers_Category() throws IOException, InterruptedException {
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Thread.sleep(6000);
		//browserScrolldown();
		//Thread.sleep(6000);
		CommonMethod.click(driver, "click_FlowersCateg");
		Thread.sleep(4000);
	}
	
	public void getCoordinates(){
		
		Point point=driver.findElement(By.id("loadmoreproducts")).getLocation();
		int xcor = point.x;
		 int ycor = point.y;
		 System.out.println("  "+xcor);
		 System.out.println("  "+ycor);
		//browserScrolldown();
		
	}
	public void clicking_LoadMoreBtn() throws InterruptedException, IOException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
			for (int i=0;; i++) {
			    if(i >=10){
			        break;
			    }
			    
			     JavascriptExecutor jse = (JavascriptExecutor)driver;
				 jse.executeScript("window.scrollBy(500,4400)", "");
				 Thread.sleep(3000);
				 getCoordinates();
				 CommonMethod.explicitWait(driver, 60, "cLICK_lOADmOREbTN");
				 driver.findElement(By.id("loadmoreproducts")).click();
			     Thread.sleep(3000);
			}
	}
			/*Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);*/
		 /*if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		} if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		} if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		} if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		} if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		}if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		}if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		}if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		}
		 if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			Actions clicker = new Actions(driver);
		    clicker.sendKeys(Keys.PAGE_DOWN);
		    Thread.sleep(1000);
		    clicker.click().perform();     
		    Thread.sleep(1000);
			driver.findElement(By.id("loadmoreproducts")).click();
			Thread.sleep(1000);
		}
		
		 if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
			 
			 
				Actions clicker = new Actions(driver);
			    clicker.sendKeys(Keys.PAGE_DOWN);
			    Thread.sleep(1000);
			    clicker.click().perform();     
			    Thread.sleep(1000);
				driver.findElement(By.id("loadmoreproducts")).click();
				
			}
		 if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
				Actions clicker = new Actions(driver);
			    clicker.sendKeys(Keys.PAGE_DOWN);
			    Thread.sleep(1000);
			    clicker.click().perform();     
			    Thread.sleep(1000);
				driver.findElement(By.id("loadmoreproducts")).click();
				Thread.sleep(1000);
			}
		 if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
				Actions clicker = new Actions(driver);
			    clicker.sendKeys(Keys.PAGE_DOWN);
			    Thread.sleep(1000);
			    clicker.click().perform();     
			    Thread.sleep(1000);
				driver.findElement(By.id("loadmoreproducts")).click();
				Thread.sleep(1000);
			}
		 if(driver.findElement(By.id("loadmoreproducts")).isDisplayed()){
				Actions clicker = new Actions(driver);
			    clicker.sendKeys(Keys.PAGE_DOWN);
			    Thread.sleep(1000);
			    clicker.click().perform();     
			    Thread.sleep(1000);
				driver.findElement(By.id("loadmoreproducts")).click();
				Thread.sleep(1000);
			}*/
	
	public void deliveryCity_Selecting() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Filters_DeliveryCity");
		Thread.sleep(4000);
	}
	
	public void deliveryCity_SearchCityName() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.sendKeys(driver, "Filters_SearchCity", "Hyderabad");
		Thread.sleep(4000);
		CommonMethod.explicitWait(driver, 30, "Filters_SearchCitySelecting");
		CommonMethod.click(driver, "Filters_SearchCitySelecting");
		Thread.sleep(4000);
	}
	
	public void filters_PriceMenu() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Filters_Price");
		Thread.sleep(4000);
		CommonMethod.explicitWait(driver, 30, "Filters_Price_CheckBox");
		CommonMethod.click(driver, "Filters_Price_CheckBox");
		Thread.sleep(4000);
	}
	
	public void filters_DeliveryDate() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Filters_Delivery_Date_Calendar");
		Thread.sleep(4000);
	}
	
	public void filters_CalendarDateSelecting() throws IOException, InterruptedException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Filters_Delivery_Date_Calendar");
		Thread.sleep(4000);
		CommonMethod.explicitWait(driver, 30, "Filters_Calendar_NextMonth");
		CommonMethod.click(driver, "Filters_Calendar_NextMonth");
		Thread.sleep(4000);
		CommonMethod.click(driver, "Filters_Calendar_SelectDate");
	}
	
	public void filtersFlowersProduct() throws IOException{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		CommonMethod.explicitWait(driver, 30, "Filters_FlowersProduct");
		CommonMethod.click(driver, "Filters_FlowersProduct");
		
	}
	
	public void filters_SelectingCity() throws IOException, InterruptedException{
			
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		Thread.sleep(5000);
		//String cityName = datatable.getCellData("CityDetails", "City Name", 2);
		/*CommonMethod.sendKeys(driver, "EnterCityName", cityName);
		Thread.sleep(5000);*/
		
		WebElement wele = driver.findElement(By.id("pincodelookup"));	
		wele.sendKeys("500");
		Thread.sleep(5000);
		wele.sendKeys(Keys.DOWN);
		Thread.sleep(5000);
		
		wele.sendKeys(Keys.ENTER);
		Thread.sleep(6000);
		
	}
	
	public void filters_SelectDate_Timeslots() throws IOException, InterruptedException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		/*CommonMethod.click(driver, "DateFieldClick");
		Thread.sleep(5000);*/
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(6000);
	}
	
public void filters_SelectDate_Timeslot() throws IOException, InterruptedException {
		
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		CommonMethod.click(driver, "DateFieldClick");
		Thread.sleep(5000);
		CommonMethod.click(driver, "NextMonthDateArrow");
		Thread.sleep(6000);
		CommonMethod.click(driver, "NextMonthDate");
		Thread.sleep(6000);
	}
	
	
	public void checkoutFlowBackNavigation() throws InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().back();
		Thread.sleep(7000);
	}
	
	public void checkingFlower_VariantProduct() throws IOException, InterruptedException{
	
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if(driver.findElement(By.xpath("//li[2]/figure/img")).isDisplayed()){
			Thread.sleep(3000);
			CommonMethod.click(driver, "MediumVariantProd");
			
		}
		if(driver.findElement(By.xpath("//li[3]/figure/img")).isDisplayed()){
			Thread.sleep(3000);
			CommonMethod.click(driver, "LargeVariantProd");
			
		}
	}
	
	public void germanyCakesSearch() throws IOException, InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		String cakes = datatable.getCellData("GermanyCakes", "Cakes", 2);
		CommonMethod.sendKeys(driver, "Germany_Cake_Search", cakes);
		Thread.sleep(4000);
		
	}
	
	public void germanyCakes_SearchBtn() throws IOException, InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Germany_Cake");
		Thread.sleep(5000);
	}
	
	public void germany_CakesProd() throws IOException, InterruptedException {
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Germany_CakeProduct");
		Thread.sleep(5000);
	}
	
	public void UAE_ChocolatesMenu() throws IOException, InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		CommonMethod.click(driver, "UAE_ChocolateMenu");
		Thread.sleep(5000);
	}
	
	public void Personalized_Search() throws IOException, InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		String searchText = datatable.getCellData("PersonalizedSearch", "PersonalizedText", 2);
		CommonMethod.sendKeys(driver, "Personalized_Search", searchText);
		Thread.sleep(4000);
		
	}
	
	
	public void Personalized_Search_Product() throws IOException, InterruptedException{
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		CommonMethod.click(driver, "Personalized_Search_ProductSelection");
		Thread.sleep(4000);
		
	}
	
	public void navigatetoHomePage(){
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		System.out.println("******************executed navigatetoHomePage************************");
		driver.navigate().to("http://fnp.com");
	}
	
	
	public void Fnp_Logout_Click() throws IOException{
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		try {
			CommonMethod.click(driver, "LogOut_Menu");
			Thread.sleep(8000);
			CommonMethod.click(driver, "Click_Logout");
			Thread.sleep(8000);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
		 public  int generateRandomNumber()
		 {
			    
			    int min = 2;
			   // int max = 9517;
			    int max = 40;


			    Random r = new Random();
			    int i1 = r.nextInt(max - min + 1) + min;
			    
			     return i1;    
	     }
			 
			 
			 public String selectOriginAreaCode()
			 
		 {
			 
			  int number=generateRandomNumber();
			  System.out.println(number);
			  
			  String areaCode= datatable.getCellData("FlightsTestData","Area Code",number);
			  System.out.println(areaCode);
			 
			  size=datatable.getRowCount("CentraFinalData");
			  
			  datatable.setCellData("CentraFinalData", "OriginCode", size+1, areaCode);
			  
			  return areaCode;

		 }
			 
		    
     					public void pnrNumber()throws Exception
     					
     					{
     					Thread.sleep(6000);
     					String pnr=	CommonMethod.getText(driver, "pnr_Number");
     					System.out.println("Pnr Number"+pnr);
     					CommonMethod.takesScreenshot(driver, "pnrpage", "sucess");
					    datatable.setCellData("CentraFinalData", "pnrnumber", size+1, pnr);
					   }
     					
     					public void handleSliders(WebDriver driver,String objectLocater) throws IOException, InterruptedException
     					
     					{
     					   
     						//Locate slider pointer.
     						WebElement dragElementFrom = CommonMethod.findElement(driver, objectLocater);     
     						new Actions(driver).dragAndDropBy(dragElementFrom, 30, 0).build().perform();
     						Thread.sleep(5000);
     						new Actions(driver).clickAndHold(dragElementFrom).moveByOffset(20,0).release().perform(); 
     	                }


     					
     					
}



package com.Rsv.Assertions;

import static com.framework.driver.CommonMethod.sleep;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

import mx4j.log.Log;

public class RsvAssertions extends TestBase {
	
	public static void verifyProtocolNumberForCreateStudy() throws IOException
    {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		String protocolNumber=datatable.getCellData("CtmsData", "Data", 3);
		CommonMethod.explicitWait(driver, 5, "STUDY");
		CommonMethod.click(driver, "STUDY");
		CommonMethod.sendKeys(driver, "SEARCHTXTBOX", protocolNumber);
		CommonMethod.selectDropdown(driver, "SRCHDROPDOWN", "Protocol Number / IRB");
		sleep(2, TimeUnit.SECONDS);
		CommonMethod.click(driver, "SRCHNOWBUTTON");
		try {
			CommonMethod.explicitWait(driver, 4, "NOSTUDYFOUND");
			String actual = CommonMethod.getText(driver, "NOSTUDYFOUND").trim();
			Assert.assertEquals(actual, "No Study Found !", "No Study Found !");
		} catch (Exception e) {
			String uiStudyName=driver.findElement(By.xpath("//*[@title='Study'][text()='"+protocolNumber+"']")).getText();
			Assert.assertNotEquals(protocolNumber, uiStudyName, "Required Study already created, So no need to create the study again");
		}
    }
	
	public static void verifyProtocolNumber() throws IOException {
  	  
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	String protocolNumber=datatable.getCellData("CtmsData", "Data", 3);
  		CommonMethod.explicitWait(driver, 10, "STUDY");
  		
  		CommonMethod.click(driver, "STUDY");
  		CommonMethod.sendKeys(driver, "SEARCHTXTBOX", protocolNumber);
  		CommonMethod.selectDropdown(driver, "SRCHDROPDOWN", "Protocol Number / IRB");
  		CommonMethod.click(driver, "SRCHNOWBUTTON");
		try {
			String uiStudyName=driver.findElement(By.xpath("//*[@title='Study'][text()='"+protocolNumber+"']")).getText();
			Assert.assertEquals(protocolNumber, uiStudyName, "Study not created successfully");
		} catch (Exception e) {
			CommonMethod.explicitWait(driver, 5, "NOSTUDYFOUND");
			String actual = CommonMethod.getText(driver, "NOSTUDYFOUND").trim();
			Assert.assertEquals(actual, "No Study Found !", "Study not created successfully");
			//CommonMethod.takesScreenshot(driver, "FNPHOMETITLE", "Sucessfull");
		}
	}
		
    public static void verifyCreateSite(String siteID) throws IOException{
    	try {
			String siteInfo=CommonMethod.getText(driver, "SITEINFO");
			Assert.assertTrue(siteInfo.contains(siteID), "Site ID "+siteID+" is not create successfully");
		} catch (Exception e) {
			Assert.assertFalse(e.getMessage().contains("org.selenium.NoSuchElementException"), "Site ID "+siteID+" is not create successfully");
			
		}
    }
      
      
      
      
      
      
    public static void searchFlight(WebDriver driver) throws IOException
    {
        Assert.assertEquals(CommonMethod.getText(driver, "Search_Flights"), "Search flights");
    	CommonMethod.takesScreenshot(driver, "verifySearchFlight", "Success");
    		
    }
    
    
     public static void verifyLogin(WebDriver driver) throws IOException
    {
		if(CommonMethod.getText(driver, "Login_Homepage").contains("Login"))
		{
			System.out.println("login link display sucessfull");
			CommonMethod.takesScreenshot(driver, "loginlink", "Success");
		}
		else
		{
			System.out.println("login link Failed");
			CommonMethod.takesScreenshotOnFailure(driver, "loginlink", "Failed");
			throw new RuntimeException("login Failed");

        }
    }

    
    public static void verifyCentraLoginpage(WebDriver driver) throws IOException, InterruptedException
    {
    	
 		if(CommonMethod.isElementPresent(driver, "SignUpButton_Login"))
 		{
 			System.out.println("Login page successfully displayed");
 			CommonMethod.takesScreenshot(driver, "verifyCentraLoginpage", "Success");
 		}
 		else
 		{
 			System.out.println("verifyCentraLoginpage Failed");
 			CommonMethod.takesScreenshotOnFailure(driver, "verifyCentraLoginpage", "Failed");
 			throw new RuntimeException("Centra Login page not displayed");

 		}
 	}
    
    public static void verifyChangeButton(WebDriver driver) throws Exception
    {
 		if(CommonMethod.isElementPresent(driver, "Change_Button"))
 		{
 			
 			System.out.println("flights available for your search.");
 			CommonMethod.takesScreenshot(driver, "verifyChangeButton", "Success");
 			Thread.sleep(6000);
 		}
 		else
 		{
 			System.out.println("No flight search avialable");
 		
 			CommonMethod.takesScreenshotOnFailure(driver, "verifyChangeButton", "Failed");
 			
 			
 			//throw new RuntimeException("Sorry, there aren't any flights available for your search.");

 		}
 	}


     
    
    public static void verifyNonstop(WebDriver driver) throws IOException
     {
    	Assert.assertEquals(CommonMethod.getText(driver, "Verify_NonStop"), "non-stop");
    	CommonMethod.takesScreenshot(driver, "Verify_NonStop", "Success");
     }



     public static void verifyContinueBooking(WebDriver driver) throws IOException
     {
		if(CommonMethod.isElementPresent(driver, "Continue_Booking"))
		{
		    //System.out.println("flights available for your search.");
			CommonMethod.takesScreenshot(driver, "verifyContinueBooking", "Success");
		}
		else
		{
			System.out.println("Not opened continuebooking");
		    CommonMethod.takesScreenshotOnFailure(driver, "verifyContinueBooking", "Failed");
			//throw new RuntimeException("Sorry, there aren't any flights available for your search.");

		}
	 }


    public static void verifyEmailPage(WebDriver driver) throws IOException
  {
	   if(CommonMethod.isElementPresent(driver, "Continue_Button"))
		{
			
			//System.out.println("flights available for your search.");
			CommonMethod.takesScreenshot(driver, "Continue_Button", "Success");
		}
		else
		{
			System.out.println("Not displayed email page");
		
			CommonMethod.takesScreenshotOnFailure(driver, "verifyEmailPage", "Failed");
			
			
			//throw new RuntimeException("Sorry, there aren't any flights available for your search.");

		}
    }
		 
      
        public static void verifyTravels(WebDriver driver) throws IOException
	 {
				if(CommonMethod.getText(driver, "Verify_BaggaegInformation").contains("Baggage Information"))
				{
					System.out.println("Travels form display sucessfull");
					CommonMethod.takesScreenshot(driver, "verifyTravels", "Success");
				}
				else
				{
					System.out.println("Travelsl page not displayed");
					CommonMethod.takesScreenshotOnFailure(driver, "verifyTravels", "Failed");
					throw new RuntimeException("Verify_BaggaegInformation Failed");

				}
		
     }
		  
		 
      public static void verifyBookingReview(WebDriver driver) throws IOException
	 {
				if(CommonMethod.getText(driver, "Block_BookingReview").contains("Block"))
				{
					System.out.println("BookingBlock displayed sucess");
					CommonMethod.takesScreenshot(driver, "verifyBookingReview", "Success");
				}
				else
				{
					System.out.println("verifyBookingReview failed");
					CommonMethod.takesScreenshotOnFailure(driver, "verifyBookingReview", "Failed");
					throw new RuntimeException("BookingReview Failed");

				}
	}  
		  
		 
      
      public static void verifyPnrSucess(WebDriver driver) throws IOException
	 {
				if(CommonMethod.getText(driver, "Pnr_Sucess").contains("PNR successfully generated."))
				{
					System.out.println("BookingBlock displayed sucess");
					CommonMethod.takesScreenshot(driver, "verifyPnrSucess", "Success");
				}
				else
				{
					System.out.println("verifyPnrSucess failed");
					CommonMethod.takesScreenshotOnFailure(driver, "verifyPnrSucess", "Failed");
					throw new RuntimeException("verifyPnrSucess Failed");

				}
		    }  
		  
      public static void verifyFlightFilter(WebDriver driver) throws IOException
 	 {
 				if(CommonMethod.getText(driver, "Errormsg_FlightsFilter").contains("We couldn't find flights to match your filters"))
 				{
 					System.out.println("We couldn't find flights to match your filters");
 				CommonMethod.takesScreenshot(driver, "verifyTravels", "Success");	
 					
 				}
 				else
 				{
 					System.out.println("fights list available");
 					
 					CommonMethod.takesScreenshot(driver, "verifyFlightFilter", "Success");
 					//throw new RuntimeException("Verify_BaggaegInformation Failed");

 				}
 		
      } 
     

}


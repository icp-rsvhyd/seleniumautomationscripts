package com.Rsv.ReUsableMethods;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;

public class RsvMethods extends TestBase{
	
	SoftAssert softAssert=new SoftAssert();
	
	/**
	 * To login required application
	 * @author Shiva(Radinat Sage Tech India Services Pvt Ltd)
	 */
	
	@Test
	public void logIn() throws IOException, InterruptedException {
		
		String s1=driver.getTitle();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonMethod.sendKeys(driver, "USERNAME", datatable.getCellData("CtmsLogin", "Uname", 2));
		CommonMethod.sendKeys(driver, "PASSWORD", datatable.getCellData("CtmsLogin", "Pwd", 2));
		CommonMethod.click(driver, "SIGNIN_BUTTON");
		Thread.sleep(3000);
		
		String s2=driver.getTitle();
		/*System.out.println(s1);
		System.out.println(s2);
		System.out.println(s1.compareToIgnoreCase(s2));*/

		if(s1.compareToIgnoreCase(s2)==0){
			log.info("*************** User logged in different Mechine, Driver trying to Loginn again******************* ");
			CommonMethod.sendKeys(driver, "PASSWORD", datatable.getCellData("CtmsLogin", "Pwd", 2));
			CommonMethod.click(driver, "SIGNIN_BUTTON");
			} else {
				Reporter.log(datatable.getCellData("CtmsLogin", "Uname", 2)+" User Successfully logged into the CTMS Application");
		}
		Reporter.log(datatable.getCellData("CtmsLogin", "Uname", 2)+" User Successfully logged into the CTMS Application");
		
	}
	
	/**
	 * To selct the required StudyName/ProtocolNumber in CTMS application
	 * @author Shiva(Radinat Sage Tech India Services Pvt Ltd)
	 */
	public void selectStudyInCtms() throws IOException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String protocolNumber=datatable.getCellData("CtmsData", "Data", 3).trim();
		CommonMethod.sleep(5, TimeUnit.SECONDS);
		CommonMethod.click(driver, "STUDY");
		CommonMethod.sendKeys(driver, "SEARCHTXTBOX", protocolNumber);
		CommonMethod.selectDropdown(driver, "SRCHDROPDOWN", "Protocol Number / IRB");
		CommonMethod.click(driver, "SRCHNOWBUTTON");
		CommonMethod.sleep(2, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@title='Study'][text()='"+protocolNumber+"']")).click();
		Reporter.log("Protocol Number is "+protocolNumber);
	
	}
}
		
	
	
	


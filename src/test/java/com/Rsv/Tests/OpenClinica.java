package com.Rsv.Tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.tools.ant.taskdefs.Sleep;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Rsv.ReUsableMethods.RsvMethods;
import com.framework.driver.CommonMethod;
import static com.framework.driver.CommonMethod.click;
import static com.framework.driver.CommonMethod.clear;
import static com.framework.driver.CommonMethod.sendKeys;
import static com.framework.driver.CommonMethod.sleep;
import static com.framework.driver.CommonMethod.explicitWait;
import static com.framework.driver.CommonMethod.getText;
import static com.framework.driver.CommonMethod.getAttribute;
import static com.framework.driver.CommonMethod.mouseMoveToElement;
import static com.framework.driver.CommonMethod.findElement;
import static com.framework.driver.CommonMethod.selectDropdown;
import static com.framework.driver.CommonMethod.uploadFileWithAutoIT;
import com.framework.driver.TestBase;

public class OpenClinica extends TestBase {

	RsvMethods methods = new RsvMethods();
	SoftAssert softAssert = new SoftAssert();

	@Test(priority = 1)
	public void logIn() throws IOException, InterruptedException {
		methods.logIn();

	}

	@Test(priority = 2)
	public void changeStudy() throws IOException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String reqStudyName = datatable.getCellData("CtmsData", "Data", 3).trim();
		String currentStudyName = getText(driver, "OCSTUDYNAME");
		Assert.assertNotEquals(reqStudyName, currentStudyName,
				"Required Study Name and Current Study Name both are equal");
		log.info("Required Study Name " + reqStudyName + " and Current Study Name " + currentStudyName
				+ " both are equal");
		click(driver, "CHANGESTUDYSITE");
		String s1 = CommonMethod.getText(driver, "TITLEMANAGE");
		System.out.println(s1 + " *********");
		Assert.assertEquals(s1, "Change Your Current Study", "Study List is not displayed to change study name");
		int i = 1;
		while (true) {
			sleep(1, TimeUnit.SECONDS);
			WebElement studyName = driver.findElement(By.xpath("(.//td[@class='table_cell']//b)[" + i + "]"));
			String rawStudyName = studyName.getText();
			Assert.assertNotEquals(reqStudyName, currentStudyName,
					"Required Study Name and Current Study Name both are not equal, Hence the change study is unsuccessful");
			System.out.println(
					rawStudyName + " ******Study Name not matched with the required Study Name: " + reqStudyName);
			if (rawStudyName.contains(reqStudyName)) {
				System.out.println("found the subject name need to change");
				// Radio button element can identified by the xpath by the reqStudyName
				WebElement reqRadioButton = driver
						.findElement(By.xpath("(.//td[@class='table_cell']//b[contains(text(),'" + reqStudyName
								+ "')]/..//input[@type='radio'])"));
				sleep(1, TimeUnit.SECONDS);
				reqRadioButton.click();
				log.info(reqStudyName + " Radio button is selected");
				click(driver, "CHANGESTUDYSUBMIT");
				click(driver, "CHANGESTUDYSUBMIT");
				// sleep(2, TimeUnit.SECONDS);
				Assert.assertEquals(reqStudyName, getText(driver, "OCSTUDYNAME"),
						"Required Study Name and Current Study Name both are not equal, Hence the change study is unsuccessful");
				log.info(reqStudyName + " is selected as Current Study successfully");
				break;
				/*
				 * if (!reqRadioButton.isSelected()) { reqRadioButton.click();
				 * log.info(reqStudyName+" Radio button is selected"); click(driver,
				 * "CHANGESTUDYSUBMIT"); click(driver, "CHANGESTUDYSUBMIT"); //sleep(2,
				 * TimeUnit.SECONDS); Assert.assertEquals(reqStudyName, getText(driver,
				 * "OCSTUDYNAME"),
				 * "Required Study Name and Current Study Name both are not equal, Hence the change study is unsuccessful"
				 * ); log.info(reqStudyName+" is selected as Current Study successfully");
				 * break; }
				 */
			}
			i++;
		}

	}

	@Test(priority = 3)
	public void uploadCRF() throws IOException, InterruptedException {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		int crfNo = 2;
		while (true) {
			String fileName = datatable.getCellData("OpenClinica", "FileName", crfNo).trim();
			if (fileName == null || fileName.equals("")) {
				int totalNumberOfCRFs = crfNo - 2;
				if (totalNumberOfCRFs == 0) {
					log.info("File name is not mentioned in Excel sheet, hence no CRFs has been Uploaded");
					break;
				} else {
					log.info("All " + totalNumberOfCRFs + " CRFs uploaded successfully");
					sleep(2, TimeUnit.SECONDS);
					break;
				}

			}
			mouseMoveToElement(driver, "OCTASKS");
			click(driver, "OCBUILDSTUDY");
			click(driver, "CREATECRFADDICON");
			click(driver, "CRFCHOOSEFILE");
			// To upload File by using AutoIT
			uploadFileWithAutoIT(driver, fileName);
			sleep(10, TimeUnit.SECONDS);
			click(driver, "PREVIEWCRFVER");
			sleep(2, TimeUnit.SECONDS);
			try {
				String posAlert = getText(driver, "OCPALERT");
				log.info("Positive Alert message " + posAlert);
				sleep(2, TimeUnit.SECONDS);
				click(driver, "OCCONTINUE");
				log.info("Clicked on continue button");
			} catch (Exception e) {
				log.info("Failed to upload CRF : " + e.getMessage());
				String negAlert = getText(driver, "OCNALERT");
				log.info("Negative Alert message " + negAlert);
			}
			sleep(2, TimeUnit.SECONDS);
			// Verifying the CRF upload with CRF count
			mouseMoveToElement(driver, "OCTASKS");
			click(driver, "OCBUILDSTUDY");
			sleep(2, TimeUnit.SECONDS);
			String crfCount1 = driver.findElement(By.xpath("//td[contains(text(),'Create CRF')]/../td[4]")).getText();
			int crfCount = Integer.parseInt(crfCount1);
			System.out.println(crfCount);
			Assert.assertEquals(crfNo - 1, crfCount, crfCount + " no CRF uploaded successfully");
			log.info("Your CRF has been saved in Database successfully and CRF Count for the study is: " + crfCount);
			crfNo++;
		}
	}

	@Test(priority = 5)
	public void modifyStudySettingInOC() throws IOException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		mouseMoveToElement(driver, "OCTASKS");
		click(driver, "OCBUILDSTUDY");
		click(driver, "CREATSTUDYEDIT");
		// need Scroll
		selectDropdown(driver, "STUDYPHASE", datatable.getCellData("OpenClinica", "StudyPhase", 2));
		// need Scroll
		click(driver, "CONDITIONSELIGIBILITY");
		sendKeys(driver, "EXPTOTALENROLLMENT", datatable.getCellData("OpenClinica", "ExpectedTotalEnrollment", 2));
		click(driver, "STUDYPARMCONFIG");
		sleep(2, TimeUnit.SECONDS);
		String discrepancyManagement = datatable.getCellData("OpenClinica", "ExpectedTotalEnrollment", 2);
		if (discrepancyManagement.equalsIgnoreCase("Yes")) {
			click(driver, "DISCREPANCYMANAGEMENTYES");
			log.info("Allow Discrepancy Management?: " + discrepancyManagement);
		} else {
			click(driver, "DISCREPANCYMANAGEMENTNO");
			log.info("Allow Discrepancy Management?: " + discrepancyManagement);
		}
		sleep(2, TimeUnit.SECONDS);
		String personIdRequired = datatable.getCellData("OpenClinica", "PerssonIDRequired", 2);
		if (personIdRequired.equalsIgnoreCase("required")) {
			click(driver, "PERSONIDREQ");
			log.info("Person ID Required?:	" + personIdRequired);
		} else if (personIdRequired.equalsIgnoreCase("optional")) {
			click(driver, "PERSONIDOPTIONAL");
			log.info("Person ID Required?:	" + personIdRequired);
		} else if (personIdRequired.equalsIgnoreCase("not used")) {
			click(driver, "PERSONIDNOTUSED");
			log.info("Person ID Required?:	" + personIdRequired);
		}
		sleep(2, TimeUnit.SECONDS);
		String eventLocationRequired = datatable.getCellData("OpenClinica", "EventLocationRequired", 2);
		if (eventLocationRequired.equalsIgnoreCase("required")) {
			click(driver, "EVENTLOCATIONREQ");
			log.info("Event Location Required?	" + eventLocationRequired);
		} else if (eventLocationRequired.equalsIgnoreCase("optional")) {
			click(driver, "EVENTLOCATIONOPTIONAL");
			log.info("Event Location Required? " + eventLocationRequired);
		} else if (eventLocationRequired.equalsIgnoreCase("not used")) {
			click(driver, "PERSONIDNOTUSED");
			log.info("Event Location Required? " + eventLocationRequired);
		}
		sleep(2, TimeUnit.SECONDS);
		String displayStudyname = datatable.getCellData("OpenClinica", "DisplayStudyNameOnCRF", 2);
		if (displayStudyname.equalsIgnoreCase("Yes")) {
			click(driver, "DISPLAYSTUDYNAMEYES");
			log.info("Display Study Name on CRF: " + displayStudyname);
		} else {
			click(driver, "DISPLAYSTUDYNAMENO");
			log.info("Display Study Name on CRF: " + displayStudyname);
		}
		sleep(2, TimeUnit.SECONDS);
		String displayVisitDescription = datatable.getCellData("OpenClinica", "DisplayVisitDescriptionOnCRF", 2);
		if (displayVisitDescription.equalsIgnoreCase("Yes")) {
			click(driver, "DISPLAYVISITDESCYES");
			log.info("Display Visit Description On CRF: " + displayVisitDescription);
		} else {
			click(driver, "DISPLAYVISITDESCNO");
			log.info("Display Visit Description On CRF: " + displayVisitDescription);
		}
		sleep(2, TimeUnit.SECONDS);
		sendKeys(driver, "DISPLAYVISITDESCNO", Keys.END);
		sleep(2, TimeUnit.SECONDS);
		click(driver, "OCSUBMIT");
		sleep(2, TimeUnit.SECONDS);
		log.info("Modified study settings in OC successfully");
	}

	@Test(priority = 4)
	public void editTimepointName() throws IOException {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		mouseMoveToElement(driver, "OCTASKS");
		click(driver, "OCBUILDSTUDY");
		sleep(2, TimeUnit.SECONDS);
		click(driver, "CREATEEVENTDEFVIEW");
		int visit = 2;
		while (true) {
			String visitName = datatable.getCellData("AddVisits", "VisitName", visit).trim();
			String newVisitName = datatable.getCellData("AddVisits", "NewVisitName", visit).trim();
			if (visitName != null && newVisitName != null && !visitName.isEmpty() && !visitName.equals(newVisitName)) {
				log.info(visitName + " visit Name change is started");
				sleep(1, TimeUnit.SECONDS);
				sendKeys(driver, "OCVISITFILTER", visitName);
				sleep(1, TimeUnit.SECONDS);
				click(driver, "OCFINDBUTTON");
				sleep(1, TimeUnit.SECONDS);
				// Visit Edit button
				try {
					driver.findElement(By.xpath("//td[text()='" + visitName + "']/..//a/img[@title='Edit']")).click();
					log.info("'" + visitName + "' visit 'Edit' button clicked");
				} catch (Exception e) {
					log.info("'" + visitName + "' visit name is not available or created for the study in CTMS");
					String ocMessage = getText(driver, "OCMESSAGE");
					softAssert.assertEquals(visitName, ocMessage,
							"Visit name is not available or not created for the study in CTMS");
				}
				sleep(2, TimeUnit.SECONDS);
				clear(driver, "OCVISITNAME");
				sendKeys(driver, "OCVISITNAME", newVisitName);
				sleep(1, TimeUnit.SECONDS);
				sendKeys(driver, "OCVISITNAME", Keys.PAGE_DOWN);
				sleep(1, TimeUnit.SECONDS);
				click(driver, "OCSUBMIT");
				sleep(2, TimeUnit.SECONDS);
				click(driver, "CINFIRMFINISH");
				log.info("'" + visitName + "' is cahnged to " + newVisitName + " successfully");
				sleep(2, TimeUnit.SECONDS);
				visit++;

			} else if (visitName == null || visitName.equals("") || newVisitName.equals("") || newVisitName == null) {
				int totalNumberOfVisits = visit - 2;
				log.info("All " + totalNumberOfVisits + " Visits Names are changed successfully");
				sleep(2, TimeUnit.SECONDS);
				break;
			}

		}
		softAssert.assertAll();
	}

	@Test(priority = 6)
	public void addCRFToEachTimepoint() throws IOException {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		mouseMoveToElement(driver, "OCTASKS");
		click(driver, "OCBUILDSTUDY");
		sleep(2, TimeUnit.SECONDS);
		click(driver, "CREATEEVENTDEFVIEW");
		int visit = 2;
		while (true) {
			String visitName = datatable.getCellData("AddVisits", "NewVisitName", visit).trim();
			String crfName = datatable.getCellData("AddVisits", "CRFName", visit).trim();
			if (visitName == null || visitName.equals("")) {
				int totalNumberOfVisits = visit - 2;
				sleep(2, TimeUnit.SECONDS);
				break;
			}
			if (!crfName.equals("") && crfName != null) {
				log.info(visitName + " visit Upload CRF is started");
				sleep(2, TimeUnit.SECONDS);
				sendKeys(driver, "OCVISITFILTER", visitName);
				sleep(1, TimeUnit.SECONDS);
				click(driver, "OCFINDBUTTON");
				try {
					sleep(1, TimeUnit.SECONDS);
					// Click on 'Edit' icon for the visit under Actions column
					driver.findElement(By.xpath("//td[text()='" + visitName + "']/..//a/img[@title='Edit']")).click();
					// click(driver, "VISITEDITBUTTON");
					log.info("'" + visitName + "' visit 'Edit' button clicked");
					String ocVisitName = getAttribute(driver, "OCVISITNAME");
					System.out.println(visitName + "==" + ocVisitName);
					Assert.assertEquals(visitName, ocVisitName,
							"Visit name is not available or not created for the study in CTMS");
					click(driver, "ADDNEWCRF");
					// String crfName=datatable.getCellData("OpenClinica", "CRFName", 2).trim();
					System.out.println(crfName + "CRF Name to search");
					sendKeys(driver, "OCCRFFILTER", crfName);
					click(driver, "OCFINDBUTTON");
					try {
						sleep(2, TimeUnit.SECONDS);
						// Check 'Selected' check box for the required CRF
						click(driver, "OCCRFCHECKBOX");
						log.info("'" + crfName + "' check box selected");
						click(driver, "OCADDBUTTON");
						click(driver, "OCSUBMIT");// If not work need Scrolling
						click(driver, "CONFIRMFINISH");
						log.info("'" + crfName + "' is uploaded to " + visitName + " successfully");
					} catch (Exception e) {
						log.info("'" + crfName + "' CRF name is not available or uploaded");
						String ocMessage = getText(driver, "OCMESSAGE");
						Assert.assertEquals(crfName, ocMessage, "CRF name is not available or not uploaded");
					}
				} catch (Exception e) {
					log.info("'" + visitName + "' visit name is not available or created for the study in CTMS");
					String ocMessage = getText(driver, "OCMESSAGE");
					softAssert.assertEquals(visitName, ocMessage,
							"Visit name is not available or not created for the study in CTMS");
				}

			} else {
				log.info("In Excel sheet no CRF is available for the Visit " + visitName);

			}
			visit++;
		}
		softAssert.assertAll();
	}

}

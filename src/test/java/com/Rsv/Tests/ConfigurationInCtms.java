package com.Rsv.Tests;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.Rsv.Assertions.RsvAssertions;
import com.Rsv.ReUsableMethods.RsvMethods;
import com.framework.driver.CommonMethod;
import static com.framework.driver.CommonMethod.sleep;
import static com.framework.driver.CommonMethod.takesScreenshot;
import static com.framework.driver.CommonMethod.click;
import static com.framework.driver.CommonMethod.clear;
import static com.framework.driver.CommonMethod.sendKeys;
import static com.framework.driver.CommonMethod.explicitWait;
import static com.framework.driver.CommonMethod.getText;
import static com.framework.driver.CommonMethod.selectDropdown;
import com.framework.driver.TestBase;

@Listeners(com.framework.utilities.TestNGListners.class)
public class ConfigurationInCtms extends TestBase {

	RsvMethods methods = new RsvMethods();
	RsvAssertions rsvAssert = new RsvAssertions();

	@Test(priority = 1)
	public void LogIn() throws IOException, InterruptedException {

		String s1 = driver.getTitle();
		explicitWait(driver, 20, "USERNAME");
		sendKeys(driver, "USERNAME", datatable.getCellData("CtmsLogin", "Uname", 2));
		sendKeys(driver, "PASSWORD", datatable.getCellData("CtmsLogin", "Pwd", 2));
		click(driver, "SIGNIN_BUTTON");
		sleep(3, TimeUnit.SECONDS);

		String s2 = driver.getTitle();
		/*
		 * System.out.println(s1); System.out.println(s2);
		 * System.out.println(s1.compareToIgnoreCase(s2));
		 */

		if (s1.compareToIgnoreCase(s2) == 0) {
			log.info(
					"*************** User logged in different Mechine, Driver trying to Loginn again******************* ");
			sendKeys(driver, "PASSWORD", datatable.getCellData("CtmsLogin", "Pwd", 2));
			click(driver, "SIGNIN_BUTTON");
		} else {
			Reporter.log(datatable.getCellData("CtmsLogin", "Uname", 2)
					+ " User Successfully logged into the CTMS Application");
		}
		Reporter.log(
				datatable.getCellData("CtmsLogin", "Uname", 2) + " User Successfully logged into the CTMS Application");

	}

	@Test(priority = 2)
	public void createStudyInCtms() throws IOException, InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		RsvAssertions.verifyProtocolNumberForCreateStudy();
		explicitWait(driver, 5, "STUDY");
		CommonMethod.mouseMoveToElement(driver, "STUDY");
		click(driver, "ADDSTUDY");
		sleep(2, TimeUnit.SECONDS);
		click(driver, "SPONSERNAMEADDBUTTON");
		sendKeys(driver, "SPONSERSEARCH", datatable.getCellData("CtmsData", "Data", 2));
		click(driver, "SEARCHNOWBTN");
		sleep(2, TimeUnit.SECONDS);
		String sponsername = datatable.getCellData("CtmsData", "Data", 2);
		log.info("*************** Sponcer name is : " + sponsername + "******************* ");
		System.out.println(".//*[@class='lvtColData']//a[text()='" + sponsername + "']");
		sleep(2, TimeUnit.SECONDS);
		try {
			driver.findElement(By.xpath(".//a[text()='" + sponsername + "']")).click();

		} catch (Exception e) {

			System.out.println(".//*[@class='lvtColData']//a[text()='" + sponsername + "']" + " xpath is not working");
		}

		String protocolNumber = datatable.getCellData("CtmsData", "Data", 3).trim();
		sendKeys(driver, "PROTOCOLNUMBER", protocolNumber);
		selectDropdown(driver, "THERAPEUTICAREA", datatable.getCellData("CtmsData", "Data", 4).trim());
		sleep(2, TimeUnit.SECONDS);

		click(driver, "LEADINVESTIGATORADDBUTTON");
		String leadInvestigator = datatable.getCellData("CtmsData", "Data", 5);
		log.info("*************** Lead Investigator name is : " + leadInvestigator + "******************* ");
		String[] splits = leadInvestigator.split(" ");
		String lastname = leadInvestigator;
		if (splits.length > 1)
			lastname = splits[1];
		log.info("*************** Lead Investigator is search by Last Name : " + lastname + "******************* ");

		try {
			sendKeys(driver, "LEADINVESTIGATORSEARCHBOX", lastname);
			selectDropdown(driver, "LEADINVESTIGATORDROPDOWN", "Last Name");
			click(driver, "LEADINVESTIGATORSEARCHBUTTON");
			sleep(2, TimeUnit.SECONDS);
			System.out.println(".//*[@class='lvtColData']//a[text()='" + leadInvestigator + "']");
			driver.findElement(By.xpath(".//a[text()='" + leadInvestigator + "']")).click();
		} catch (Exception e) {
			log.info("*************** Lead Investigator " + leadInvestigator
					+ " is not available in Contacts by Searching Last Name******************* ");
			try {
				String firstName = splits[0];
				System.out.println("Lead Investigator First Name: " + firstName);
				CommonMethod.clear(driver, "LEADINVESTIGATORSEARCHBOX");
				sendKeys(driver, "LEADINVESTIGATORSEARCHBOX", firstName);
				selectDropdown(driver, "LEADINVESTIGATORDROPDOWN", "First Name");
				click(driver, "LEADINVESTIGATORSEARCHBUTTON");
				sleep(2, TimeUnit.SECONDS);
				System.out.println(".//*[@class='lvtColData']//a[text()='" + leadInvestigator + "']");
				driver.findElement(By.xpath(".//a[text()='" + leadInvestigator + "']")).click();
			} catch (Exception e2) {
				log.info("*************** Lead Investigator " + leadInvestigator
						+ " is not available in Contacts by Searching First Name******************* ");
			}
		}
		sleep(3, TimeUnit.SECONDS);
		sendKeys(driver, "COMPLETEPROTOCOLTITLE", protocolNumber);
		sendKeys(driver, "PAGEDOWN1", Keys.PAGE_DOWN);
		sendKeys(driver, "PAGEDOWN2", Keys.PAGE_DOWN);
		sendKeys(driver, "PAGEDOWN3", Keys.PAGE_DOWN);
		// sleep(3, TimeUnit.SECONDS);
		try {
			log.info("*************** Entering Billing Information******************* ");
			sleep(4, TimeUnit.SECONDS);
			sendKeys(driver, "IMPORT", datatable.getCellData("CtmsData", "Data", 6));
			sendKeys(driver, "QC", datatable.getCellData("CtmsData", "Data", 7));
			sendKeys(driver, "READ", datatable.getCellData("CtmsData", "Data", 8));
			sendKeys(driver, "STUDYSETUP", datatable.getCellData("CtmsData", "Data", 9));
			sendKeys(driver, "CURRENCYTYPE", datatable.getCellData("CtmsData", "Data", 10));
			click(driver, "IBTLEVELCHECKBOX");
			sendKeys(driver, "CURRENCYTYPE", Keys.PAGE_DOWN);
		} catch (Exception e) {
			log.info("Billing Information is not available" + e.getMessage());
		}
		explicitWait(driver, 10, "SAVEBUTTON");
		try {
			sleep(3, TimeUnit.SECONDS);
			click(driver, "SAVEBUTTON");
		} catch (Exception e) {
			sleep(3, TimeUnit.SECONDS);
			WebElement html = driver.findElement(By.tagName("html"));
			html.sendKeys(Keys.chord(Keys.COMMAND, Keys.SUBTRACT));
			click(driver, "SAVEBUTTON");
			html.sendKeys(Keys.chord(Keys.COMMAND, "0"));
		}
		log.info("*************** " + protocolNumber + " Study is ready to verify******************* ");
		sleep(3, TimeUnit.SECONDS);
		rsvAssert.verifyProtocolNumber();
		log.info("*************** " + protocolNumber
				+ " Study is created and verified successfully******************* ");

	}

	@Test(priority = 3)
	public void editPickListEditor() throws IOException, InterruptedException {
		explicitWait(driver, 5, "SETTINGS");
		click(driver, "SETTINGS");

		click(driver, "PICKLISTEDITOR");
		sleep(2, TimeUnit.SECONDS);
		String protocolNumber = datatable.getCellData("CtmsData", "Data", 3);
		selectDropdown(driver, "PEPROTOCOLNUMBER", protocolNumber);// To select Pick List Editor Protocol Number

		log.info("***************Study Module ******************* ");
		selectDropdown(driver, "SELECTMODULE", datatable.getCellData("PickListEditor", "Data", 2));// Select Study as
																									// Module
		sleep(2, TimeUnit.SECONDS);
		selectDropdown(driver, "SELECTPICKLIST", datatable.getCellData("PickListEditor", "Data", 3));// Select
																										// Studyphase as
																										// Picklist
		sendKeys(driver, "SELECTPICKLIST", Keys.PAGE_DOWN);
		sendKeys(driver, "STUDYDESIGNASSIGNBUTTON", Keys.PAGE_DOWN);

		log.info("***************Assigning available required Modality List  ******************* ");
		sleep(2, TimeUnit.SECONDS);
		explicitWait(driver, 5, "MODALITYASSIGNBUTTON");
		click(driver, "MODALITYASSIGNBUTTON");
		sendKeys(driver, "MODALITYASSIGNBUTTON", Keys.PAGE_DOWN);

		try {
			explicitWait(driver, 3, "SCROLLTOTOP");
			click(driver, "SCROLLTOTOP");
		} catch (Exception e) {
			log.info("*************** Driver Unable to click on Scroll to Top ******************* ");
			sendKeys(driver, "SCROLLTOTOP", Keys.PAGE_UP);
			sendKeys(driver, "MODALITYASSIGNBUTTON", Keys.PAGE_UP);
			sendKeys(driver, "STUDYDESIGNASSIGNBUTTON", Keys.PAGE_UP);
			sendKeys(driver, "SELECTPICKLIST", Keys.PAGE_UP);
		}
		explicitWait(driver, 5, "AVAILABLELIST");

		log.info("***************  To Clear Study module Modality Assigned picklist values  ******************* ");
		/*
		 * int assignedValueSize=-1; while(true){ if(assignedValueSize==-1) { Select
		 * assignedValues= new
		 * Select(driver.findElement(By.xpath("//select[@id='selectedColumns']")));//
		 * assignedValueSize=assignedValues.getOptions().size(); }
		 * System.out.println("Total current number of Options are :"+assignedValueSize)
		 * ; if(assignedValueSize<=0){//Taking extra time need to solve it System.out.
		 * println("*******All Options are cleared from Assigned Pick List*******" );
		 * break; } String currentOption=driver.findElement(By.xpath(
		 * ".//*[@id='selectedColumns']//option[1]")).getText(); sleep(3,
		 * TimeUnit.SECONDS);
		 * System.out.println("Current Option to be clear is :"+currentOption );
		 * driver.findElement(By.xpath(".//*[@id='selectedColumns']//option[1]")).click(
		 * ); click(driver, "LEFTBUTTON"); assignedValueSize--; }
		 */

		sleep(3, TimeUnit.SECONDS);
		sendKeys(driver, "ASSIGNPICKVALUES", Keys.chord(Keys.CONTROL, "a"));
		click(driver, "LEFTBUTTON");
		Reporter.log("Study module Modality Assigned picklist values are cleared successfully");

		log.info("***************  To Select Study module Modality Assigned picklist values  ******************* ");
		int modality = 2;
		while (true) {
			String modalityValue = datatable.getCellData("StudyAssign", "Modality", modality);
			// Reporter.log(modalityValue);
			if (modalityValue == null || modalityValue.equals("")) {
				int totalNumberOfValues = modality - 2;
				sleep(2, TimeUnit.SECONDS);
				click(driver, "AVAILABLELISTSAVE");
				Reporter.log("All " + totalNumberOfValues + " Modality values are selected and saved successfully");
				break;
			}
			sleep(2, TimeUnit.SECONDS);
			driver.findElement(By.xpath("(.//*[@id='availList']//option[text()='"
					+ datatable.getCellData("StudyAssign", "Modality", modality) + "'])[1]")).click();
			click(driver, "RIGHTBUTTON");
			Reporter.log("Modality " + modalityValue + "added to Assigned Picklist values");
			modality++;
		}

		log.info("***************QC Module ******************* ");
		selectDropdown(driver, "SELECTMODULE", datatable.getCellData("PickListEditor", "Data", 5));
		sleep(2, TimeUnit.SECONDS);
		selectDropdown(driver, "SELECTPICKLIST", datatable.getCellData("PickListEditor", "Data", 6));
		// sendKeys(driver, "SELECTPICKLIST", Keys.PAGE_DOWN);
		click(driver, "ICPTNASSIGNBUTTON");
		sleep(3, TimeUnit.SECONDS);

		log.info("***************  To Clear QC Module Assigned ICP Task Name picklist values  ******************* ");
		sleep(3, TimeUnit.SECONDS);
		sendKeys(driver, "ASSIGNPICKVALUES", Keys.chord(Keys.CONTROL, "a"));
		click(driver, "LEFTBUTTON");
		Reporter.log("QC Module Assigned ICP Task Name picklist all values are cleared successfully");

		log.info("***************   To Select QC Module Available picklist values  ******************* ");
		explicitWait(driver, 5, "AVAILABLELIST");
		int taskname = 2;
		while (true) {
			String icpTaskNameValue = datatable.getCellData("StudyAssign", "ICPTaskName", taskname);
			Reporter.log(icpTaskNameValue);
			if (icpTaskNameValue == null || icpTaskNameValue.equals("")) {
				int totalNumberOfValues = taskname - 2;
				sleep(2, TimeUnit.SECONDS);
				click(driver, "AVAILABLELISTSAVE");
				Reporter.log("All " + totalNumberOfValues
						+ " required ICP Task Name all values are selected and saved successfully");
				break;
			}
			sleep(2, TimeUnit.SECONDS);
			driver.findElement(By.xpath("(.//*[@id='availList']//option[text()='"
					+ datatable.getCellData("StudyAssign", "ICPTaskName", taskname) + "'])[1]")).click();
			click(driver, "RIGHTBUTTON");
			Reporter.log("ICP Task Name " + icpTaskNameValue + " added to Available picklist");
			taskname++;
		}

		log.info("***************   To Create QC Module ICP Group Name  ******************* ");
		sleep(2, TimeUnit.SECONDS);
		selectDropdown(driver, "SELECTPICKLIST", datatable.getCellData("PickListEditor", "Data", 7));
		click(driver, "PICKLISTADDBUTTON");

		log.info(
				"***************   Check with Existing Picklist values and enter the value into New entries if not match with the existing value ******************* ");
		int groupname1 = 2;
		while (true) {
			String groupName = datatable.getCellData("StudyAssign", "NewQCGroups", groupname1);
			System.out.println(groupName);
			if (groupName == null || groupName.equals("")) {
				// System.out.println("All ICP Group Names are entered into Text Area and ready
				// to Save");
				String newEntries = CommonMethod.getValue(driver, "NEWENTRYTEXTAREA");
				if (newEntries == null || newEntries.equals("")) {
					click(driver, "AVAILABLELISTCANCEL");
					log.info("All required values are Existed in ICP Groups");
					break;
				} else {
					click(driver, "AVAILABLELISTSAVE");
					log.info("All required values are added into ICP Groups");
					break;
				}

			}
			sleep(2, TimeUnit.SECONDS);
			List<WebElement> picklistValueXpaths = driver
					.findElements(By.xpath("//div[@id='add_availPickList']/div[@class='picklist_existing_options']"));
			int size = picklistValueXpaths.size();// To know the size of all existing Pick list values
			// System.out.println(size+" -----------------------------");
			boolean find = false;
			wl2: for (int i = 1; i <= size; i++) {// To check ICP Task group name with all existing values
				String currentOption = driver.findElement(By.xpath("//div[@id='add_availPickList']/div[" + i + "]"))
						.getText();
				System.out.println(currentOption + "=====================");
				if (groupName.equalsIgnoreCase(currentOption)) {
					log.info(groupName + " is Existed in ICP Groups ");
					find = true;
					break wl2;
				}
				if (currentOption == null || currentOption.equals("")) {// to terminate the loop after completion of all
																		// options
					break wl2;
				}
			}

			if (!find) {// currentOption is not exist in the list then the current value should get
						// enter into the New entries text area
				sendKeys(driver, "NEWENTRYTEXTAREA", datatable.getCellData("StudyAssign", "NewQCGroups", groupname1));
				sendKeys(driver, "NEWENTRYTEXTAREA", Keys.ENTER);
				log.debug(groupName + " is added into ICP Groups ");

			}
			groupname1++;
		}

		log.info("***************   Clicking on Assign button beside the ICP Group Name  ******************* ");
		selectDropdown(driver, "SELECTMODULE", datatable.getCellData("PickListEditor", "Data", 5));
		System.out.println(datatable.getCellData("PickListEditor", "Data", 7));
		click(driver, "ICPGNASSIGNBUTTON");

		log.info("***************  To Clear ICP Group Name Module Assigned picklist values  ******************* ");
		sleep(3, TimeUnit.SECONDS);
		sendKeys(driver, "ASSIGNPICKVALUES", Keys.chord(Keys.CONTROL, "a"));
		click(driver, "LEFTBUTTON");
		Reporter.log("QC Module Assigned ICP Group Name picklist values are cleared successfully");

		log.info("***************   To Select ICP Group Name Module Available picklist values  ******************* ");
		explicitWait(driver, 5, "AVAILABLELIST");
		int groupname2 = 2;
		while (true) {
			String icpGroupNameValue = datatable.getCellData("StudyAssign", "NewQCGroups", groupname2);
			System.out.println(icpGroupNameValue);
			if (icpGroupNameValue == null || icpGroupNameValue.equals("")) {
				int totalNumberOfValues = groupname2 - 2;
				System.out.println("All " + totalNumberOfValues + " required ICP Task Name values are selected");
				sleep(2, TimeUnit.SECONDS);
				click(driver, "AVAILABLELISTSAVE");
				break;
			}
			sleep(2, TimeUnit.SECONDS);
			System.out.println(groupname2);
			driver.findElement(By.xpath(".//*[@id='availList']//option[text()='" + icpGroupNameValue + "']")).click();
			click(driver, "RIGHTBUTTON");
			groupname2++;
		}
	}

	@Test(priority = 4)
	public void addVisitsUnderStudy() throws IOException, InterruptedException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		methods.selectStudyInCtms();
		int visit = 2;
		while (true) {
			String visitNumber = datatable.getCellData("AddVisits", "VisitNumber", visit);
			String visitName = datatable.getCellData("AddVisits", "VisitName", visit);
			String visitPriority = datatable.getCellData("AddVisits", "Priority", visit);
			System.out.println("VisitNumber: " + visitNumber);
			System.out.println("VisitNumber Length: " + visitNumber.length());
			System.out.println(visitName);
			if (visitNumber != null && !visitNumber.trim().isEmpty()) {

				explicitWait(driver, 5, "VISITS");
				CommonMethod.mouseMoveToElement(driver, "VISITS");
				click(driver, "ADDVISITS");
				sleep(2, TimeUnit.SECONDS);
			}

			if (visitNumber == null || visitNumber.equals("")) {
				int totalNumberOfValues = visit - 2;
				log.info("All " + totalNumberOfValues + " Visits are Created successfully");
				sleep(2, TimeUnit.SECONDS);
				break;
			}
			sleep(2, TimeUnit.SECONDS);
			System.out.println(visit);
			// driver.findElement(By.xpath(".//*[@id='availList']//option[text()='"+icpGroupNameValue+"']")).click();
			sendKeys(driver, "VISITNUM", visitNumber);
			sendKeys(driver, "VISITNAME", visitName);
			try {
				selectDropdown(driver, "PRIORITY", visitPriority);
			} catch (Exception e) {
				log.info("No Priority");
			}

			log.info(
					"***************  Adding Modalities to the Visit Number " + visitNumber + "  ******************* ");
			int modality = 2;
			do {
				String modalityValue = datatable.getCellData("StudyAssign", "Modality", modality);
				System.out.println(modalityValue);
				if (modalityValue == null || modalityValue.equals("")) {
					int totalNumberOfValues = modality - 2;
					log.info("All " + totalNumberOfValues + " number of required Modality values are Added to the "
							+ visitNumber + " Visit successfully");
					sleep(2, TimeUnit.SECONDS);
					break;
				}
				sleep(2, TimeUnit.SECONDS);
				try {
					int index = modality - 1;
					click(driver, "MODALITYADDBUTTON");
					System.out.println("//select[@id='modality" + modality + "']");
					Select modality1 = new Select(
							driver.findElement(By.xpath("//select[@id='modality" + modality + "']")));
					System.out.println("Modality Value to be add is :" + modalityValue);
					modality1.selectByIndex(index);
					/*
					 * Select modality2=new
					 * Select(driver.findElement(By.xpath("//select[@id='modality"+modality+"']")));
					 * modality1.selectByValue(modalityValue);
					 */
				} catch (Exception e) {
					Reporter.log("***************  Because of change in xpath value Unable to Adding Modality "
							+ modalityValue + " to the Visit Number " + visitNumber + "  ******************* ");
					// click(driver, "MODALITYADDBUTTON");
				}
				modality++;
			} while (true);
			visit++;
			click(driver, "SAVEBUTTON");
			sleep(3, TimeUnit.SECONDS);
		}

	}

	@Test(priority = 5)
	public void addEmailTemplate() throws IOException, InterruptedException {

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CommonMethod.click(driver, "SETTINGS");
		CommonMethod.click(driver, "EMAILTEMP");

		log.info("**************  Select Requred Email Template CheckBox   ****************** ");
		int templatename = 2;
		br1: while (true) {
			String emailTempName = datatable.getCellData("EMailTemplate", "Template", templatename);
			System.out.println(emailTempName);
			if (emailTempName == null || emailTempName.equals("")) {
				int totalNumberOfValues = templatename - 2;
				log.info("All " + totalNumberOfValues + " required ICP Task Name values are selected");
				Thread.sleep(2000);
				break br1;
			}
			int templateNo = 9;
			while (true) {
				try {
					sleep(2, TimeUnit.SECONDS);
					System.out.println("current Template No is:" + templateNo);
					driver.findElement(
							By.xpath("//b[contains(text(),'" + emailTempName + "')]/../../..//td/*[@type='checkbox']"))
							.click();
					log.info("'" + emailTempName + "' template checked successfully");
					driver.findElement(
							By.xpath("//b[contains(text(),'" + emailTempName + "')]/../../..//td/*[@type='checkbox']"))
							.sendKeys(Keys.HOME);
					break;
				} catch (Exception e) {
					System.out.println(
							emailTempName + " email template is not available under :" + templateNo + " templates");
					String pageDownXpath = "(//td[text()='" + templateNo + "']/..//a)[2]";
					try {
						sleep(2, TimeUnit.SECONDS);
						driver.findElement(By.xpath(pageDownXpath)).sendKeys(Keys.PAGE_DOWN);
						sleep(2, TimeUnit.SECONDS);
						templateNo = templateNo + 13;
					} catch (Exception e1) {
						System.out.println("current template no " + templateNo + " is not available");
						driver.findElement(By.xpath("(//td[text()='64']/..//a)[2]")).sendKeys(Keys.HOME);
						// CommonMethod.click(driver, "SCROLLTOTOP");

					}
				}
			}
			templatename++;
		}
		CommonMethod.click(driver, "ASSIGNTOSTUDY");
		String protocolNumber = datatable.getCellData("CtmsData", "Data", 3);

		log.info("Ready to select Trial " + protocolNumber);
		sleep(3, TimeUnit.SECONDS);
		try {
			driver.findElement(By.xpath("//select[@id='assignId']//option[contains(text(),'" + protocolNumber + "')]"))
					.click();
			log.info("Trial " + protocolNumber + " selected");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			log.info("Trial " + protocolNumber + " not found on this server, Please check the trial Name");

		}
		sleep(2, TimeUnit.SECONDS);
		CommonMethod.click(driver, "ETSAVE");
		log.info(protocolNumber + "trial Email Templates configured successfully");

		sleep(15, TimeUnit.SECONDS);

	}

	@Test(priority = 6)
	public void changeStudySettings() throws Exception {
		log.info("***************  To change study settings   ******************* ");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		explicitWait(driver, 5, "STUDY");
		methods.selectStudyInCtms();
		sleep(2, TimeUnit.SECONDS);
		click(driver, "STUDYSETTINGS");

		clear(driver, "SITEIDLENGTH");
		// sendKeys(driver, "SITEIDLENGTH", Keys.chord(Keys.CONTROL, "a"));
		// sendKeys(driver, "SITEIDLENGTH", Keys.LEFT);
		explicitWait(driver, 40, "SITEIDLENGTH");
		sendKeys(driver, "SITEIDLENGTH", datatable.getCellData("StudySettings", "Value", 2));
		log.info("Site ID lenght is set to " + datatable.getCellData("StudySettings", "Value", 2));
		sleep(2, TimeUnit.SECONDS);
		if (datatable.getCellData("StudySettings", "Type", 2).equalsIgnoreCase("Numeric")) {
			click(driver, "SITEIDNRBUTTON");
			log.info("Numeric radio button is selected for Site ID Type");
		} else if (datatable.getCellData("StudySettings", "Type", 2).equalsIgnoreCase("Alpha Numeric")) {
			click(driver, "SITEIDANRBUTTON");
			log.info("Alpha Numeric radio button is selected for Site ID Type");
		} else {
			log.info("Please enter the valid Site ID Type");
		}
		sendKeys(driver, "SITEIDLENGTH", Keys.PAGE_DOWN);
		// CommonMethod.clear(driver, "SUBJECTIDLENGTH");

		clear(driver, "SUBJECTIDLENGTH");
		sendKeys(driver, "SUBJECTIDLENGTH", datatable.getCellData("StudySettings", "Value", 3));
		log.info("Subject ID lenght is set to " + datatable.getCellData("StudySettings", "Value", 3));
		sleep(3, TimeUnit.SECONDS);
		if (datatable.getCellData("StudySettings", "Type", 3).equalsIgnoreCase("Numeric")) {
			click(driver, "SUBJECTIDNRBUTTON");
			log.info("Numeric radio button is selected for Subject ID Type");
		} else if (datatable.getCellData("StudySettings", "Type", 3).equalsIgnoreCase("Alpha Numeric")) {
			click(driver, "SUBJECTIDANRBUTTON");
			log.info("Alpha Numeric radio button is selected for Subject ID Type");
		} else {
			log.info("Please enter the valid Subject ID Type");
		}

		sleep(5, TimeUnit.SECONDS);
		sendKeys(driver, "SITEIDNRBUTTON", Keys.END);
		sleep(2, TimeUnit.SECONDS);
		click(driver, "SETTINGSSAVE");
		sleep(2, TimeUnit.SECONDS);
		sendKeys(driver, "REASONTEXT", datatable.getCellData("StudySettings", "ResonForChange", 2));
		click(driver, "REASONOKBTN");
		sleep(3, TimeUnit.SECONDS);
		log.info(CommonMethod.handleAlertMessage(driver));
	}

	@Test(priority = 7)
	public void taskSettings() throws IOException, InterruptedException {
		log.info("***************  To make Task Settings  ******************* ");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		methods.selectStudyInCtms();
		CommonMethod.mouseMoveToElement(driver, "MORE");
		click(driver, "TASKSETTING");

		int taskSetting = 2;
		while (true) {
			String taskSettingName = datatable.getCellData("TaskSettings", "MessageTitle", taskSetting);
			System.out.println(taskSettingName + "************");
			if (taskSettingName.isEmpty() || taskSettingName == null) {
				int totalNumberOfValues = taskSetting - 2;
				log.info("All " + totalNumberOfValues + " required ICP Task Name values are added");
				sleep(2, TimeUnit.SECONDS);
				break;
			}
			click(driver, "CONFIGTASKADDICON");
			sleep(3, TimeUnit.SECONDS);
			selectDropdown(driver, "MESSAGETITLE", datatable.getCellData("TaskSettings", "MessageTitle", taskSetting));
			selectDropdown(driver, "TASKNAME", datatable.getCellData("TaskSettings", "TaskName", taskSetting));
			selectDropdown(driver, "GROUPNAME", datatable.getCellData("TaskSettings", "GroupName", taskSetting));
			selectDropdown(driver, "MESSAGETYPE", datatable.getCellData("TaskSettings", "MessageType", taskSetting));
			selectDropdown(driver, "TSEMAILTEMP", datatable.getCellData("TaskSettings", "EmailTemplate", taskSetting));
			sleep(2, TimeUnit.SECONDS);
			click(driver, "ACTIONS");
			sleep(3, TimeUnit.SECONDS);
			CommonMethod.handleAlert(driver);
			Reporter.log("Task Setting Message Type is: " + taskSettingName + " done successfully");
			taskSetting++;
		}

	}

	@Test(priority = 8)
	public void createSite() throws IOException, InterruptedException, AWTException {
		log.info("*************** Creating Sites ******************* ");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		methods.selectStudyInCtms();

		/*
		 * CommonMethod.robotKeyPress(driver, KeyEvent.VK_CONTROL);
		 * CommonMethod.robotKeyPress(driver, KeyEvent.VK_MINUS);
		 * CommonMethod.robotKeyRelease(driver, KeyEvent.VK_CONTROL);
		 */

		int site = 2;
		while (true) {
			String status = datatable.getCellData("CreateSite", "Status", site).trim();
			String siteID = datatable.getCellData("CreateSite", "SiteID", site).trim();// getting double value, In Excel
																						// sheet cell format type
																						// changed to Text
			int totalNumberOfValues = site - 2;
			if (siteID == null || siteID.equals("")) {// Site Name Need to change to SiteID
				// int totalNumberOfValues=site-2;
				System.out.println("All " + totalNumberOfValues + " SiteIDs are Created successfully");
				sleep(2, TimeUnit.SECONDS);
				break;
			}
			if ("Created".equalsIgnoreCase(status)) {// Site Name Need to change to SiteID
				System.out.println("skipped " + siteID + " SiteID is exists");
				site++;
				continue;
			}
			CommonMethod.mouseMoveToElement(driver, "SITE");
			// sleep(1, TimeUnit.SECONDS);
			// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			click(driver, "ADDSITE");

			System.out.println("Site ID is: " + siteID);
			String siteName = datatable.getCellData("CreateSite", "SiteName", site).trim();
			// String siteName1="_"+siteID+" "+siteName;
			System.out.println(siteName);
			sendKeys(driver, "SITENAME", siteName);
			sendKeys(driver, "SITEID", datatable.getCellData("CreateSite", "SiteID", site));
			try {
				selectDropdown(driver, "COUNTRY", datatable.getCellData("CreateSite", "Country", site).trim());
				selectDropdown(driver, "EMAILTYPE", datatable.getCellData("CreateSite", "EmailType", site).trim());
				sendKeys(driver, "EMAILID", datatable.getCellData("CreateSite", "EmailID", site));
				selectDropdown(driver, "PHONETYPE", datatable.getCellData("CreateSite", "PhoneType", site));
				sendKeys(driver, "PHONENUMBER", datatable.getCellData("CreateSite", "PhoneNumber", site));
			} catch (Exception e) {
				log.info(totalNumberOfValues + "). Site ID is: " + siteID + " is not created");
				datatable.setCellData("CreateSite", "Status", site, "Not Created");

			}

			sleep(1, TimeUnit.SECONDS);
			sendKeys(driver, "PHONENUMBER", Keys.PAGE_DOWN);
			sleep(2, TimeUnit.SECONDS);
			click(driver, "SITESAVEBUTTON");
			// Reporter.log("Site ID is: " +siteID+" created Successfully");
			sleep(2, TimeUnit.SECONDS);
			if (CommonMethod.isAlertPresent(driver)) {
				CommonMethod.handleAlert(driver);
				log.info(totalNumberOfValues + "). Site ID is: " + siteID + " already exist");
				click(driver, "SITECANCELBUTTON");
				sleep(2, TimeUnit.SECONDS);
				datatable.setCellData("CreateSite", "Status", site, "Already Exist");
			} else {
				RsvAssertions.verifyCreateSite(siteID);
				log.info(totalNumberOfValues + ").Site ID is: " + siteID + " created Successfully with Site Name :"
						+ siteName);
				datatable.setCellData("CreateSite", "Status", site, "Created");
			}
			site++;
		}
	}

	@Test(priority = 9)
	public void assignContact() throws IOException, InterruptedException {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		click(driver, "CONTACT");

		sleep(3, TimeUnit.SECONDS);
		String assignContact = datatable.getCellData("Contacts", "AssignContacts", 2);
		String[] splits = assignContact.split(" ");
		String firstName = splits[0];
		String lastName = splits[1];

		sendKeys(driver, "SEARCHTXTBOX", firstName);
		// sendKeys(driver, "SEARCHTXTBOX", lastName);
		selectDropdown(driver, "SRCHDROPDOWN", "First Name");
		click(driver, "SRCHNOWBUTTON");
		sleep(3, TimeUnit.SECONDS);
		String pageCountstring = CommonMethod.getText(driver, "PAGECOUNT");
		String[] splitString = pageCountstring.split(" ");
		String pageCount1 = splitString[1];
		int pageCount = Integer.parseInt(pageCount1);
		System.out.println("No of pages are :" + pageCount);
		outer: for (int j = 1; j <= pageCount; j++) {
			sleep(3, TimeUnit.SECONDS);
			try {
				String MATCHEDCONTACTS = "//a[text()='" + lastName + "']/../..//input[@type='checkbox']";
				int noOfMatchedContacts = driver.findElements(By.xpath(MATCHEDCONTACTS)).size();
				System.out.println(noOfMatchedContacts + " Matched contacts ***************************");
				for (int i = 0; i < noOfMatchedContacts; i++) {
					System.out.println(i + "..............................,");
					List<WebElement> matchedLastElement = driver
							.findElements(By.xpath("//a[text()='" + lastName + "']"));
					List<WebElement> matchedFirstElement = driver
							.findElements(By.xpath("//a[text()='" + firstName + "']"));
					String matchedFirstContact = matchedFirstElement.get(i).getText();
					String matchedLastContact = matchedLastElement.get(i).getText();
					System.out.println("Matched First Name " + matchedFirstContact + "-------First name " + firstName);
					System.out.println("Matched Last Name " + matchedLastContact + "-------Last name " + lastName);
					sleep(2, TimeUnit.SECONDS);
					if (matchedLastContact.equalsIgnoreCase(lastName)
							|| matchedFirstContact.equalsIgnoreCase(firstName)) {
						sleep(2, TimeUnit.SECONDS);
						int index = i + 1;
						driver.findElement(By
								.xpath("//a[text()='" + lastName + "']/../..//input[@type='checkbox'][" + index + "]"))
								.click();

						// get the user name and compare with excel User name
						break outer;
					}
				}

			} catch (Exception e) {
				log.info("Matched Contacts are not having in the Page Number :" + j);

			}
			try {
				click(driver, "NEXTPAGE");
				log.info("Driver clicked on Next button");
			} catch (Exception e1) {
				log.info("Next button is disabled due to Page not availability");
			}
		}
	}

	@Test(priority = 10)
	public void addInDistributionList() throws IOException, InterruptedException {

		methods.selectStudyInCtms();
		click(driver, "DISTRIBUTIONlIST");
		click(driver, "ADVANCEDDISTRIBUTIONlIST");
		driver.findElement(By.xpath("//td[contains(text(),'" + datatable.getCellData("Contacts", "ContactName", 2)
				+ "')]/..//input[@type='checkbox']")).click();
		driver.findElement(By.xpath("//td[contains(text(),'" + datatable.getCellData("DistributionList", "Sites", 2)
				+ "')]/..//input[@type='checkbox']")).click();
		driver.findElement(By.xpath("//td[contains(text(),'"
				+ datatable.getCellData("DistributionList", "NotificationType", 2) + "')]/..//input[@type='checkbox']"))
				.click();
		sleep(5, TimeUnit.SECONDS);
		// click(driver, "DISTRIBUTIONlISTSAVE");

	}

	@Test(priority = 11)
	public void editVisitsUnderStudy() throws IOException, InterruptedException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		methods.selectStudyInCtms();
		int visit = 2;
		while (true) {
			String visitNumber = datatable.getCellData("AddVisits", "VisitNumber", visit).trim();
			String visitName = datatable.getCellData("AddVisits", "VisitName", visit).trim();
			String newVisitName = datatable.getCellData("AddVisits", "NewVisitName", visit).trim();
			String visitPriority = datatable.getCellData("AddVisits", "Priority", visit);
			System.out.println("VisitNumber: " + visitNumber);
			System.out.println("VisitNumber Length: " + visitNumber.length());
			System.out.println(visitName);

			if (visitNumber == null || visitNumber.equals("")) {
				int totalNumberOfValues = visit - 2;
				log.info("All " + totalNumberOfValues + " Visits are Created successfully");
				sleep(2, TimeUnit.SECONDS);
				break;
			}

			if (visitNumber != null && !visitNumber.isEmpty()) {
				System.out.println(""+visitName != null && newVisitName != null &&!visitName.isEmpty() && !visitName.equals(newVisitName));
				if (visitName != null && newVisitName != null &&!visitName.isEmpty() && !visitName.equals(newVisitName) ) {
					System.out.println("Enterd...........");
					log.info("Visit Name " + visitName + " different from New Visit Name " + newVisitName);
					explicitWait(driver, 5, "VISITS");
					if (visit==2) {
						CommonMethod.mouseMoveToElement(driver, "VISITS");
						sleep(2, TimeUnit.SECONDS);
						click(driver, "VISITSLIST");
						sleep(3, TimeUnit.SECONDS);
					}
					sendKeys(driver, "SEARCHTXTBOX", visitName);
					sleep(2, TimeUnit.SECONDS);
					click(driver, "VISITSEARCHNOWBTN");
					sleep(3, TimeUnit.SECONDS);
					// To click on Edit button with respective to Visit Name
					String editXpath="(//tr/td//a[text()='"+visitName+"']/../..//a[contains(text(),'edit')])";
					System.out.println("Xpath is "+editXpath);
					driver.findElement(By.xpath(editXpath)).click();
					sleep(2, TimeUnit.SECONDS);
					clear(driver, "VISITNAME");
					sendKeys(driver, "VISITNAME", newVisitName);
					sendKeys(driver, "VISITNAME", Keys.PAGE_DOWN);
					sleep(2, TimeUnit.SECONDS);
					click(driver, "SAVEBUTTON");
					sleep(3, TimeUnit.SECONDS);
					sendKeys(driver, "REASONTEXT", newVisitName);
					click(driver, "REASONOKBTN");
					log.info(visitNumber+ ". Old Visit Name " + visitName + " changed to New Visit Name " + newVisitName + " Successfully");
					sleep(2, TimeUnit.SECONDS);
					visit++;
					
				} else {
					explicitWait(driver, 5, "VISITS");
					CommonMethod.mouseMoveToElement(driver, "VISITS");
					click(driver, "ADDVISITS");
					sleep(2, TimeUnit.SECONDS);
					System.out.println(visit);
					// driver.findElement(By.xpath(".//*[@id='availList']//option[text()='"+icpGroupNameValue+"']")).click();
					sendKeys(driver, "VISITNUM", visitNumber);
					sendKeys(driver, "VISITNAME", newVisitName);
					try {
						selectDropdown(driver, "PRIORITY", visitPriority);
					} catch (Exception e) {
						log.info("No Priority");
					}

					log.info("***************  Adding Modalities to the Visit Number " + visitNumber
							+ "  ******************* ");
					int modality = 2;
					do {
						String modalityValue = datatable.getCellData("StudyAssign", "Modality", modality);
						System.out.println(modalityValue);
						if (modalityValue == null || modalityValue.equals("")) {
							int totalNumberOfValues = modality - 2;
							log.info("All " + totalNumberOfValues
									+ " number of required Modality values are Added to the " + visitNumber
									+ " Visit successfully");
							sleep(2, TimeUnit.SECONDS);
							break;
						}
						sleep(2, TimeUnit.SECONDS);
						try {
							int index = modality - 1;
							click(driver, "MODALITYADDBUTTON");
							System.out.println("//select[@id='modality" + modality + "']");
							Select modality1 = new Select(
									driver.findElement(By.xpath("//select[@id='modality" + modality + "']")));
							System.out.println("Modality Value to be add is :" + modalityValue);
							modality1.selectByIndex(index);
							/*
							 * Select modality2=new
							 * Select(driver.findElement(By.xpath("//select[@id='modality"+modality+"']")));
							 * modality1.selectByValue(modalityValue);
							 */
						} catch (Exception e) {
							Reporter.log("***************  Because of change in xpath value Unable to Adding Modality "
									+ modalityValue + " to the Visit Number " + visitNumber + "  ******************* ");
							// click(driver, "MODALITYADDBUTTON");
						}
						modality++;

					} while (true);
					visit++;
					click(driver, "SAVEBUTTON");
					sleep(3, TimeUnit.SECONDS);

				}
			}
			sleep(2, TimeUnit.SECONDS);

		}

	}

}

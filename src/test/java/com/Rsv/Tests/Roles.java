package com.Rsv.Tests;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.Rsv.ReUsableMethods.RsvMethods;
import com.framework.driver.CommonMethod;
import com.framework.driver.TestBase;
import static com.framework.driver.CommonMethod.sleep;
import static com.framework.driver.CommonMethod.click;
import static com.framework.driver.CommonMethod.sendKeys;
import static com.framework.driver.CommonMethod.selectDropdown;
import static com.framework.driver.CommonMethod.explicitWait;
import static com.framework.driver.CommonMethod.getText;

@Listeners(com.framework.utilities.TestNGListners.class)
public class Roles extends TestBase{
	RsvMethods methods=new RsvMethods();
	
	@Test(priority=1)
	public void logIn() throws IOException, InterruptedException {
		methods.logIn();		
		
	}
	
	@Test(priority=2)
	public void createRole() throws IOException, InterruptedException 
	{
		CommonMethod.explicitWait(driver, 3, "ADDNEWROLE");		
		click(driver, "ADDNEWROLE");
		sleep(2, TimeUnit.SECONDS);
		int i=2;
		while(true){//To get all Role types(Ex; SiteUser, QCUser,...) from Excel sheet  
			String roleType=datatable.getCellData("Roles", "RoleType", i);
			if (roleType.trim().isEmpty()||roleType==null) {
				System.out.println("Role Type loop gets break");
				break;
			}
			String[] splits=roleType.split(" ");
			String ddroletype=splits[0];
			
			System.out.println(roleType+"************");
			System.out.println(ddroletype+"+++++++++++");
			if (roleType!="")
			{
				if(roleType.equalsIgnoreCase("SiteUser"))//To check all permissions for Site User
				{
					String rolename=datatable.getCellData("Roles", "CreateRole", i);
					if (!rolename.isEmpty())
					{
						sendKeys(driver, "CREATEROLE", rolename);
						selectDropdown(driver, "ROLETYPEDD", roleType);
						sleep(2, TimeUnit.SECONDS);
						click(driver, "SELECTALLCHECKBOX");//To check all check boxes
						sleep(2, TimeUnit.SECONDS);
						click(driver, "SELECTALLCHECKBOX");//To uncheck all check boxes
						
						int j=2;
						while (true)//Get all RequiredRsvfw permissions from Excel sheet 
						{
							String rsvfwPermission=datatable.getCellData("SiteRoles", "RequiredRsvfw", j);
							if (rsvfwPermission.trim().isEmpty()||rsvfwPermission==null)
							{
								System.out.println("RequiredRsvfw Permission cell value is empty or All permissions checked Successfully");
								break;
							}
							//To get all List items of Rsvfw permissions from UI
							List<WebElement> list=driver.findElements(By.xpath("//ul[@class='row ng-scope']//li"));//to get list of RSVFW permissions
							int size=list.size();
							System.out.println("Total number of RSVFW Permissions are "+size);
							for (int k = 0; k < size; k++) //To check for each List item
							{
								String uiPermissionName=list.get(k).getText();
								if (uiPermissionName.equals(rsvfwPermission))//To click on Matched and Required RSVFW permission check box 
								{
									//To get all check boxes of Rsvfw permissions List
									List<WebElement> rsvfwcheckbox=driver.findElements(By.xpath("//ul[@class='row ng-scope']//li/input"));//to get RSFVW list check box
									sleep(2, TimeUnit.SECONDS);
									rsvfwcheckbox.get(k).click();
									System.out.println(uiPermissionName+" is selected");
								}	
								else
								{
									System.out.println(uiPermissionName+" ****** is not matched with ****** "+rsvfwPermission);
									
								}
							}
							j++;
						}

						//To Set CTMS permissions
						click(driver, "SHOWADVANCESETTINGS");
						sleep(2, TimeUnit.SECONDS);
						sendKeys(driver, "SHOWADVANCESETTINGS", Keys.PAGE_DOWN);
						
						int l=2;
						while (true)//Get all RequiredCtms permissions from Excel sheet 
						{
							String ctmsPermission=datatable.getCellData("SiteRoles", "AllCtmsPermissions", l);
							if (ctmsPermission.trim().isEmpty()||ctmsPermission==null)
							{
								System.out.println("Requiredctms Permission cell value is empty or All permissions checked Successfully");
								break;
							}
							
							String moduleToBeShown=datatable.getCellData("SiteRoles", "ModuleToBeShown", l);
							String create=datatable.getCellData("SiteRoles", "Create", l);
							String view=datatable.getCellData("SiteRoles", "View", l);
							String delete=datatable.getCellData("SiteRoles", "Delete", l);
							System.out.println("** "+ctmsPermission+" ** "+moduleToBeShown+" ** "+create+" ** "+delete+" **");
							try
							{
								if (moduleToBeShown.equalsIgnoreCase("Yes"))
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement moduleToBeShownchkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[1]"));//To click ModuleToBeShown check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!moduleToBeShownchkbox.isSelected()+" >>>>>>>>>>>");
									if (!moduleToBeShownchkbox.isSelected())
									{
										moduleToBeShownchkbox.click();
										System.out.println(ctmsPermission+" Module to be shown check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" Module to be shown check box already checked");

									}
									
								} 
								else if (create.equalsIgnoreCase("Yes"))
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement createchkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[2]"));//To click Create/Edit check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!createchkbox.isSelected()+" >>>>>>>>>>>");
									if (!createchkbox.isSelected())
									{
										createchkbox.click();
										System.out.println(ctmsPermission+" Create/Edit check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" Create/Edit check box already checked");

									}
								}
								else if (view.equalsIgnoreCase("Yes"))
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement viewchkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[3]"));//To click View check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!viewchkbox.isSelected()+" >>>>>>>>>>>");
									if (!viewchkbox.isSelected())
									{
										viewchkbox.click();
										System.out.println(ctmsPermission+" View check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" View check box already checked");

									}

								}
								else if (delete.equalsIgnoreCase("Yes")) 
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement deletechkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[4]"));//To click ModuleToBeShown check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!deletechkbox.isSelected()+" >>>>>>>>>>>");
									if (!deletechkbox.isSelected())
									{
										deletechkbox.click();
										System.out.println(ctmsPermission+" Delete check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" Delete check box already checked");

									}
								} else
								{
									System.out.println("No permissions is given to "+ctmsPermission);
									if (ctmsPermission.equalsIgnoreCase("Study Team Log") || ctmsPermission.equalsIgnoreCase("RSV_Reports"))
									{
										driver.findElement(By.xpath("")).sendKeys(Keys.PAGE_DOWN);  
										sleep(2, TimeUnit.SECONDS);
																			
									}

								} 
								
							} catch (Exception e)
							{
								System.out.println("Exception is  "+e);
							}
							
							l++;
						}
						sleep(2, TimeUnit.SECONDS);
						click(driver, "ROLESAVE");
						CommonMethod.explicitWait(driver, 30, "ROLEMESSAGE");
						String message=CommonMethod.getText(driver, "ROLEMESSAGE"); 
						System.out.println(message+".........");
					}
					else if(roleType.equalsIgnoreCase("QCUser 1"))
					{
						
					}
				}
				else if(roleType.equalsIgnoreCase("QCUser 1"))//To check all permissions for Site User
				{
					String rolename=datatable.getCellData("Roles", "CreateRole", 2);
					if (!rolename.isEmpty())
					{
						sendKeys(driver, "CREATEROLE", rolename);
						selectDropdown(driver, "ROLETYPEDD", roleType);
						sleep(2, TimeUnit.SECONDS);
						click(driver, "SELECTALLCHECKBOX");//To check all check boxes
						sleep(2, TimeUnit.SECONDS);
						click(driver, "SELECTALLCHECKBOX");//To uncheck all check boxes
						
						int j=2;
						while (true)//Get all RequiredRsvfw permissions from Excel sheet 
						{
							String rsvfwPermission=datatable.getCellData("SiteRoles", "RequiredRsvfw", j);
							if (rsvfwPermission.trim().isEmpty()||rsvfwPermission==null)
							{
								System.out.println("RequiredRsvfw Permission cell value is empty or All permissions checked Successfully");
								break;
							}
							//To get all List items of Rsvfw permissions from UI
							List<WebElement> list=driver.findElements(By.xpath("//ul[@class='row ng-scope']//li"));//to get list of RSVFW permissions
							int size=list.size();
							System.out.println("Total number of RSVFW Permissions are "+size);
							for (int k = 0; k < size; k++) //To check for each List item
							{
								String uiPermissionName=list.get(k).getText();
								if (uiPermissionName.equals(rsvfwPermission))//To click on Matched and Required RSVFW permission check box 
								{
									//To get all check boxes of Rsvfw permissions List
									List<WebElement> rsvfwcheckbox=driver.findElements(By.xpath("//ul[@class='row ng-scope']//li/input"));//to get RSFVW list check box
									sleep(2, TimeUnit.SECONDS);
									rsvfwcheckbox.get(k).click();
									System.out.println(uiPermissionName+" is selected");
								}	
								else
								{
									System.out.println(uiPermissionName+" ****** is not matched with ****** "+rsvfwPermission);
									
								}
							}
							j++;
						}

						//To Set CTMS permissions
						click(driver, "SHOWADVANCESETTINGS");
						sleep(2, TimeUnit.SECONDS);
						sendKeys(driver, "SHOWADVANCESETTINGS", Keys.PAGE_DOWN);
						
						int l=2;
						while (true)//Get all RequiredCtms permissions from Excel sheet 
						{
							String ctmsPermission=datatable.getCellData("SiteRoles", "AllCtmsPermissions", l);
							if (ctmsPermission.trim().isEmpty()||ctmsPermission==null)
							{
								System.out.println("Requiredctms Permission cell value is empty or All permissions checked Successfully");
								break;
							}
							
							String moduleToBeShown=datatable.getCellData("SiteRoles", "ModuleToBeShown", l);
							String create=datatable.getCellData("SiteRoles", "Create", l);
							String view=datatable.getCellData("SiteRoles", "View", l);
							String delete=datatable.getCellData("SiteRoles", "Delete", l);
							System.out.println("** "+ctmsPermission+" ** "+moduleToBeShown+" ** "+create+" ** "+delete+" **");
							try
							{
								if (moduleToBeShown.equalsIgnoreCase("Yes"))
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement moduleToBeShownchkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[1]"));//To click ModuleToBeShown check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!moduleToBeShownchkbox.isSelected()+" >>>>>>>>>>>");
									if (!moduleToBeShownchkbox.isSelected())
									{
										moduleToBeShownchkbox.click();
										System.out.println(ctmsPermission+" Module to be shown check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" Module to be shown check box already checked");

									}
									
								} 
								else if (create.equalsIgnoreCase("Yes"))
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement createchkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[2]"));//To click Create/Edit check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!createchkbox.isSelected()+" >>>>>>>>>>>");
									if (!createchkbox.isSelected())
									{
										createchkbox.click();
										System.out.println(ctmsPermission+" Create/Edit check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" Create/Edit check box already checked");

									}
								}
								else if (view.equalsIgnoreCase("Yes"))
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement viewchkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[3]"));//To click View check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!viewchkbox.isSelected()+" >>>>>>>>>>>");
									if (!viewchkbox.isSelected())
									{
										viewchkbox.click();
										System.out.println(ctmsPermission+" View check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" View check box already checked");

									}

								}
								else if (delete.equalsIgnoreCase("Yes")) 
								{
									sleep(2, TimeUnit.SECONDS);
									WebElement deletechkbox=driver.findElement(By.xpath("(//p[text()='"+ctmsPermission+"']/../../td/div/input)[4]"));//To click ModuleToBeShown check box
									sleep(2, TimeUnit.SECONDS);
									System.out.println(!deletechkbox.isSelected()+" >>>>>>>>>>>");
									if (!deletechkbox.isSelected())
									{
										deletechkbox.click();
										System.out.println(ctmsPermission+" Delete check box checked");
										
									} 
									else
									{
										System.out.println(ctmsPermission+" Delete check box already checked");

									}
								} else
								{
									System.out.println("No permissions is given to "+ctmsPermission);
									if (ctmsPermission.equalsIgnoreCase("Study Team Log") || ctmsPermission.equalsIgnoreCase("RSV_Reports"))
									{
										driver.findElement(By.xpath("")).sendKeys(Keys.PAGE_DOWN);  
										sleep(2, TimeUnit.SECONDS);
																			
									}

								} 
								
							} catch (Exception e)
							{
								System.out.println("Exception is  "+e);
							}
							
							l++;
						}
						sleep(2, TimeUnit.SECONDS);
						click(driver, "ROLESAVE");
						CommonMethod.explicitWait(driver, 30, "ROLEMESSAGE");
						String message=CommonMethod.getText(driver, "ROLEMESSAGE"); 
						System.out.println(message+".........");
					}
					else if(roleType.equalsIgnoreCase("QCUser 1"))
					{
						
					}
				}
			}
			i++;
		}
	}	

	@Test(priority=3)
	public void addRolesToUser() throws IOException, InterruptedException {
	
		explicitWait(driver, 2, "ADDROLETOUSER");
		click(driver, "ADDROLETOUSER");
		explicitWait(driver, 3, "CREATENEWUSER");
		click(driver, "CREATENEWUSER");
		
		int newUser=2;
		while(true)
		{
			String userName=datatable.getCellData("CreateNewUser", "UserName", newUser);
			if (userName==null||userName.equals("")) 
			{
				newUser=newUser-2;
				System.out.println("All "+newUser+" User names created successfully");
				break;
			}
			sleep(2, TimeUnit.SECONDS);
			String selectUserInOpenClinica=datatable.getCellData("CreateNewUser", "UserInOpenClinica",newUser);//getting the Create User In OpenClinica from excel
			if("Yes".equalsIgnoreCase(selectUserInOpenClinica))
			{
				click(driver, "CREATEUSERINOCYES");
				System.out.println("Yes");
				CommonMethod.selectDropdown(driver, "ACTIVESTUDY", datatable.getCellData("CtmsData", "Data", 3));
			}
			else
			{
				 click(driver, "CREATEUSERINOCNO");
			}
			//CommonMethod.selectDropdown(driver, "ACTIVESTUDY", datatable.getCellData("CtmsData", "Data", 3));
							
			sendKeys(driver, "CREATEUSERNAME", userName);
			sendKeys(driver, "EMAIL", datatable.getCellData("Roles", "Email",newUser));
			sendKeys(driver, "FIRSTNAME", datatable.getCellData("Roles", "FirstName",newUser));
			sendKeys(driver, "LASTNAME", datatable.getCellData("Roles", "LastName",newUser));
			sendKeys(driver, "CREATEUSERPASSWORD", datatable.getCellData("Roles", "Password",newUser));
			sendKeys(driver, "REENTERPASSWORD", datatable.getCellData("Roles", "Password",newUser));
			sendKeys(driver, "MOBILENO", datatable.getCellData("Roles", "MobileNo",newUser));
			String selectIsAdmin=datatable.getCellData("Roles", "IsAdmin",newUser);
			if("Yes".equalsIgnoreCase(selectIsAdmin))
			{
				click(driver, "ISADMINYES");
				System.out.println("Yes");
			}
			else 
			{
				 click(driver, "ISADMINNO");
			}
			sleep(2, TimeUnit.SECONDS);
			click(driver, "CREATEUSERCANCEL");
			//click(driver, "CREATEUSERSUBMIT");
			newUser++;	
		}//while loop end
	
	}
	
	@Test(priority=4)
	public void createNewUser() throws IOException, InterruptedException
	{
		
		
	}
	
	@Test(priority=5)
	public void assignUser() throws IOException, InterruptedException
	{
		
		explicitWait(driver, 2, "ADDROLETOUSER");
		click(driver, "ADDROLETOUSER");
		click(driver, "ASSIGNUSER");
		sleep(2, TimeUnit.SECONDS);
		String userName = datatable.getCellData("Roles", "UserName", 2);
		
		System.out.println(userName);
	
		//Selection of Users from the Select User dropdown
		List<WebElement>listUsers=driver.findElements(By.xpath("//select[@class='ng-valid ng-touched ng-not-empty ng-dirty ng-valid-parse']/..//option[4]"));
		System.out.println("Size of list is" +listUsers.size());
		for(int i=0;i<listUsers.size();i++)
		{
			String uilistUsers = listUsers.get(i).getText();
			System.out.println(uilistUsers);
			if(uilistUsers.equals(userName))
			{
				listUsers.get(i).click();
				System.out.println(uilistUsers + " is selected");
			}
			/*else
			{
				//System.out.println("Please create the User and assign");
			}*/
			
		}
		
		sleep(2, TimeUnit.SECONDS);
		//Selection of Trial from the Select Trial dropdown
		String userTrial = datatable.getCellData("CtmsData", "Data", 3);
		List<WebElement>listTrial=driver.findElements(By.xpath("//select[@ng-model='selectedTrail']/..//option"));
		System.out.println("Size of list" + listTrial.size());
		
		for(int i=0;i<listTrial.size();i++)
		{
			String uilistTrial = listTrial.get(i).getText();
			if(uilistTrial.equals(userTrial))
			{
				listTrial.get(i).click();
				System.out.println(uilistTrial + " is selected");
			}else
			{
				//System.out.println("Please create the Trial");
			}
		}
		//Selection of Trial from the Select Roles multiselect dropdown
		click(driver, "SELECTROLE");
		int j=2; 
		w1:while(true) 
		{ 
			String role=datatable.getCellData("Roles", "CreateRole", j); 
			System.out.println("Role name is "+role); 	
			sleep(2, TimeUnit.SECONDS);	
			if (role==null||role.equals("")) 	
			{ 	
				int totalRoles=j-2; 	
				sleep(2, TimeUnit.SECONDS); 
				//click(driver, "ROLESSUBMITBUTTON"); 
				System.out.println("All "+totalRoles+" Roles are selected successfully"); 	
				break w1; 	
			} 			
			List<WebElement> list=driver.findElements(By.xpath("//dropdown-multiselectrole[@class='ng-isolate-scope']//li")); 
			System.out.println("Size of list is "+list.size()); 	
			for (int i = 0; i < list.size(); i++)  		
			{ 	
				String uiRoleName=list.get(i).getText(); 
				System.out.println(uiRoleName);
				/*if(j==2) 	
			    { 		
					if (i==1||uiRoleName.equals("Uncheck All")) 
					{ 		
						list.get(i).click(); 	
						sleep(2, TimeUnit.SECONDS);	
						CommonMethod.handleAlert(driver); 	
					} 					
				} */			
			   if (uiRoleName.equals(role)) 		
			    { 		
				list.get(i).click(); 		
				System.out.println(uiRoleName+" is selected"); 		
			    }	 		
			} 	
			j++; 
		}
		
		click(driver, "SELECTROLE");
		
		//Selection of Sites from the multiselect dropdown
		CommonMethod.explicitWait(driver, 5, "SELECTSITE"); 	
		click(driver, "SELECTSITE"); 
		sleep(2, TimeUnit.SECONDS);	
		int k=2; 
		w1:while(true) 
		{ 	
			String siteName=datatable.getCellData("Roles", "SiteID", k); 	
			System.out.println("Site name is "+siteName); 		
			sleep(2, TimeUnit.SECONDS);		
			if (siteName==null||siteName.equals("")) 	
			{ 		
				int totalsites=j-2; 	
				sleep(2, TimeUnit.SECONDS);	
				//click(driver, "ASSIGNSUBMITBUTTON"); 	
				System.out.println("All "+totalsites+" Sites are selected successfully"); 
				break w1; 	
				} 	
			List<WebElement> list=driver.findElements(By.xpath("//div[@class='btn-group open']//ul[@class='dropdown-menu scrollable-menu ng-scope']/..//li")); 	
			System.out.println("Size of list is "+list.size()); 
			for (int i = 0; i < list.size(); i++) 
			{ 
				String uiSiteName=list.get(i).getText(); 
				if(k==2) 	
				{ 	
					if (i==1||uiSiteName.equals("Uncheck All")) 
					{ 	
						list.get(i).click(); 	
						sleep(2, TimeUnit.SECONDS);
						CommonMethod.handleAlert(driver); 	
					} 		
				} 		
				if (uiSiteName.equals(siteName)) 
				{ 		
					list.get(i).click(); 	
					System.out.println(uiSiteName+" is selected"); 		
				}	 	
			} 	
			k++; 
		}
		
	}

	@Test(priority=5)
	public void addRolesToTrials() throws IOException, InterruptedException 
	{
		CommonMethod.explicitWait(driver, 5, "ADDROLESTOTRIALS");
		click(driver, "ADDROLESTOTRIALS");
		selectDropdown(driver, "SELECTTRIALDD", datatable.getCellData("CtmsData", "Data", 3));
		sleep(2, TimeUnit.SECONDS);
		click(driver, "SELECTROLESBUTTON");
		sleep(2, TimeUnit.SECONDS);
		int j=2;
		w1:while(true)
		{
			String roleName=datatable.getCellData("Roles", "AddRole", j);
			System.out.println("Role name is "+roleName);
			sleep(2, TimeUnit.SECONDS);
			if (roleName==null||roleName.equals(""))
			{
				int totalRoles=j-2;
				sleep(2, TimeUnit.SECONDS);
				click(driver, "ROLESSUBMITBUTTON");
				System.out.println("All "+totalRoles+" Roles are selected successfully");
				break w1;
			}
			List<WebElement> list=driver.findElements(By.xpath("//ul[@class='dropdown-menu scrollable-menu ng-scope']/li/a"));
			System.out.println("Size of list is "+list.size());
			for (int i = 0; i < list.size(); i++) 
			{
				String uiRoleName=list.get(i).getText();
				if(j==2)
				{
					if (i==1||uiRoleName.equals("Uncheck All")) {
						list.get(i).click();
						sleep(2, TimeUnit.SECONDS);
						CommonMethod.handleAlert(driver);
					}
				}
				if (uiRoleName.equals(roleName))
				{
					list.get(i).click();
					System.out.println(uiRoleName+" is selected");
				}	
			}
			j++;
		}
	}
	
	@Test(priority=6)
	public void distributionToGroup() throws IOException, InterruptedException
	{
		click(driver, "DISTRIBUTIONTOGROUP");
		
		int j=2;
		w1:while(true)
		{
			String roleName=datatable.getCellData("Roles", "DistributionTrial", j);
			System.out.println("Trial name is "+roleName);
			sleep(2, TimeUnit.SECONDS);
			if (roleName==null||roleName.equals(""))
			{
				int totalRoles=j-2;
				sleep(2, TimeUnit.SECONDS);
				System.out.println("Expected "+totalRoles+" Trail is selected successfully");
				break w1;
			}
			List<WebElement> list=driver.findElements(By.xpath("//select[@ng-model='selectedTrail']/..//option"));
			System.out.println("Size of list is "+list.size());
			for (int i = 0; i < list.size(); i++) 
			{
				String uiRoleName=list.get(i).getText();
		
				if (uiRoleName.equals(roleName))
				{
					list.get(i).click();
					System.out.println(uiRoleName+" is selected");
				}	
			}
	        j++;  
		}
		int k=2;
		w1:while(true)
		{
			String roleName=datatable.getCellData("Roles", "DistributionRole", k);
			System.out.println("Role name is "+roleName);
			sleep(2, TimeUnit.SECONDS);
			if (roleName==null||roleName.equals(""))
			{
				int totalRoles=k-2;
				sleep(2, TimeUnit.SECONDS);
				System.out.println("Expected "+totalRoles+" Roles is selected successfully");
				break w1;
			}
			List<WebElement> list=driver.findElements(By.xpath("//select[@ng-model='selectedRole']/..//option"));
			System.out.println("Size of list is "+list.size());
			for (int i = 0; i < list.size(); i++) 
			{
				String uiRoleName=list.get(i).getText();
		
				if (uiRoleName.equals(roleName))
				{
					list.get(i).click();
					System.out.println(uiRoleName+" is selected");
				}	
			}
		k++;
		}
	
		int l=2;
		w1:while(true)
		{
			String roleName=datatable.getCellData("Roles", "NotificationType", l);
			System.out.println("NotificationType name is "+roleName);
			sleep(2, TimeUnit.SECONDS);
			if (roleName==null||roleName.equals(""))
			{
				int totalRoles=l-2;
				sleep(2, TimeUnit.SECONDS);
				//click(driver, "DISTRIBUTIONTOGROUPSUBMITBTN");
				System.out.println("All "+totalRoles+" NotificationTypes are selected successfully");
				break w1;
			}
			List<WebElement> list1=driver.findElements(By.xpath("//ul[@class='row']/..//li"));
			List<WebElement> list2=driver.findElements(By.xpath("//ul[@class='row']/..//li/input"));
			System.out.println("Size of list is "+list1.size());
			
			for (int i = 0; i < list1.size(); i++) 
			{
				String uiRoleName=list1.get(i).getText();
				
				if (uiRoleName.equals(roleName))
				{
					list2.get(i).click();
					System.out.println(uiRoleName+" is selected");
				}	
			}
		l++;
		}
	}

}
